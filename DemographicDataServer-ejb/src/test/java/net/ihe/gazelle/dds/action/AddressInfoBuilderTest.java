package net.ihe.gazelle.dds.action;

import net.ihe.gazelle.dds.model.WebServicesInformation;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(PowerMockRunner.class)
@PrepareForTest(EntityManagerService.class)
public class AddressInfoBuilderTest {

    private static final String PERSISTENCE_UNIT_NAME_CONST = "PersistenceUnitTest";

    static EntityManager entityManager;

    @BeforeClass
    public static void initializeDB() {
        entityManager = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME_CONST).createEntityManager();
        entityManager.getTransaction().begin();
        addTestEntitiesToDB(entityManager);
        entityManager.getTransaction().commit();
    }

    @Before
    public void setup() {
        PowerMockito.mockStatic(EntityManagerService.class);
        Mockito.when(EntityManagerService.provideEntityManager()).thenReturn(entityManager);
    }

    /**
     * Add entities in Database to test the search on.
     *
     * @param entityManager: EntityManager used to merge entities.
     */
    private static void addTestEntitiesToDB(EntityManager entityManager) {
        for (WebServicesInformation entity : getWebServicesInformations()) {
            entityManager.merge(entity);
        }
    }

    /**
     * Getter for the list of existing entities for tests.
     *
     * @return the list of entities available for Search tests
     */
    private static List<WebServicesInformation> getWebServicesInformations() {
        List<WebServicesInformation> webServicesInformationList = new ArrayList<>();

        WebServicesInformation webServicesInformation0 = new WebServicesInformation();
        webServicesInformation0.setWsName("Google");
        webServicesInformation0.setWsURL("http://maps.google.com/maps/geo?q=&output=xml&oe=utf8&sensor=false");
        webServicesInformation0.setWsPriority(2);
        webServicesInformation0.setTotalRequest(0);
        webServicesInformation0.setLastUserIpId(0);
        webServicesInformation0.setRequestSinceLastDay(0);
        webServicesInformation0.setRequestMaximumNumber(0);
        WebServicesInformation webServicesInformation1 = new WebServicesInformation();
        webServicesInformation1.setWsName("Nominatim");
        webServicesInformation1.setWsURL("https://nominatim.openstreetmap.org/reverse?format=xml&lat=&lon=&zoom=18&addressdetails=1&email=eric.poiseau@inria.fr");
        webServicesInformation1.setWsPriority(1);
        webServicesInformation1.setTotalRequest(0);
        webServicesInformation1.setLastUserIpId(0);
        webServicesInformation1.setRequestSinceLastDay(0);
        webServicesInformation1.setRequestMaximumNumber(500);
        WebServicesInformation webServicesInformation2 = new WebServicesInformation();
        webServicesInformation2.setWsName("Geoname");
        webServicesInformation2.setWsURL("http://ws.geonames.org/findNearbyPostalCodes?lat=&lng=&style=SHORT&maxRows=1");
        webServicesInformation2.setWsPriority(3);
        webServicesInformation2.setTotalRequest(0);
        webServicesInformation2.setLastUserIpId(0);
        webServicesInformation2.setRequestSinceLastDay(0);
        webServicesInformation2.setRequestMaximumNumber(0);

        webServicesInformationList.add(webServicesInformation0);
        webServicesInformationList.add(webServicesInformation1);
        webServicesInformationList.add(webServicesInformation2);
        return webServicesInformationList;
    }

    @Test
    public void getAddressServiceForInseeCode() {
        String expected = "https://nominatim.openstreetmap.org/search?format=xml&city=Rennes&country=France&extratags=1&email=eric.poiseau@inria.fr";
        String url = AddressInfoBuilder.getAddressServiceForInseeCode("Rennes");
        assertEquals(expected, url);
    }

    @Test
    public void getAddressServiceForInseeCodeCityWithSpace() {
        String expected = "https://nominatim.openstreetmap.org/search?format=xml&city=Le+Havre&country=France&extratags=1&email=eric.poiseau@inria.fr";
        String url = AddressInfoBuilder.getAddressServiceForInseeCode("Le Havre");
        assertEquals(expected, url);
    }
}
