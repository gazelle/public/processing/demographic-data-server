package net.ihe.gazelle.demographic.ws;

import ca.uhn.hl7v2.app.Connection;
import ca.uhn.hl7v2.app.ConnectionHub;
import ca.uhn.hl7v2.app.Initiator;
import ca.uhn.hl7v2.llp.ExtendedMinLowerLayerProtocol;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.parser.DefaultXMLParser;
import ca.uhn.hl7v2.parser.Parser;
import ca.uhn.hl7v2.parser.PipeParser;
import net.ihe.gazelle.dds.action.AddressInfoBuilder;
import net.ihe.gazelle.dds.action.DemographicDataService;
import net.ihe.gazelle.dds.action.SystemConfig;
import net.ihe.gazelle.dds.admin.ApplicationConfigurationProvider;
import net.ihe.gazelle.dds.admin.WebServicesSecurity;
import net.ihe.gazelle.dds.geonames.GeoName;
import net.ihe.gazelle.dds.hl7.HL7v3MessageGenerator;
import net.ihe.gazelle.dds.hl7.Hl7MessageGenerator;
import net.ihe.gazelle.dds.model.*;
import net.ihe.gazelle.dds.utils.Utils;
import org.jboss.seam.Component;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.contexts.Lifecycle;
import org.jdom.JDOMException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.bootstrap.DOMImplementationRegistry;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletRequest;
import javax.xml.soap.SOAPException;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

/**
 * @author abderrazek boufahja, nicolas lefebvre, Anne-Gaëlle Bergé | IHE development team | IHE Europe
 */
@Stateless
@Name("DemographicDataServer")
@WebService(name = "DemographicDataServer", serviceName = "DemographicDataServerService")
public class DemographicDataServerBean implements Serializable {

    private static final String CC = "countryCode";
    private static final String MINIMAL = "minimal";
    private static final String SOAP_MESSAGE_ERROR = "This instance of Demographic Data Server tool is running in minimal mode thus this operation is not permitted";
    private static final String SOAP_MESSAGE_ERROR2 = "You need to specify a valid country code (FR, DE, IT, US, JP...).";
    private static final String DDS_DOC = "You can see documentation on http://gazelle.ihe-europe.net/hudson/job/DemographicDataServer/ws/DemographicDataServer-ejb/doc/net/ihe/gazelle/demographic/ws/DemographicDataServerBean.html#returnPatient(java.lang.String,%20java.lang.Boolean,%20java.lang.Boolean,%20java.lang.Boolean,%20java.lang.Boolean,%20java.lang.Boolean,%20java.lang.Boolean,%20java.lang.String,%20java.lang.String,%20java.lang.String,%20java.lang.String,%20java.lang.String)";
    private static final String EM = "entityManager";
    private static final String V231 = "2.3.1";
    private static final String V25 = "2.5";
    private static final String A01_231 = "ADT^A01^ADT_A01";
    private static final String A04_231 = "ADT^A04^ADT_A01";
    private static final String A28_25 = "ADT^A28^ADT_A05";
    private static final String DDS_OID = "DDS_OID";
    private static final String DDS_DOMAIN = "DDS_domain";
    private static Logger log = LoggerFactory.getLogger(DemographicDataServerBean.class);
    @Resource
    WebServiceContext wsCtx;

    private String getIp() {
        MessageContext jaxwsContext = wsCtx.getMessageContext();
        HttpServletRequest hRequest = (HttpServletRequest) jaxwsContext.get(MessageContext.SERVLET_REQUEST);
        return hRequest.getRemoteAddr();
    }

    /**
     * Method to return an address specific to a country code
     *
     * @param countryCode : Code for the country we wish to get data. For Example : JP, FR, DE, US , etc
     * @return PatientAddress
     * @throws JDOMException when failed to construct address
     * @throws IOException   when failed to construct address
     * @throws SOAPException when country code keyword is null
     */
    @WebMethod
    public PatientAddress returnAddress(@WebParam(name = CC) String countryCode, @WebParam(name = "birthPlace") Boolean birthPlace) throws JDOMException,
            IOException, SOAPException {
        Lifecycle.beginCall();
        PatientAddress patientAddress = DemographicDataService.returnAddress(countryCode, birthPlace);
        Lifecycle.endCall();
        return patientAddress;
    }

    /**
     * Method to return an address specific to a country code and the lattitude, the longitude specified
     *
     * @param countryCode : Code for the country we wish to get data. For Example : JP, FR, DE, US , etc
     * @param lat         : the lattitude of the place from which we extract the address. For Example 53.5459481506 , 48.5851203778 , etc
     * @param lng         : the longitude of the place from which we extract the address. For Example 10.204494639 , 7.6997042178 , etc.
     * @return a patient address
     * @throws SOAPException failed to create a new address
     */
    @WebMethod
    public PatientAddress returnAddressByCoordinates(@WebParam(name = CC) String countryCode,
                                                     @WebParam(name = "lattitude") Double lat, @WebParam(name = "longitude") Double lng) throws SOAPException {
        Lifecycle.beginCall();
        if (ApplicationConfigurationProvider.instance().getApplicationMode().equals(MINIMAL)) {
            throw new SOAPException(
                    SOAP_MESSAGE_ERROR);
        } else {
            if (lng < -180 || lng > 180 || lat > 90 || lat < -90) {
                SOAPException soapEx = new SOAPException(
                        "You need to specify a valid latitude (Between -90 and 90) and a valid longitude (Between -180 and 180)");
                throw soapEx;
            }

            String currentIp = getIp();

            if (WebServicesSecurity.requestWsPermission(currentIp)) {
                String countryCodeKW = CountryCode.getCountryCodeKeyword(countryCode);
                if ((countryCodeKW == null || countryCodeKW.length() == 0)) {
                    SOAPException soapEx = new SOAPException(
                            SOAP_MESSAGE_ERROR2);
                    throw soapEx;
                }
                PatientAddress patientAddress = null;
                try {
                    patientAddress = AddressInfoBuilder.constructNearbyAddress(countryCodeKW, lat, lng);
                    if (patientAddress.getStreet() == null) {
                        throw new SOAPException();
                    }
                    patientAddress.getCountry().setLanguage(null);
                    Lifecycle.endCall();
                    return patientAddress;
                } catch (Exception e) {
                    SOAPException soapEx = new SOAPException("Please verify the lattitude and the longitude.", e);
                    throw soapEx;
                }
            } else {
                SOAPException soapEx = new SOAPException(
                        "Request number limite exceeded ! Will be available in a little while.");
                throw soapEx;
            }
        }
    }

    /**
     * Method to return an address from a specific town
     *
     * @param townName : the name of the town, for Example Paris, Rennes, ...
     * @return an address from a specific town
     * @throws JDOMException failed to construct address
     * @throws IOException   failed to construct address
     * @throws SOAPException application is minimal or town name not find in DB or to much request in the day
     */
    @WebMethod
    public PatientAddress returnAddressByTown(@WebParam(name = "townName") String townName) throws JDOMException,
            IOException, SOAPException {
        Lifecycle.beginCall();
        if (ApplicationConfigurationProvider.instance().getApplicationMode().equals(MINIMAL)) {
            throw new SOAPException(
                    SOAP_MESSAGE_ERROR);
        } else {

            if (townName == null || townName.isEmpty()) {
                SOAPException soapEx = new SOAPException(
                        "You need to specify a town name");
                throw soapEx;
            }
            String currentIp = getIp();

            if (WebServicesSecurity.requestWsPermission(currentIp)) {
                List<GeoName> listGeo = GeoName.getGeoNameFiltered(null, townName);
                GeoName inGeoName;
                if (listGeo != null) {
                    if (listGeo.size() > 0) {
                        inGeoName = listGeo.get(0);
                    } else {
                        SOAPException soapEx = new SOAPException(
                                "the town's name ("
                                        + townName
                                        + ") was not found. Try please an other name. You can see documentation on http://gazelle.ihe-europe.net/hudson/job/DemographicDataServer/ws/DemographicDataServer-ejb/doc/net/ihe/gazelle/demographic/ws/DemographicDataServerBean.html#returnAddressByTown(java.lang.String)");
                        throw soapEx;
                    }
                } else {
                    SOAPException soapEx = new SOAPException(
                            "the town's name ("
                                    + townName
                                    + ") was not found. Try please an other name. You can see documentation on http://gazelle.ihe-europe.net/hudson/job/DemographicDataServer/ws/DemographicDataServer-ejb/doc/net/ihe/gazelle/demographic/ws/DemographicDataServerBean.html#returnAddressByTown(java.lang.String)");
                    throw soapEx;
                }

                PatientAddress patientAddress;
                patientAddress = AddressInfoBuilder.constructAddress(inGeoName);
                patientAddress.getCountry().setLanguage(null);
                Lifecycle.endCall();
                return patientAddress;
            } else {
                SOAPException soapEx = new SOAPException(
                        "Request number limite exceeded ! Will be available in a little while. You can see documentation on http://gazelle.ihe-europe.net/hudson/job/DemographicDataServer/ws/DemographicDataServer-ejb/doc/net/ihe/gazelle/demographic/ws/DemographicDataServerBean.html#returnPatient(java.lang.String,%20java.lang.Boolean,%20java.lang.Boolean,%20java.lang.Boolean,%20java.lang.Boolean,%20java.lang.Boolean,%20java.lang.Boolean,%20java.lang.String,%20java.lang.String,%20java.lang.String,%20java.lang.String,%20java.lang.String)");
                throw soapEx;
            }
        }
    }

    /**
     * Method to return an address from a specific town
     *
     * @param townName : the name of the town, for Example Paris, Rennes, ...
     * @return the INSEE (COG) code of the town
     * @throws SOAPException application is minimal or town name not find in DB or to much request in the day
     */
    @WebMethod
    public String returnTownInseeCode(@WebParam(name = "townName") String townName) throws SOAPException {
        Lifecycle.beginCall();
        String inseeCode = DemographicDataService.returnTownInseeCode(townName);
        Lifecycle.endCall();
        return inseeCode;

    }

    /**
     * Method to return an address from a specific town
     *
     * @param countryCode : the iso code of the country, for Example FR for France
     * @return the INSEE (COG) code of the country
     * @throws SOAPException application is minimal or town name not find in DB or to much request in the day
     */
    @WebMethod
    public String returnCountryInseeCode(@WebParam(name = "countryCode") String countryCode) throws SOAPException {
        Lifecycle.beginCall();
        String inseedCode = DemographicDataService.returnCountryInseeCode(countryCode);
        Lifecycle.endCall();
        return inseedCode;
    }



    /**
     * To Return a Person from some parameters.
     *
     * @param countryCode            : Code for the country we wish to get data. For Example : JP, FR, DE, US , etc
     * @param lastNameOption         : specify if you want to generate a last name. It take two values : true , false.
     * @param firstNameOption        : specify if you want to generate a first name. It take two values : true , false.
     * @param motherMaidenNameOption : specify if you want to generate a mother maiden name for the person. It take three values : true , false, null.
     * @param religionOption         : specify if you want to generate a religion for the person. It take two values : true , false.
     * @param raceOption             : specify if you want to generate a race for the person. It take two values : true , false.
     * @param birthDayOption         : specify if you want to generate a date of birth for the patient. It take two values : true , false.
     * @param genderDescription      : Specify the gender of the person. It take these values : male, female, Male, Female, m, M, f, F. For other value, a random gender is token.
     * @param firstNameLike          : Restrict the generation for first name which begin with this parameter. Exemple : Dav, Loui, etc.
     * @param lastNameLike           : Restrict the generation for last name which begin with this parameter. Exemple : May, Bek, etc.
     * @param firstNameIs            : Give the exact value of the first name. Exemple : David, Louis, etc.
     * @param lastNameIs             : Give the exact value of the last name. Exemple : Lambert, Smith, etc.
     * @return a Person from some parameters.
     * @throws SOAPException application is minimal or country code, first name or last name not find in DB
     */
    @WebMethod
    public Person returnPerson(@WebParam(name = CC) String countryCode,
                               @WebParam(name = "lastNameOption") Boolean lastNameOption,
                               @WebParam(name = "firstNameOption") Boolean firstNameOption,
                               @WebParam(name = "motherMaidenNameOption") Boolean motherMaidenNameOption,
                               @WebParam(name = "religionOption") Boolean religionOption,
                               @WebParam(name = "raceOption") Boolean raceOption,
                               @WebParam(name = "birthDayOption") Boolean birthDayOption,
                               @WebParam(name = "genderDescription") String genderDescription,
                               @WebParam(name = "firstNameLike") String firstNameLike,
                               @WebParam(name = "lastNameLike") String lastNameLike, @WebParam(name = "firstNameIs") String firstNameIs,
                               @WebParam(name = "lastNameIs") String lastNameIs) throws SOAPException {
        Lifecycle.beginCall();
        if (ApplicationConfigurationProvider.instance().getApplicationMode().equals(MINIMAL)) {
            throw new SOAPException(
                    SOAP_MESSAGE_ERROR);
        } else {
            String countryCodeKW = CountryCode.getCountryCodeKeyword(countryCode);
            if ((countryCodeKW == null || countryCodeKW.isEmpty() || countryCodeKW.length() == 0)) {
                SOAPException soapEx = new SOAPException(
                        SOAP_MESSAGE_ERROR2 +
                                "You can see documentation on http://gazelle.ihe-europe.net/hudson/job/DemographicDataServer/ws/DemographicDataServer-ejb/doc/net/ihe/gazelle/demographic/ws/DemographicDataServerBean.html#returnPerson(java.lang.String,%20boolean,%20boolean,%20boolean,%20boolean,%20boolean,%20java.lang.String,%20java.lang.String,%20java.lang.String,%20java.lang.String,%20java.lang.String)");
                throw soapEx;
            }

            if ((firstNameIs != null && !firstNameIs.isEmpty()) && (firstNameLike != null && !firstNameLike.isEmpty())) {
                SOAPException soapEx = new SOAPException(
                        "You must choose between the firstNameLike and the firstNameIs. "
                                + DDS_DOC);
                throw soapEx;
            }

            if ((lastNameIs != null && !lastNameIs.isEmpty()) && (lastNameLike != null && !lastNameLike.isEmpty())) {
                SOAPException soapEx = new SOAPException(
                        "You must choose between the lastNameLike and the lastNameIs. "
                                + DDS_DOC);
                throw soapEx;
            }

            Gender gender = null;
            if (genderDescription != null && !genderDescription.equals("")) {
                String gd = Gender.capitalize(genderDescription);
                gender = Gender.getGenderByDescription(gd);
                if(gender == null && !genderDescription.equals("R") && !genderDescription.equals("Random")){
                    throw new SOAPException(
                            "The specified gender description is not supported by DDS. Possible values are : 'Male', 'M', 'Female', 'F', 'Random' and 'R'. ");
                }
            }

            Person person;

            if (lastNameOption == null) {
                lastNameOption = true;
            }

            if (firstNameOption == null) {
                firstNameOption = true;
            }

            if (religionOption == null) {
                religionOption = true;
            }

            if (raceOption == null) {
                raceOption = true;
            }

            if (birthDayOption == null) {
                birthDayOption = true;
            }

            if (motherMaidenNameOption == null) {
                motherMaidenNameOption = new Boolean(true);
            }

            person = Person.generateRandomPerson(countryCodeKW, lastNameOption, firstNameOption, motherMaidenNameOption,
                    religionOption, raceOption, birthDayOption, gender, null, null, firstNameLike, lastNameLike, null,
                    false, false, false, false, false, null);

            if (firstNameOption && firstNameIs != null && !firstNameIs.isEmpty()) {
                person.setFirstAlternativeName(firstNameIs);
                person.setFirstNameSex(null);
            }

            if (lastNameOption != false && lastNameIs != null && !lastNameIs.isEmpty()) {
                person.setLastAlternativeName(lastNameIs);
                person.setLastName(null);
            }
            Lifecycle.endCall();
            return person;
        }
    }

    /**
     * Method to return a patient which correspond to options specified. If a boolean value is not filled, the default value will be true.
     *
     * @param countryCode            : Code for the country we wish to get data. For Example : JP, FR, DE, US , etc
     * @param lastNameOption         : specify if you want to generate a last name. It take two values : true , false.
     * @param firstNameOption        : specify if you want to generate a first name. It take two values : true , false.
     * @param motherMaidenNameOption : specify if you want to generate a mother maiden name for the person. It take three values : true , false, null.
     * @param religionOption         : specify if you want to generate a religion for the person. It take two values : true , false.
     * @param raceOption             : specify if you want to generate a race for the person. It take two values : true , false.
     * @param birthDayOption         : specify if you want to generate a date of birth for the patient. It take two values : true , false.
     * @param addressOption          : specify if you want to generate an address for the patient. It take two values : true , false.
     * @param genderDescription      : Specify the gender of the person. It take these values : male, female, Male, Female, m, M, f, F. For other value, a random gender is token.
     * @param firstNameLike          : Restrict the generation for first name which begin with this parameter. Exemple : Dav, Loui, etc.
     * @param lastNameLike           : Restrict the generation for last name which begin with this parameter. Exemple : May, Bek, etc.
     * @param firstNameIs            : Give the exact value of the first name. Exemple : David, Louis, etc.
     * @param lastNameIs             : Give the exact value of the last name. Exemple : Lambert, Smith, etc.
     * @param maritalSatusOption     : specify the marital status of the patient. Possible values are : Married or M,Single or S,Divorced or D,Unknown or U, Random or R.
     * @param deadPatientOption      : specify if the patient is dead or not. Possible values are : true, false
     * @param maidenNameOption       : specify if you want to generate a maiden name for the patient. If true, the gender description must be 'Female'. Possible values : true, false
     * @param aliasNameOption        : specify if you want to generate an alias name for the patient. Possible values: true, false.
     * @param displayNameOption      : specify if you want to generate a display name for the patient. Possible values: true, false.
     * @param newBornOption          : specify if the patient is a new born or not. If it is a new born, the patient will have a mother.
     * @return a patient which correspond to options specified
     * @throws JDOMException failed to generate random patient
     * @throws IOException   failed to generate random patient
     * @throws SOAPException exception
     */
    @WebMethod
    public Patient returnPatientWithAllOptions(@WebParam(name = CC) String countryCode,
                                               @WebParam(name = "lastNameOption") Boolean lastNameOption,
                                               @WebParam(name = "firstNameOption") Boolean firstNameOption,
                                               @WebParam(name = "motherMaidenNameOption") Boolean motherMaidenNameOption,
                                               @WebParam(name = "religionOption") Boolean religionOption,
                                               @WebParam(name = "raceOption") Boolean raceOption,
                                               @WebParam(name = "birthDayOption") Boolean birthDayOption,
                                               @WebParam(name = "addressOption") Boolean addressOption,
                                               @WebParam(name = "genderDescription") String genderDescription,
                                               @WebParam(name = "firstNameLike") String firstNameLike,
                                               @WebParam(name = "lastNameLike") String lastNameLike,
                                               @WebParam(name = "firstNameIs") String firstNameIs,
                                               @WebParam(name = "lastNameIs") String lastNameIs,
                                               @WebParam(name = "maritalSatusOption") String maritalSatusOption,
                                               @WebParam(name = "deadPatientOption") Boolean deadPatientOption,
                                               @WebParam(name = "maidenNameOption") Boolean maidenNameOption,
                                               @WebParam(name = "aliasNameOption") Boolean aliasNameOption,
                                               @WebParam(name = "displayNameOption") Boolean displayNameOption,
                                               @WebParam(name = "newBornOption") Boolean newBornOption) throws JDOMException, IOException, SOAPException {
        Lifecycle.beginCall();
        if (ApplicationConfigurationProvider.instance().getApplicationMode().equals(MINIMAL)) {
            throw new SOAPException(
                    SOAP_MESSAGE_ERROR);
        } else {
            String currentIp = getIp();

            if (WebServicesSecurity.requestWsPermission(currentIp)) {

                String countryCodeKW = CountryCode.getCountryCodeKeyword(countryCode);
                if ((countryCodeKW == null || countryCodeKW.length() == 0)) {
                    SOAPException soapEx = new SOAPException(
                            SOAP_MESSAGE_ERROR2);
                    throw soapEx;
                }

                if ((firstNameIs != null && !firstNameIs.isEmpty())
                        && (firstNameLike != null && !firstNameLike.isEmpty())) {
                    SOAPException soapEx = new SOAPException(
                            "You must choose between the firstNameLike and the firstNameIs. ");
                    throw soapEx;
                }

                if ((lastNameIs != null && !lastNameIs.isEmpty()) && (lastNameLike != null && !lastNameLike.isEmpty())) {
                    SOAPException soapEx = new SOAPException(
                            "You must choose between the lastNameLike and the lastNameIs. ");
                    throw soapEx;
                }

                if (lastNameOption == null) {
                    lastNameOption = true;
                }

                if (firstNameOption == null) {
                    firstNameOption = true;
                }

                if (motherMaidenNameOption == null) {
                    motherMaidenNameOption = true;
                }

                if (religionOption == null) {
                    religionOption = true;
                }

                if (raceOption == null) {
                    raceOption = true;
                }

                if (birthDayOption == null) {
                    birthDayOption = true;
                }

                if (addressOption == null) {
                    addressOption = true;
                }

                if (deadPatientOption == null) {
                    deadPatientOption = true;
                }

                if (aliasNameOption == null) {
                    aliasNameOption = true;
                }

                if (displayNameOption == null) {
                    displayNameOption = true;
                }

                if (maidenNameOption == null) {
                    maidenNameOption = true;
                }

                String maritalStatus = "";
                if (!(maritalSatusOption.equals("Unknown") || maritalSatusOption.equals("Married")
                        || maritalSatusOption.equals("Single") || maritalSatusOption.equals("Divorced")
                        || maritalSatusOption.equals("U") || maritalSatusOption.equals("M")
                        || maritalSatusOption.equals("S") || maritalSatusOption.equals("D")
                        || maritalSatusOption.equals("Random") || maritalSatusOption.equals("R"))) {
                    SOAPException soapEx = new SOAPException(
                            "You need to specify a value for the marital Satus Option (Married or M,Single or S,Divorced or D,Unknown or U, Random or R). ");
                    throw soapEx;
                }

                if (!(maritalSatusOption.isEmpty() || maritalSatusOption.equals("Unknown") || maritalSatusOption
                        .equals("U"))) {
                    if (maritalSatusOption.equals("Married") || maritalSatusOption.equals("M")) {
                        maritalStatus = "M";
                    } else if (maritalSatusOption.equals("Single") || maritalSatusOption.equals("S")) {
                        maritalStatus = "S";
                    } else if (maritalSatusOption.equals("Divorced") || maritalSatusOption.equals("D")) {
                        maritalStatus = "D";
                    } else if (maritalSatusOption.equals("Random") || maritalSatusOption.equals("R")) {
                        double a = Math.random();
                        if (a < 0.3) {
                            maritalStatus = "M";
                        } else if (a > 0.7) {
                            maritalStatus = "D";
                        } else {
                            maritalStatus = "S";
                        }
                    }
                }

                if (newBornOption == null) {
                    newBornOption = true;
                }

                if (newBornOption && !maritalStatus.isEmpty()) {
                    SOAPException soapEx = new SOAPException(
                            "If the new born option is true, the marital status must be set to 'Unknown' or 'U'. ");
                    throw soapEx;
                }

                Gender gender = null;
                if (genderDescription != null && !genderDescription.equals("")) {
                    genderDescription = Gender.capitalize(genderDescription);
                    gender = Gender.getGenderByDescription(genderDescription);
                    if(gender == null && !genderDescription.equals("R") && !genderDescription.equals("Random")){
                        throw new SOAPException(
                                "The specified gender description is not supported by DDS. Possible values are : 'Male', 'M', 'Female', 'F', 'Random' and 'R'. ");
                    }
                }

                if (maidenNameOption && (gender == null || !gender.getDescription().equals("Male"))) {
                    gender = Gender.getGender_Female();
                }

                if (gender == null
                        && (genderDescription == null || genderDescription.isEmpty() || genderDescription.equals("Random") || genderDescription
                        .equals("R"))) {
                    gender = Gender.getRandomGenderBetweenMaleAndFemale();
                }

                if (gender == null) {
                    SOAPException soapEx = new SOAPException(
                            "The specified gender description is not supported by DDS. Possible values are : 'Male', 'M', 'Female', 'F', 'Random' and 'R'. ");
                    throw soapEx;
                }

                if (gender.getDescription().equals("Male") && (maidenNameOption)) {
                    SOAPException soapEx = new SOAPException(
                            "The gender description = 'Male' and the maiden Name Option = 'true' are incompatible. ");
                    throw soapEx;
                }

                EntityManager entityManager = (EntityManager) Component.getInstance(EM);

                Patient patient = Patient.generateRandomPatient(countryCodeKW, lastNameOption, firstNameOption,
                        motherMaidenNameOption.booleanValue(), religionOption.booleanValue(),
                        raceOption.booleanValue(), birthDayOption.booleanValue(), addressOption.booleanValue(), gender,
                        null, null, firstNameLike, lastNameLike, maritalStatus, deadPatientOption.booleanValue(),
                        maidenNameOption.booleanValue(), aliasNameOption.booleanValue(),
                        displayNameOption.booleanValue(), newBornOption.booleanValue(), null, entityManager);

                if (addressOption) {
                    for (PatientAddress inPatientAddress : patient.getAddress()) {
                        inPatientAddress.getCountry().setLanguage(null);
                    }
                }

                if (firstNameOption && firstNameIs != null && !firstNameIs.isEmpty()) {
                    patient.getPerson().setFirstAlternativeName(firstNameIs);
                    patient.getPerson().setFirstNameSex(null);
                }

                if (lastNameOption && lastNameIs != null && !lastNameIs.isEmpty()) {
                    patient.getPerson().setLastAlternativeName(lastNameIs);
                    patient.getPerson().setLastName(null);
                }

                if (patient != null) {
                    patient = Patient.mergePatient(patient, entityManager);
                }
                Lifecycle.endCall();
                return patient;
            } else {
                SOAPException soapEx = new SOAPException(
                        "Request number limit exceeded ! Will be available again in a little while.");
                Lifecycle.endCall();
                throw soapEx;
            }
        }

    }

    /**
     * Method to return a patient which correspond to options specified
     *
     * @param countryCode       : Code for the country we wish to get data. For Example : JP, FR, DE, US , etc
     * @param lastNameOption    : specify if you want to generate a last name. It take two values : true , false.
     * @param firstNameOption   : specify if you want to generate a first name. It take two values : true , false.
     * @param religionOption    : specify if you want to generate a religion for the person. It take two values : true , false.
     * @param raceOption        : specify if you want to generate a race for the person. It take two values : true , false.
     * @param birthDayOption    : specify if you want to generate a date of birth for the patient. It take two values : true , false.
     * @param addressOption     : specify if you want to generate an address for the patient. It take two values : true , false.
     * @param genderDescription : Specify the gender of the person. It take these values : male, female, Male, Female, m, M, f, F. For other value, a random gender is token.
     * @param firstNameLike     : Restrict the generation for first name which begin with this parameter. Exemple : Dav, Loui, etc.
     * @param lastNameLike      : Restrict the generation for last name which begin with this parameter. Exemple : May, Bek, etc.
     * @param firstNameIs       : Give the exact value of the first name. Exemple : David, Louis, etc.
     * @param lastNameIs        : Give the exact value of the last name. Exemple : Lambert, Smith, etc.
     * @return a patient which correspond to options specified
     * @throws JDOMException failed to generate random patient
     * @throws IOException   failed to generate random patient
     * @throws SOAPException exception
     */
    @WebMethod
    public Patient returnPatient(@WebParam(name = CC) String countryCode,
                                 @WebParam(name = "lastNameOption") Boolean lastNameOption,
                                 @WebParam(name = "firstNameOption") Boolean firstNameOption,
                                 @WebParam(name = "religionOption") Boolean religionOption,
                                 @WebParam(name = "raceOption") Boolean raceOption,
                                 @WebParam(name = "birthDayOption") Boolean birthDayOption,
                                 @WebParam(name = "addressOption") Boolean addressOption,
                                 @WebParam(name = "genderDescription") String genderDescription,
                                 @WebParam(name = "firstNameLike") String firstNameLike,
                                 @WebParam(name = "lastNameLike") String lastNameLike, @WebParam(name = "firstNameIs") String firstNameIs,
                                 @WebParam(name = "lastNameIs") String lastNameIs) throws JDOMException, IOException, SOAPException {
        Lifecycle.beginCall();
        if (ApplicationConfigurationProvider.instance().getApplicationMode().equals(MINIMAL)) {
            throw new SOAPException(
                    SOAP_MESSAGE_ERROR);
        } else {
            String currentIp = getIp();

            if (WebServicesSecurity.requestWsPermission(currentIp)) {

                String countryCodeKW = CountryCode.getCountryCodeKeyword(countryCode);
                if ((countryCodeKW == null || countryCodeKW.length() == 0)) {
                    SOAPException soapEx = new SOAPException(
                            "You need to specify a valid country code (FR, DE, IT, US, JP...). You can see documentation on http://gazelle.ihe-europe.net/hudson/job/DemographicDataServer/ws/DemographicDataServer-ejb/doc/net/ihe/gazelle/demographic/ws/DemographicDataServerBean.html#returnPatient(java.lang.String,%20java.lang.Boolean,%20java.lang.Boolean,%20java.lang.Boolean,%20java.lang.Boolean,%20java.lang.Boolean,%20java.lang.Boolean,%20java.lang.String,%20java.lang.String,%20java.lang.String,%20java.lang.String,%20java.lang.String)");
                    throw soapEx;
                }

                if ((firstNameIs != null && !firstNameIs.isEmpty())
                        && (firstNameLike != null && !firstNameLike.isEmpty())) {
                    SOAPException soapEx = new SOAPException(
                            "You must choose between the firstNameLike and the firstNameIs. "
                                    + DDS_DOC);
                    throw soapEx;
                }

                if ((lastNameIs != null && !lastNameIs.isEmpty()) && (lastNameLike != null && !lastNameLike.isEmpty())) {
                    SOAPException soapEx = new SOAPException(
                            "You must choose between the lastNameLike and the lastNameIs. "
                                    + DDS_DOC);
                    throw soapEx;
                }

                if (lastNameOption == null) {
                    lastNameOption = true;
                }

                if (firstNameOption == null) {
                    firstNameOption = true;
                }

                if (religionOption == null) {
                    religionOption = true;
                }

                if (raceOption == null) {
                    raceOption = true;
                }

                if (birthDayOption == null) {
                    birthDayOption = true;
                }

                if (addressOption == null) {
                    addressOption = true;
                }

                Gender gender = null;
                if (genderDescription != null && !genderDescription.equals("")) {
                    genderDescription = Gender.capitalize(genderDescription);
                    gender = Gender.getGenderByDescription(genderDescription);
                    if(gender == null && !genderDescription.equals("R") && !genderDescription.equals("Random")){
                        throw new SOAPException(
                                "The specified gender description is not supported by DDS. Possible values are : 'Male', 'M', 'Female', 'F', 'Random' and 'R'. ");
                    }
                }

                Patient patient;

                EntityManager entityManager = (EntityManager) Component.getInstance(EM);

                patient = Patient.generateRandomPatient(countryCodeKW, lastNameOption.booleanValue(),
                        firstNameOption.booleanValue(), true, religionOption.booleanValue(), raceOption.booleanValue(),
                        birthDayOption.booleanValue(), addressOption.booleanValue(), gender, null, null, firstNameLike,
                        lastNameLike, null, false, false, false, false, false, null, entityManager);

                if (addressOption) {
                    for (PatientAddress inPatientAddress : patient.getAddress()) {
                        inPatientAddress.getCountry().setLanguage(null);
                    }
                }

                if (firstNameOption && firstNameIs != null && !firstNameIs.isEmpty()) {
                    patient.getPerson().setFirstAlternativeName(firstNameIs);
                    patient.getPerson().setFirstNameSex(null);
                }

                if (lastNameOption && lastNameIs != null && !lastNameIs.isEmpty()) {
                    patient.getPerson().setLastAlternativeName(lastNameIs);
                    patient.getPerson().setLastName(null);
                }

                if (patient != null) {
                    patient = Patient.mergePatient(patient, entityManager);
                }
                Lifecycle.endCall();
                return patient;
            } else {
                SOAPException soapEx = new SOAPException(
                        "Request number limite exceeded ! Will be available again in a little while. You can see documentation on http://gazelle.ihe-europe.net/hudson/job/DemographicDataServer/ws/DemographicDataServer-ejb/doc/net/ihe/gazelle/demographic/ws/DemographicDataServerBean.html#returnPatient(java.lang.String,%20java.lang.Boolean,%20java.lang.Boolean,%20java.lang.Boolean,%20java.lang.Boolean,%20java.lang.Boolean,%20java.lang.Boolean,%20java.lang.String,%20java.lang.String,%20java.lang.String,%20java.lang.String,%20java.lang.String)");
                throw soapEx;
            }
        }

    }

    /**
     * Method to return a patient from a country.
     *
     * @param countryCode : Code for the country we wish to get data. For Example : JP, FR, DE, US , etc
     * @return a patient from a country.
     * @throws JDOMException failed to generate random patient
     * @throws IOException   failed to generate random patient
     * @throws SOAPException country code not find in DB or request number limite exceeded
     */
    @WebMethod
    public Patient returnSimplePatient(@WebParam(name = CC) String countryCode) throws JDOMException,
            IOException, SOAPException {
        Lifecycle.beginCall();
        String currentIp = getIp();

        if (WebServicesSecurity.requestWsPermission(currentIp)) {
            String countryCodeKW = CountryCode.getCountryCodeKeyword(countryCode);
            if ((countryCodeKW == null || countryCodeKW.length() == 0)) {
                SOAPException soapEx = new SOAPException(
                        "You need to specify a valid country code (FR, DE, IT, US, JP...)");
                throw soapEx;
            }

            CountryCode country = CountryCode.getCountryCodeByKeyword(countryCodeKW);

            EntityManager entityManager = (EntityManager) Component.getInstance(EM);
            Patient patient = null;
            if (ApplicationConfigurationProvider.instance().getApplicationMode().equals(MINIMAL)) {
                patient = Patient.getRandomPatientFromDatabase(countryCodeKW);
                patient.setAddress(new ArrayList<PatientAddress>(patient.getAddress()));
                patient.getPerson().setOtherFirstNameSexList(new ArrayList<FirstNameSex>(patient.getPerson().getOtherFirstNameSexList()));
                patient.getPerson().setOtherNameList(new ArrayList<OtherName>(patient.getPerson().getOtherNameList()));
            } else {
                patient = Patient.generateRandomPatient(countryCodeKW, true, true, true, country.getGenerateReligion(),
                        country.getGenerateRace(), true, true, null, null, null, null, null, null, false, false, false,
                        false, false, null, entityManager);
                if (patient != null) {
                    patient = Patient.mergePatient(patient, entityManager);
                }
            }
            Lifecycle.endCall();
            return patient;
        } else {
            SOAPException soapEx = new SOAPException(
                    "Request number limite exceeded ! Will be available in a little while.");
            throw soapEx;
        }
    }


    /**
     * Method to return a HL7 Message containing description of a patient that correspond to the specified country code.
     *
     * @param countryCode          : Code for the country we wish to get data. For Example : JP, FR, DE, US , etc
     * @param receivingApplication : The Application of your system.
     * @param receivingFacility    : The Facility of your system.
     * @param characterSet         : The character set encoding of your message. Possible values : UNICODE UTF-8, ASCII, etc
     * @param hl7Version           : The HL7 version of the message. Possible values : 2.3.1 or 2.5
     * @param messageType          : The message type of the message. Possible values : ADT^A01^ADT_A01, ADT^A04^ADT_A01 or ADT^A28^ADT_A05. The ADT^A28^ADT_A05 is only available with the HL7 v2.5 version and the
     *                             ADT^A01^ADT_A01 and ADT^A04^ADT_A01 with the HL7 v2.3.1 version.
     * @return a HL7 Message containing description of a patient that correspond to the specified country code.
     * @throws JDOMException failed to generate random patient
     * @throws IOException   failed to generate random patient
     * @throws SOAPException exception
     */
    @WebMethod
    public String returnHl7Message(@WebParam(name = CC) String countryCode,
                                   @WebParam(name = "receivingApplication") String receivingApplication,
                                   @WebParam(name = "receivingFacility") String receivingFacility,
                                   @WebParam(name = "characterSet") String characterSet, @WebParam(name = "hl7Version") String hl7Version,
                                   @WebParam(name = "messageType") String messageType) throws JDOMException, IOException,
            SOAPException {
        Lifecycle.beginCall();
        String currentIp = getIp();

        if (WebServicesSecurity.requestWsPermission(currentIp)) {
            String countryCodeKW = CountryCode.getCountryCodeKeyword(countryCode);
            if ((countryCodeKW == null || countryCodeKW.length() == 0)) {
                SOAPException soapEx = new SOAPException(
                        SOAP_MESSAGE_ERROR2);
                throw soapEx;
            }

            // To verify that the characterSet is available for the country chosen.
            List<CharacterSetEncoding> charsetList = getCharsetList(countryCodeKW);
            if ((characterSet == null) || (charsetList == null)) {
                SOAPException soapEx = new SOAPException(
                        "You need to specify a character set. Example : UNICODE UTF-8, ASCII, etc. ");
                throw soapEx;
            }

            boolean testCharset = false;
            StringBuilder sb = new StringBuilder();
            testCharset = isTestCharset(characterSet, charsetList, testCharset, sb);

            if (!testCharset) {
                SOAPException soapEx = new SOAPException(
                        "You need to specify a valid character encoding for this country. Example : "
                                + sb.toString());
                throw soapEx;
            }

            if (!hl7Version.equals(V231) && !hl7Version.equals(V25)) {
                SOAPException soapEx = new SOAPException(
                        "You need to specify a valid HL7 version for message. Example : 2.3.1 or 2.5.");
                throw soapEx;
            }

            if (!messageType.equals(A01_231) && !messageType.equals(A04_231)
                    && !messageType.equals(A28_25)) {
                SOAPException soapEx = new SOAPException(
                        "You need to specify a valid HL7 message type. Example : ADT^A01^ADT_A01, ADT^A04^ADT_A01 or ADT^A28^ADT_A05.");
                throw soapEx;
            }

            if (hl7Version.equals(V231)
                    && !(messageType.equals(A01_231) || messageType.equals(A04_231))) {
                SOAPException soapEx = new SOAPException(
                        "If you choose the HL7 v2.3.1 version, you can only use the HL7 message types : ADT^A01^ADT_A01 or ADT^A04^ADT_A01.");
                throw soapEx;
            }

            if (hl7Version.equals(V25) && !messageType.equals(A28_25)) {
                SOAPException soapEx = new SOAPException(
                        "If you choose the HL7 v2.5 version, you can only use the HL7 message type : ADT^A28^ADT_A05.");
                throw soapEx;
            }
            if (!Contexts.isApplicationContextActive()) {
                Lifecycle.beginCall();
            }

            String messagetypeAndVersion = messageType + " (v" + hl7Version + ")";

            SystemConfig configMessage = new SystemConfig();
            configMessage.setReceivingApplication(receivingApplication);
            configMessage.setReceivingFacility(receivingFacility);

            EntityManager entityManager = (EntityManager) Component.getInstance(EM);
            AssigningAuthority authority = new AssigningAuthority();
            String apoid = ApplicationConfiguration.getValueOfVariable(DDS_OID);
            authority.setRootOID(apoid);
            String apdomaine = ApplicationConfiguration.getValueOfVariable(DDS_DOMAIN);
            authority.setName(apdomaine);
            authority.setKeyword(apdomaine);

            configMessage.setAuthority(authority);

            CountryCode country = CountryCode.getCountryCodeByKeyword(countryCodeKW);

            Patient patientWsHl7 = null;
            if (ApplicationConfigurationProvider.instance().getApplicationMode().equals(MINIMAL)) {
                patientWsHl7 = Patient.getRandomPatientFromDatabase(countryCodeKW);
            } else {
                patientWsHl7 = Patient.generateRandomPatient(countryCodeKW, true, true, true,
                        country.getGenerateReligion(), country.getGenerateRace(), true, true, null, null, null, null,
                        null, null, false, false, false, false, false, null, entityManager);

                patientWsHl7 = Patient.mergePatient(patientWsHl7, entityManager);
            }
            Message createdMessage = Hl7MessageGenerator.generate(messagetypeAndVersion, patientWsHl7, configMessage,
                    characterSet);

            // Now, let's encode the message and look at the output
            try {
                Parser parser = new PipeParser();
                String encodedMessage = parser.encode(createdMessage);

                // Next, let's use the XML parser to encode as XML
                parser = new DefaultXMLParser();
                encodedMessage = parser.encode(createdMessage);

                Lifecycle.endCall();
                return encodedMessage;
            } catch (Exception e) {
                log.error("" + e.getMessage());
                SOAPException soapEx = new SOAPException("Error while parsing HL7 message");
                throw soapEx;
            }
        } else {
            SOAPException soapEx = new SOAPException(
                    "Request number limite exceeded ! Will be available again in a little while.");
            throw soapEx;
        }
    }

    private boolean isTestCharset(@WebParam(name = "characterSet") String characterSet, List<CharacterSetEncoding> charsetList, boolean testCharset, StringBuilder sb) {
        for (CharacterSetEncoding charset : charsetList) {
            if (characterSet.equals(charset.getCharset())) {
                testCharset = true;
            }
            sb.append(charset.getCharset());
            sb.append(", ");
        }
        return testCharset;
    }

    /**
     * Method to create and send a HL7 message to a specific port and host.
     *
     * @param countryCode          : Code for the country we wish to get data. For Example : JP, FR, DE, US , etc
     * @param targetHost           : The Ip of the host to which we send the HL7 message. Example : tatami.irisa.fr, 131.254.12.221.
     * @param targetPort           : The port used to send the message. Example : 3601, 2400, etc.
     * @param receivingApplication : The Application of your system.
     * @param receivingFacility    : The Facility of your system.
     * @param characterSet         : The character set encoding of your message. Possible values : UNICODE UTF-8, ASCII, etc
     * @param hl7Version           : The HL7 version of the message. Possible values : 2.3.1 or 2.5
     * @param messageType          : The message type of the message. Possible values : ADT^A01^ADT_A01, ADT^A04^ADT_A01 or ADT^A28^ADT_A05. The ADT^A28^ADT_A05 is only available with the HL7 v2.5 version and the
     *                             ADT^A01^ADT_A01 and ADT^A04^ADT_A01 with the HL7 v2.3.1 version.
     * @return response from specific port and host.
     * @throws JDOMException failed to generate random patient
     * @throws IOException   failed to generate random patient
     * @throws SOAPException exception
     */
    @WebMethod
    @WebResult(name = "Hl7MessageRespond")
    public String sendHl7Message(@WebParam(name = CC) String countryCode,
                                 @WebParam(name = "targetHost") String targetHost, @WebParam(name = "targetPort") Integer targetPort,
                                 @WebParam(name = "receivingApplication") String receivingApplication,
                                 @WebParam(name = "receivingFacility") String receivingFacility,
                                 @WebParam(name = "characterSet") String characterSet, @WebParam(name = "hl7Version") String hl7Version,
                                 @WebParam(name = "messageType") String messageType) throws JDOMException, IOException,
            SOAPException {
        Lifecycle.beginCall();
        System.setProperty(DOMImplementationRegistry.PROPERTY, org.apache.xerces.dom.DOMImplementationSourceImpl.class.getCanonicalName());

        EntityManager entityManager = (EntityManager) Component.getInstance(EM);
        String responseString = "";
        String currentIp = getIp();

        if (WebServicesSecurity.requestWsPermission(currentIp)) {
            String countryCodeKW = CountryCode.getCountryCodeKeyword(countryCode);
            if ((countryCodeKW == null || countryCodeKW.length() == 0)) {
                SOAPException soapEx = new SOAPException(
                        SOAP_MESSAGE_ERROR2);
                throw soapEx;
            }

            if ((targetHost.length() == 0)) {
                SOAPException soapEx = new SOAPException(
                        "You need to specify a hostname. Example : tatami.irisa.fr,  131.254.12.221.");
                throw soapEx;
            }

            if ((targetPort == null) || (targetPort == 0)) {
                SOAPException soapEx = new SOAPException(
                        "You need to specify a port number. Example : 3601, 2400, etc.");
                throw soapEx;
            }

            // To verify that the characterSet is available for the country chosen.
            List<CharacterSetEncoding> charsetList = getCharsetList(countryCodeKW);
            if ((characterSet == null) || (charsetList == null)) {
                SOAPException soapEx = new SOAPException(
                        "You need to specify a character set. Example : UNICODE UTF-8, ASCII, etc.");
                throw soapEx;
            }

            boolean testCharset = false;
            StringBuilder sb = new StringBuilder();
            testCharset = isTestCharset(characterSet, charsetList, testCharset, sb);

            if (!testCharset) {
                SOAPException soapEx = new SOAPException(
                        "You need to specify a valid character encoding for this country. Example : "
                                + sb.toString() + "You can see documentation on");
                throw soapEx;
            }

            if (!hl7Version.equals(V231) && !hl7Version.equals(V25)) {
                SOAPException soapEx = new SOAPException(
                        "You need to specify a valid HL7 version for message. Example : 2.3.1 or 2.5.");
                throw soapEx;
            }

            if (!messageType.equals(A01_231) && !messageType.equals(A04_231)
                    && !messageType.equals(A28_25)) {
                SOAPException soapEx = new SOAPException(
                        "You need to specify a valid HL7 message type. Example : ADT^A01^ADT_A01, ADT^A04^ADT_A01 or ADT^A28^ADT_A05.");
                throw soapEx;
            }

            if (hl7Version.equals(V231)
                    && !(messageType.equals(A01_231) || messageType.equals(A04_231))) {
                SOAPException soapEx = new SOAPException(
                        "If you choose the HL7 v2.3.1 version, you can only use the HL7 message types : ADT^A01^ADT_A01 or ADT^A04^ADT_A01.");
                throw soapEx;
            }

            if (hl7Version.equals(V25) && !messageType.equals(A28_25)) {
                SOAPException soapEx = new SOAPException(
                        "If you choose the HL7 v2.5 version, you can only use the HL7 message type : ADT^A28^ADT_A05.");
                throw soapEx;
            }
            if (!Contexts.isApplicationContextActive()) {
                Lifecycle.beginCall();
            }
            String messagetypeAndVersion = messageType + " (v" + hl7Version + ")";

            Message response;

            SystemConfig configMessage = new SystemConfig();
            configMessage.setReceivingApplication(receivingApplication);
            configMessage.setReceivingFacility(receivingFacility);

            AssigningAuthority authority = new AssigningAuthority();
            String apoid = ApplicationConfiguration.getValueOfVariable(DDS_OID);

            authority.setRootOID(apoid);
            String apdomaine = ApplicationConfiguration.getValueOfVariable(DDS_DOMAIN);

            authority.setName(apdomaine);
            authority.setKeyword(apdomaine);
            configMessage.setAuthority(authority);

            CountryCode country = CountryCode.getCountryCodeByKeyword(countryCodeKW);
            Patient patientWsHl7 = null;
            if (ApplicationConfigurationProvider.instance().getApplicationMode().equals(MINIMAL)) {
                patientWsHl7 = Patient.getRandomPatientFromDatabase(countryCodeKW);
            } else {
                patientWsHl7 = Patient.generateRandomPatient(countryCodeKW, true, true, true,
                        country.getGenerateReligion(), country.getGenerateRace(), true, true, null, null, null, null,
                        null, null, false, false, false, false, false, null, entityManager);

                patientWsHl7 = Patient.mergePatient(patientWsHl7, entityManager);
            }
            Message createdMessage = Hl7MessageGenerator.generate(messagetypeAndVersion, patientWsHl7, configMessage,
                    characterSet);

            Parser parser = PipeParser.getInstanceWithNoValidation();
            Connection connection = null;
            try {
                ConnectionHub connectionHub = ConnectionHub.getInstance();
                connection = connectionHub.attach(targetHost, targetPort, parser,
                        ExtendedMinLowerLayerProtocol.class);
                Initiator initiator = connection.getInitiator();
                initiator.setTimeoutMillis(10000);
                Message receivedMessage = initiator.sendAndReceive(createdMessage);
                connectionHub.detach(connection);
                parser = new DefaultXMLParser();
                responseString = parser.encode(receivedMessage);
                connection.close();

            } catch (Exception e) {
                log.error("" + e.getMessage());
                SOAPException soapEx = new SOAPException("Problem sending the HL7 message to the target :" + targetHost
                        + ":" + targetPort, e);
                throw soapEx;
            } finally {
                if (connection != null) {
                    connection.close();
                }
            }
            Lifecycle.endCall();
            return responseString;
        } else {
            SOAPException soapEx = new SOAPException(
                    "Request number limite exceeded ! Will be available again in a little while.");
            throw soapEx;
        }

    }

    /**
     * Method to return a HL7v3 Message containing description of a patient that correspond to the specified country code.
     *
     * @param countryCode : Code for the country we wish to get data. For Example : JP, FR, DE, US , etc
     * @return a HL7v3 Message containing description of a patient that correspond to the specified country code.
     * @throws JDOMException failed to generate random patient
     * @throws IOException   failed to generate random patient
     * @throws SOAPException country code not find in DB or request number limite exceeded
     */
    @WebMethod
    public String returnHl7v3Message(@WebParam(name = "countryCode1") String countryCode) throws SOAPException,
            JDOMException, IOException {
        Lifecycle.beginCall();

        String currentIp = getIp();

        if (WebServicesSecurity.requestWsPermission(currentIp)) {
            String countryCodeKW = CountryCode.getCountryCodeKeyword(countryCode);
            if ((countryCodeKW == null || countryCodeKW.length() == 0)) {
                SOAPException soapEx = new SOAPException(
                        SOAP_MESSAGE_ERROR2);
                throw soapEx;
            }

            SystemConfig configMessage = new SystemConfig();
            configMessage.setReceivingApplication("applicationExample");
            configMessage.setReceivingFacility("facilityExample");

            EntityManager em = (EntityManager) Component.getInstance(EM);
            AssigningAuthority authority = new AssigningAuthority();
            String apoid = ApplicationConfiguration.getValueOfVariable(DDS_OID);

            authority.setRootOID(apoid);
            String apdomaine = ApplicationConfiguration.getValueOfVariable(DDS_DOMAIN);

            authority.setName(apdomaine);
            authority.setKeyword(apdomaine);
            configMessage.setAuthority(authority);

            CountryCode country = CountryCode.getCountryCodeByKeyword(countryCodeKW);
            Patient patientWsHl7 = null;
            if (ApplicationConfigurationProvider.instance().getApplicationMode().equals(MINIMAL)) {
                patientWsHl7 = Patient.getRandomPatientFromDatabase(countryCodeKW);
            } else {
                patientWsHl7 = Patient.generateRandomPatient(countryCodeKW, true, true, true,
                        country.getGenerateReligion(), country.getGenerateRace(), true, true, null, null, null, null,
                        null, null, false, false, false, false, false, null, em);

                patientWsHl7 = Patient.mergePatient(patientWsHl7, em);
            }
            HL7v3MessageGenerator hv3gen = new HL7v3MessageGenerator();
            String createdMessage = hv3gen.generate(patientWsHl7, configMessage);
            Lifecycle.endCall();
            return createdMessage;
        } else {
            SOAPException soapEx = new SOAPException(
                    "Request number limite exceeded ! Will be again available in a little while.");
            throw soapEx;
        }

    }

    /**
     * Method to create and send a HL7v3 message to a specific port and host.
     *
     * @param countryCode          : Code for the country we wish to get data. For Example : JP, FR, DE, US , etc
     * @param systemName           : name for the system
     * @param url                  : the URL of your system
     * @param receivingApplication : the application of your system
     * @param receivingFacility    :the facility of your system
     * @return a HL7v3 message to send to a specific port and host.
     * @throws JDOMException failed to generate random patient
     * @throws IOException   failed to generate random patient
     * @throws SOAPException country code not find in DB or request number limite exceeded
     */
    @WebMethod
    @WebResult(name = "Hl7v3MessageRespond")
    public String sendHl7v3Message(@WebParam(name = CC) String countryCode,
                                   @WebParam(name = "systemName") String systemName, @WebParam(name = "url") String url,
                                   @WebParam(name = "receivingApplication") String receivingApplication,
                                   @WebParam(name = "receivingFacility") String receivingFacility) throws JDOMException,
            IOException, SOAPException {
        Lifecycle.beginCall();

        URLConnection connection;
        DataOutputStream printout;
        EntityManager entityManager = (EntityManager) Component.getInstance(EM);

        String countryCodeKW = CountryCode.getCountryCodeKeyword(countryCode);
        if ((countryCodeKW == null || countryCodeKW.length() == 0)) {
            SOAPException soapEx = new SOAPException(
                    SOAP_MESSAGE_ERROR2);
            throw soapEx;
        }

        CountryCode country = CountryCode.getCountryCodeByKeyword(countryCodeKW);
        Patient patientWsHl7 = null;
        if (ApplicationConfigurationProvider.instance().getApplicationMode().equals(MINIMAL)) {
            patientWsHl7 = Patient.getRandomPatientFromDatabase(countryCodeKW);
        } else {
            patientWsHl7 = Patient.generateRandomPatient(countryCodeKW, true, true, true,
                    country.getGenerateReligion(), country.getGenerateRace(), true, true, null, null, null, null, null,
                    null, false, false, false, false, false, null, entityManager);

            patientWsHl7 = Patient.mergePatient(patientWsHl7, entityManager);
        }
        SystemConfig configMessage = new SystemConfig();
        configMessage.setName(systemName);
        configMessage.setReceivingApplication(receivingApplication);
        configMessage.setReceivingFacility(receivingFacility);
        configMessage.setUrl(url);

        AssigningAuthority authority = new AssigningAuthority();
        String apoid = ApplicationConfiguration.getValueOfVariable(DDS_OID);

        authority.setRootOID(apoid);
        String apdomaine = ApplicationConfiguration.getValueOfVariable(DDS_DOMAIN);

        authority.setName(apdomaine);
        authority.setKeyword(apdomaine);
        configMessage.setAuthority(authority);
        HL7v3MessageGenerator hv3gen = new HL7v3MessageGenerator();
        String messageToSend = hv3gen.generate(patientWsHl7, configMessage);

        try {
            int timeout = Utils.getHL7v3TimeOut(entityManager);

            URL urlConf = new URL(configMessage.getUrl());
            connection = urlConf.openConnection();
            connection.setConnectTimeout(timeout);
            connection.setReadTimeout(timeout);
            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setUseCaches(false);
            connection.setRequestProperty("Content-Type", "application/soap+xml");
            printout = new DataOutputStream(connection.getOutputStream());
            printout.writeBytes(messageToSend);
            printout.flush();
            printout.close();
            Lifecycle.endCall();
            return messageToSend;
        } catch (MalformedURLException e) {
            log.error("" + e.getMessage());
        } catch (EOFException e) {
            log.error("" + e.getMessage());
        } catch (IOException e) {
            log.error("" + e.getMessage());
        }

        return messageToSend;
    }

    /**
     * @return list of country code
     */
    @WebMethod
    public List<CountryCode> getListCountryCode() {
        Lifecycle.beginCall();
        List<CountryCode> lcc;
        lcc = LastNameCountryCode.getCountryCodeList();
        for (CountryCode cc : lcc) {
            cc.setLanguage(null);
        }
        Lifecycle.endCall();
        return lcc;
    }

    private List<CharacterSetEncoding> getCharsetList(String countryCodeIso) {
        Lifecycle.beginCall();
        CountryCode cc = CountryCode.getCountryCodeByKeyword(countryCodeIso);

        try {
            if (countryCodeIso == null) {
                return null;
            }

            List<CharacterSetEncoding> charsetList = cc.getCharacterSetEncoding();
            Lifecycle.endCall();
            return charsetList;
        } catch (Exception e) {
            log.error("" + e.getMessage());
            return null;
        }
    }

}
