/*
 * Copyright 2009 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.dds.model;

import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.log.Log;

import javax.persistence.*;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * <b>Class Description :  </b>Person_dds<br><br>
 * This class describes a Person_dds object. It corresponds to a Person_dds generated through DemographicDataServer application.
 * It corresponds to a medical Information for a Patient.
 * <p>
 * Person_dds object possesses the following attributes :
 * <ul>
 * <li><b>id</b> : id corresponding to the Person_dds</li>
 * <li><b>firstNameCountryCode</b> : Object corresponding to a firstNameCountryCode for a person, with some informations (eg. firstname, codeCountry etc...)</li>
 * <li><b>lastName</b> : Object corresponding to the lastName of a person</li>
 * <li><b>race</b> : Object corresponding to the race of the person </li>
 * <li><b>religion</b> : Object corresponding to the religion of a person</li>
 * <li><b>dateOfBirth</b> : Object corresponding to the dateOfBirth of a person</li>
 * </ul>
 * <b>Example of Person_dds</b> : Person_dds(id, firstNameCountryCode, lastName, dateOfBirth, race, religion) = (1, 25, 56, 5, 6, 4) <br>
 *
 * @author Samii Ayed - Jean-Renan Chatel / INRIA Rennes IHE development Project
 * @version 1.0 - 2009, May 6th
 */

@Entity(name = "person")
@Name("person")
@Table(name = "dds_person", schema = "public")
@org.hibernate.annotations.Entity(selectBeforeUpdate = true)
@SequenceGenerator(name = "dds_person_sequence", sequenceName = "dds_person_id_seq", allocationSize = 1)


public class Person extends AuditModule implements java.io.Serializable {

    /**
     * Serial ID version of this object
     */
    private static final long serialVersionUID = -451234331541567890L;

    /**
     * Logger
     */
    @Logger
    private static Log log;


    //	Attributes (existing in database as a column)

    /**
     * Id of this Person_dds object
     */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "dds_person_sequence")
    private Integer id;

    /** FirstNameCountryCode object corresponding to this person */
    /**
     * Date object corresponding to this person
     */
    @Column(name = "date_of_birth")
    private Calendar dateOfBirth;

    @Column(name = "date_of_death")
    private Calendar dateOfDeath;

    //	 Variables used for Foreign Keys

    @ManyToOne
    @JoinColumn(name = "first_name_sex_id")
    private FirstNameSex firstNameSex;


    /**
     * LastName object corresponding to this person
     */
    @ManyToOne
    @JoinColumn(name = "last_name_id")
    private LastName lastName;

    @Column(name = "name_type")
    private String nameType;

    /**
     * LastName object corresponding to this person
     */
    @ManyToOne
    @JoinColumn(name = "mother_maiden_name_id")
    private LastName motherMaidenName;


    /**
     * Race object corresponding to this person
     */
    @ManyToOne
    @JoinColumn(name = "race_id")
    private Race race;

    /**
     * Sex object corresponding to this person
     */
    @ManyToOne
    @JoinColumn(name = "sex_id")
    private Sex sex;

    /**
     * Religion object corresponding to this person
     */
    @ManyToOne
    @JoinColumn(name = "religion_id")
    private Religion religion;

    //firstAlternativeName and lastAlternativeName : Is it very useful ???? In doubt, it is better to keep them.
    @Column(name = "first_alternative_name")
    private String firstAlternativeName;

    @Column(name = "last_alternative_name")
    private String lastAlternativeName;

    @ManyToMany
    @JoinTable(name = "dds_person_first_name_sex", joinColumns = @JoinColumn(name = "person_id"), inverseJoinColumns = @JoinColumn(name = "first_name_sex_id"))
    private List<FirstNameSex> otherFirstNameSexList;


    @OneToMany(fetch = FetchType.LAZY)
    private List<OtherName> otherNameList;

    @Column(name = "marital_status")
    private String maritalStatus;


    //	Constructors
    public Person() {

    }

    public Person(FirstNameSex firstNameSex, LastName lastName, LastName motherMaidenName, Calendar dateOfBirth, Race race, Religion religion) {
        this.firstNameSex = firstNameSex;
        this.lastName = lastName;
        this.motherMaidenName = motherMaidenName;
        this.dateOfBirth = dateOfBirth;
        this.race = race;
        this.religion = religion;
    }

    public Person(FirstNameSex firstNameSex, LastName lastName, Calendar dateOfBirth, Race race, Religion religion) {
        this.firstNameSex = firstNameSex;
        this.lastName = lastName;
        this.dateOfBirth = dateOfBirth;
        this.race = race;
        this.religion = religion;
    }

    public Person(Person person) {
        if (person.getFirstNameSex() != null) {
            this.firstNameSex = new FirstNameSex(person.getFirstNameSex());
        }
        if (person.getLastName() != null) {
            this.lastName = new LastName(person.getLastName());
        }
        if (person.getDateOfBirth() != null) {
            this.dateOfBirth = person.getDateOfBirth();
        }
        if (person.getRace() != null) {
            this.race = new Race(person.getRace());
        }
        if (person.getReligion() != null) {
            this.religion = new Religion(person.getReligion());
        }
    }

    // *********************************************************
    //  Getters and Setters : setup the DB columns properties
    // *********************************************************

    public static Person generateRandomPerson(
            String countryCode,
            boolean lastNameOption,
            boolean firstNameOption,
            boolean motherMaidenNameOption,
            boolean religionOption,
            boolean raceOption,
            boolean birthDayOption,
            Gender genderOption,
            String firstNameNearby,
            String lastNameNearby,
            String firstNameLike,
            String lastNameLike,
            String maritalSatusOption,
            boolean deadPatientOption,
            boolean maidenNameOption,
            boolean aliasNameOption,
            boolean displayNameOption,
            boolean newBornMotherOption,
            Patient motherPatient) {
        long debut = System.currentTimeMillis();
        log.debug("beginning of execution to generate a Person ");

        CountryCode cc = CountryCode.getCountryByCountryCode(countryCode);

        Person person = new Person();
        if (firstNameOption != false) {
            FirstNameCountryCode firstNameCountryCode = generateFirstName(
                    countryCode,
                    genderOption,
                    firstNameNearby,
                    firstNameLike);

            if (firstNameCountryCode != null) {
                person.setFirstNameSex(firstNameCountryCode.getFirstNameSex());
                if (cc.getIsLastNameSexed()) {
                    genderOption = firstNameCountryCode.getFirstNameSex().getGender();
                }
            }
        }

        person.setNameType("L");
        //	Legal Name

        //Generate the Additional first name sex
        if (cc != null
                && cc.getMinNumberOfPatientMiddleName() != null
                && cc.getMaxNumberOfPatientMiddleName() != null
                && cc.getMaxNumberOfPatientMiddleName() > 1
                && (cc.getMinNumberOfPatientMiddleName() <= cc.getMaxNumberOfPatientMiddleName())
                && cc.getMinNumberOfPatientMiddleName() >= 0
                && cc.getMaxNumberOfPatientMiddleName() > 0) {
            double a;

            if (cc.getMinNumberOfPatientMiddleName().equals(cc.getMaxNumberOfPatientMiddleName())) {
                a = cc.getMaxNumberOfPatientMiddleName();
            } else {
                a = Math.random();

                if (a >= 0.95) {
                    a = 1;
                }

                a = a * (cc.getMaxNumberOfPatientMiddleName());
                a = a + cc.getMinNumberOfPatientMiddleName();

                if (a > cc.getMaxNumberOfPatientMiddleName()) {
                    a = cc.getMaxNumberOfPatientMiddleName();
                }
            }

            Integer maxNumber = (int) a;
            List<FirstNameSex> firstNameSexs = new ArrayList<FirstNameSex>();

            for (int i = 0; i < maxNumber; i++) {
                firstNameSexs.add(Person.generateFirstName(
                        countryCode,
                        Gender.getRandomGenderBetweenMaleAndFemale(),
                        null,
                        null).getFirstNameSex());
            }
            person.setOtherFirstNameSexList(firstNameSexs);
        }

        if (lastNameOption != false) {
            LastNameCountryCode lastNameCountryCode = Person.generateLastName(
                    countryCode,
                    lastNameNearby,
                    lastNameLike,
                    genderOption);

            if (lastNameCountryCode != null) {
                person.setLastName(lastNameCountryCode.getLastName());
            }
        }

        if (motherMaidenNameOption) {
            Gender gdmother = null;
            if (cc.getIsLastNameSexed()) {
                gdmother = Gender.getGender_Female();
            }

            LastNameCountryCode lastNameCountryCode = Person.generateLastName(countryCode, lastNameNearby, null, gdmother);
            if (lastNameCountryCode != null) {
                person.setMotherMaidenName(lastNameCountryCode.getLastName());
            }
        }

        if (birthDayOption) {
            Calendar dateOfBirth = Person.getRandomDate(84);
            if (dateOfBirth != null) {
                person.setDateOfBirth(dateOfBirth);
            }
        }


        if (person.getFirstNameSex() != null) {
            Sex sex = Person.generateSex(countryCode, person.getFirstNameSex());
            if (sex != null) {
                person.setSex(sex);
            }
        }

        if (raceOption) {
            RaceByCountryCode raceByCountryCode = Person.generateRace(countryCode);
            if (raceByCountryCode != null) {
                person.setRace(raceByCountryCode.getRace());
            }
        }

        if (religionOption) {
            ReligionByCountryCode religionByCountryCode = Person.generateReligion(countryCode);
            if (religionByCountryCode != null) {
                person.setReligion(religionByCountryCode.getReligion());
            }
        }


        long tempsTotal = System.currentTimeMillis() - debut;
        log.debug("The time of execution of Person in millisecond is :  " + tempsTotal);


        //New options
        List<OtherName> otherNames = new ArrayList<OtherName>();

        if (maritalSatusOption != null && !maritalSatusOption.isEmpty()) {
            person.setMaritalStatus(maritalSatusOption);
        }

        if (maritalSatusOption != null && (maritalSatusOption.equals("M") || maritalSatusOption.equals("D")
                || maritalSatusOption.equals("S"))) {
            person.setDateOfBirth(getDateBetweenTwoAge(15, 100));
        }

        if (maidenNameOption) {
            LastNameCountryCode lastNameCountryCode = Person.generateLastName(
                    countryCode,
                    null,
                    null,
                    genderOption);

            OtherName otherName = new OtherName();

            if (lastNameCountryCode != null) {
                otherName.setLastName(lastNameCountryCode.getLastName());
            }

            otherName.setFirstNameSex(person.getFirstNameSex());

            otherName.setNameType("M");

            otherName = OtherName.saveOtherName(otherName);
            otherNames.add(otherName);
        }

        if (newBornMotherOption) {
            //If the Mother is not married
            //If married, should be the maiden name of the mother
            if (motherPatient.getPerson().getMaritalStatus() != null
                    && motherPatient.getPerson().getMaritalStatus().equals("M")) {
                List<OtherName> otherNamesForMother = motherPatient.getPerson().getOtherNameList();
                for (OtherName otherName : otherNamesForMother) {
                    if (otherName.getNameType().equals("M")) {
                        person.setMotherMaidenName(otherName.getLastName());
                    }
                }
            } else {
                person.setMotherMaidenName(person.getLastName());
            }

            //new born age < 2 days
            Calendar birthDate = Calendar.getInstance();
            Long date = (birthDate.getTimeInMillis() - 2 * 60 * 60 * 1000) - (birthDate.getTimeInMillis() - 48 * 60 * 60 * 1000);
            date = (long) (date * Math.random());
            date = (birthDate.getTimeInMillis() - 2 * 60 * 60 * 1000) - date;
            birthDate.setTimeInMillis(date);
            person.setDateOfBirth(birthDate);
        }

        if (deadPatientOption) {
            Calendar dateOfBirth = person.getDateOfBirth();
            Calendar dateOfDeath = Calendar.getInstance();
            Long date = dateOfDeath.getTimeInMillis() - (dateOfDeath.getTimeInMillis() - dateOfBirth.getTimeInMillis()) / 4;
            dateOfDeath.setTimeInMillis(date);

            person.setDateOfDeath(dateOfDeath);
        }

        if (aliasNameOption) {
            FirstNameCountryCode firstNameCountryCode = Person.generateFirstName(
                    countryCode,
                    genderOption,
                    null,
                    null);

            OtherName otherName = new OtherName();

            if (firstNameCountryCode != null) {
                otherName.setFirstNameSex(firstNameCountryCode.getFirstNameSex());

                if (cc.getIsLastNameSexed()) {
                    genderOption = firstNameCountryCode.getFirstNameSex().getGender();
                }
            }

            LastNameCountryCode lastNameCountryCode = Person.generateLastName(
                    countryCode,
                    null,
                    null,
                    genderOption);

            if (lastNameCountryCode != null) {
                otherName.setLastName(lastNameCountryCode.getLastName());
            }

            otherName.setNameType("A");
            otherName = OtherName.saveOtherName(otherName);
            otherNames.add(otherName);
        }


        if (displayNameOption) {
            FirstNameCountryCode firstNameCountryCode = Person.generateFirstName(
                    countryCode,
                    genderOption,
                    null,
                    null);

            OtherName otherName = new OtherName();

            if (firstNameCountryCode != null) {
                otherName.setFirstNameSex(firstNameCountryCode.getFirstNameSex());

                if (cc.getIsLastNameSexed()) {
                    genderOption = firstNameCountryCode.getFirstNameSex().getGender();
                }
            }

            LastNameCountryCode lastNameCountryCode = Person.generateLastName(
                    countryCode,
                    null,
                    null,
                    genderOption);

            if (lastNameCountryCode != null) {
                otherName.setLastName(lastNameCountryCode.getLastName());
            }

            otherName.setNameType("D");
            otherName = OtherName.saveOtherName(otherName);
            otherNames.add(otherName);
        }


        if (!otherNames.isEmpty()) {
            person.setOtherNameList(otherNames);
        }

        return person;

    }

    public static RaceByCountryCode generateRace(String countryCode) {
        long debut1 = System.currentTimeMillis();
        log.debug("beginning of execution Race ");


        RaceByCountryCode raceByCountryCode = RaceByCountryCode.generateRandomRaceByCountryCode(countryCode);

        if (raceByCountryCode != null && raceByCountryCode.getRace() != null && countryCode.equals("US")) {
            long tempsTotal = System.currentTimeMillis() - debut1;
            log.debug("The time of execution of Race in millisecond is :  " + tempsTotal);
            return raceByCountryCode;
        }
        return null;

    }

    public static ReligionByCountryCode generateReligion(String countryCode) {
        long debut2 = System.currentTimeMillis();
        log.debug("beginning of execution Religion ");

        ReligionByCountryCode religionByCountryCode = ReligionByCountryCode.generateRandomReligionByCountryCode(countryCode);
        if (religionByCountryCode != null && religionByCountryCode.getReligion() != null) {
            long tempsTotal = System.currentTimeMillis() - debut2;
            log.debug("The time of execution of Religion in millisecond is :  " + tempsTotal);
            return religionByCountryCode;
        }
        return null;
    }

    public static FirstNameCountryCode generateFirstName(String countryCode, Gender genderOption, String firstNameNearby, String firstNameLike) {
        log.debug("beginning of execution FirstName ");
        return FirstNameCountryCode.generateRandomFirstNameCountryCode(countryCode, genderOption, firstNameNearby, firstNameLike);
    }

    public static LastNameCountryCode generateLastName(String countryCode, String lastNameNearby, String lastNameLike, Gender gender) {
        return LastNameCountryCode.generateRandomLastNameCountryCode(countryCode, lastNameNearby, lastNameLike, gender);
    }

    public static Sex generateSex(String countryCode, FirstNameSex firstNameSex) {
        if (firstNameSex.getGender().equals(Gender.getGender_Male())) {
            return Sex.getSEX_MALE();
        } else if (firstNameSex.getGender().equals(Gender.getGender_Female())) {
            return Sex.getSEX_FEMALE();
        } else {
            return Sex.getSEX_OTHER();
        }
    }

    /**
     * Returns a random date between now and some years ago in the past.
     *
     * @param range : number of years for the range to search for the date
     * @return
     */
    public static Calendar getRandomDate(Integer range) {
        Calendar now = Calendar.getInstance();
        Calendar lowerBoundary = Calendar.getInstance();
        lowerBoundary.roll(Calendar.YEAR, -range);

        long val2 = now.getTimeInMillis();
        long val1 = lowerBoundary.getTimeInMillis();

        Random r = new Random();
        long randomTS = (long) (r.nextDouble() * (val2 - val1)) + val1;
        log.debug("the random is   " + randomTS + "val1= " + val1 + " val2= " + val2);
        Date d = new Date(randomTS);
        Calendar c = Calendar.getInstance();
        c.setTime(d);
        log.debug("the date is  " + d);
        return c;
    }

    //This method returns one Calendar between the beginYear to the endYear
    //For example, if you want to have a birth date for a person between 15 and 20 years old : getDateBetweenTwoAge(15,20)
    public static Calendar getDateBetweenTwoAge(int beginYear, int endYear) {
        Calendar birthDate = Calendar.getInstance();
        Calendar birthDateBegin = Calendar.getInstance();
        Calendar birthDateEnd = Calendar.getInstance();

        birthDateBegin.roll(Calendar.YEAR, -beginYear);
        birthDateEnd.roll(Calendar.YEAR, -endYear);

        Long date = birthDateBegin.getTimeInMillis() - ((long) ((birthDateBegin.getTimeInMillis()
                - birthDateEnd.getTimeInMillis()) * Math.random()));

        birthDate.setTimeInMillis(date);

        return birthDate;
    }

    public FirstNameSex getFirstNameSex() {
        return firstNameSex;
    }

    public void setFirstNameSex(FirstNameSex firstNameSex) {
        this.firstNameSex = firstNameSex;
    }

    public Integer getId() {
        return id;
    }

    public LastName getLastName() {
        return lastName;
    }

    public void setLastName(LastName lastName) {
        this.lastName = lastName;
    }

    public LastName getMotherMaidenName() {
        return motherMaidenName;
    }

    public void setMotherMaidenName(LastName motherMaidenName) {
        this.motherMaidenName = motherMaidenName;
    }

    public Race getRace() {
        return race;
    }

    public void setRace(Race race) {
        this.race = race;
    }

    public Religion getReligion() {
        return religion;
    }

    public void setReligion(Religion religion) {
        this.religion = religion;
    }

    public Calendar getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Calendar dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getFirstAlternativeName() {
        return firstAlternativeName;
    }

    public void setFirstAlternativeName(String firstAlternativeName) {
        this.firstAlternativeName = firstAlternativeName;
    }

    public String getLastAlternativeName() {
        return lastAlternativeName;
    }

    public void setLastAlternativeName(String lastAlternativeName) {
        this.lastAlternativeName = lastAlternativeName;
    }

    public List<FirstNameSex> getOtherFirstNameSexList() {
        return otherFirstNameSexList;
    }

    public void setOtherFirstNameSexList(List<FirstNameSex> otherFirstNameSexList) {
        this.otherFirstNameSexList = otherFirstNameSexList;
    }

    public String getNameType() {
        return nameType;
    }

    public void setNameType(String nameType) {
        this.nameType = nameType;
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public List<OtherName> getOtherNameList() {
        return otherNameList;
    }

    public void setOtherNameList(List<OtherName> otherNameList) {
        this.otherNameList = otherNameList;
    }

    public String getDateOfBirthAsString() {
        String result = new String();
        if (dateOfBirth != null) {
            final String DATE_FORMAT_NOW = "yyyy-MM-dd";
            SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
            result = sdf.format(dateOfBirth.getTime());
        }
        return result;
    }

    public String getDateOfDeathAsString() {
        String result = new String();
        if (dateOfDeath != null) {
            final String DATE_FORMAT_NOW = "yyyy-MM-dd";
            SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
            result = sdf.format(dateOfDeath.getTime());
        }
        return result;
    }

    public Calendar getDateOfDeath() {
        return dateOfDeath;
    }

    public void setDateOfDeath(Calendar dateOfDeath) {
        this.dateOfDeath = dateOfDeath;
    }

    public Sex getSex() {
        return sex;
    }

    public void setSex(Sex sex) {
        this.sex = sex;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((dateOfBirth == null) ? 0 : dateOfBirth.hashCode());
        result = prime * result
                + ((dateOfDeath == null) ? 0 : dateOfDeath.hashCode());
        result = prime
                * result
                + ((firstAlternativeName == null) ? 0 : firstAlternativeName
                .hashCode());
        result = prime * result
                + ((firstNameSex == null) ? 0 : firstNameSex.hashCode());
        result = prime
                * result
                + ((lastAlternativeName == null) ? 0 : lastAlternativeName
                .hashCode());
        result = prime * result
                + ((lastName == null) ? 0 : lastName.hashCode());
        result = prime
                * result
                + ((motherMaidenName == null) ? 0 : motherMaidenName.hashCode());
        result = prime * result + ((race == null) ? 0 : race.hashCode());
        result = prime * result
                + ((religion == null) ? 0 : religion.hashCode());
        result = prime * result + ((sex == null) ? 0 : sex.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Person other = (Person) obj;
        if (dateOfBirth == null) {
            if (other.dateOfBirth != null) {
                return false;
            }
        } else if (!dateOfBirth.equals(other.dateOfBirth)) {
            return false;
        }
        if (dateOfDeath == null) {
            if (other.dateOfDeath != null) {
                return false;
            }
        } else if (!dateOfDeath.equals(other.dateOfDeath)) {
            return false;
        }
        if (firstAlternativeName == null) {
            if (other.firstAlternativeName != null) {
                return false;
            }
        } else if (!firstAlternativeName.equals(other.firstAlternativeName)) {
            return false;
        }
        if (firstNameSex == null) {
            if (other.firstNameSex != null) {
                return false;
            }
        } else if (!firstNameSex.equals(other.firstNameSex)) {
            return false;
        }
        if (lastAlternativeName == null) {
            if (other.lastAlternativeName != null) {
                return false;
            }
        } else if (!lastAlternativeName.equals(other.lastAlternativeName)) {
            return false;
        }
        if (lastName == null) {
            if (other.lastName != null) {
                return false;
            }
        } else if (!lastName.equals(other.lastName)) {
            return false;
        }
        if (motherMaidenName == null) {
            if (other.motherMaidenName != null) {
                return false;
            }
        } else if (!motherMaidenName.equals(other.motherMaidenName)) {
            return false;
        }
        if (race == null) {
            if (other.race != null) {
                return false;
            }
        } else if (!race.equals(other.race)) {
            return false;
        }
        if (religion == null) {
            if (other.religion != null) {
                return false;
            }
        } else if (!religion.equals(other.religion)) {
            return false;
        }
        if (sex == null) {
            if (other.sex != null) {
                return false;
            }
        } else if (!sex.equals(other.sex)) {
            return false;
        }
        return true;
    }

    public String toString() {
        String res = "Firstname : ";
        if (this.getFirstNameSex() != null && this.getFirstNameSex().getFirstName() != null) {
            res = res + this.getFirstNameSex().getFirstName();
        }
        res = res + ". Lastname :";
        if (this.getLastName() != null && this.getLastName().getValue() != null) {
            res = res + this.getLastName().getValue();
        }

        return res;
    }

}
