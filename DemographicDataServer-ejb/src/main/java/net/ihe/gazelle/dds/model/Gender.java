/*
 * Copyright 2009 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.dds.model;

import net.ihe.gazelle.hql.restrictions.HQLRestrictionLikeMatchMode;
import org.jboss.seam.annotations.Name;

import javax.persistence.*;
import java.util.List;

/**
 * <b>Class Description :  </b>Gender<br><br>
 * This class describes a Gender object. It corresponds to a Gender generated through DemographicDataServer application.
 * It corresponds to a medical Information for a Patient.
 * <p>
 * Gender object possesses the following attributes :
 * <ul>
 * <li><b>id</b> : id corresponding to the Gender</li>
 * <li><b>value</b> : Object value corresponding to a Gender</li>
 * </ul>
 * <b>Example of Gender</b> : Gender(id, value) = (1, Male) <br>
 *
 * @author Abderrazek Boufahja - Samii Ayed - Jean-Renan Chatel / INRIA Rennes IHE development Project
 * @version 1.0 - 2009, May 6th
 */


@Entity
@Name("gender")
@Table(name = "dds_gender", schema = "public")
@org.hibernate.annotations.Entity(selectBeforeUpdate = true)
@SequenceGenerator(name = "dds_gender_sequence", sequenceName = "dds_gender_id_seq", allocationSize = 1)
public class Gender extends AuditModule implements java.io.Serializable {


    /**
     * Serial ID version of this object
     */
    private static final long serialVersionUID = -451234511576591760L;

    private final static String GENDER_MALE_STRING = "Male";
    private final static String GENDER_FEMALE_STRING = "Female";
    private final static String GENDER_MIXED_STRING = "Mixed";

    //	Attributes (existing in database as a column)

    /**
     * Id of this Gender object
     */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "dds_gender_sequence")
    private Integer id;

    /**
     * The value of this gender
     */
    @Column(name = "description")
    private String description;

    //	Constructors


    public Gender() {
    }

    public Gender(String description) {
        this.description = description;

    }

    public Gender(Gender gender) {
        if (gender != null) {
            this.description = gender.getDescription();
        }
    }

    // *********************************************************
    //  Getters and Setters : setup the DB columns properties
    // *********************************************************

    public static Gender getGender_Male() {
        return Gender.getGenderByDescription(GENDER_MALE_STRING);
    }

    public static Gender getGender_Female() {
        return Gender.getGenderByDescription(GENDER_FEMALE_STRING);
    }

    public static Gender getGender_Mixed() {
        return Gender.getGenderByDescription(GENDER_MIXED_STRING);
    }

    public static Gender getGenderByDescription(String gender) {
        if (gender == null || gender.isEmpty()) {
            return null;
        } else {
            GenderQuery query = new GenderQuery();
            if (gender.equalsIgnoreCase(GENDER_MIXED_STRING)) {
                query.description().eq(GENDER_MIXED_STRING);
            } else {
                query.description().like(gender, HQLRestrictionLikeMatchMode.START);
                query.description().neq(GENDER_MIXED_STRING);
            }
            return query.getUniqueResult();
        }
    }

    /**
     * This methods returns a list containing all the FirstNameSex object existing in the database
     *
     * @return list of all Gender objects
     */
    public static List<Gender> getAllGender() {
        GenderQuery query = new GenderQuery();
        return query.getList();
    }

    public static List<Gender> getAllGenderWithoutMixed() {
        GenderQuery query = new GenderQuery();
        query.description().neq(GENDER_MIXED_STRING);
        return query.getList();
    }

    public static Gender getRandomGenderWithoutMixed() {
        List<Gender> results = Gender.getAllGenderWithoutMixed();
        if (results != null) {
            int resultIndex = Patient.generateRandomInteger(0, results.size() - 1);
            return results.get(resultIndex);
        }
        return null;
    }

    public static Gender getRandomGender() {
        List<Gender> results = Gender.getAllGender();
        if (results != null) {
            int resultIndex = Patient.generateRandomInteger(0, results.size() - 1);
            return results.get(resultIndex);
        }
        return null;
    }

    public static String capitalize(String s) {
        if (s.length() == 0) {
            return s;
        }

        return s.substring(0, 1).toUpperCase() + s.substring(1).toLowerCase();
    }

    public static Gender getRandomGenderBetweenMaleAndFemale() {
        double a = Math.random();

        if (a > 0.5) {
            return getGender_Male();
        } else {
            return getGender_Female();
        }
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((description == null) ? 0 : description.hashCode());
        return result;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Gender other = (Gender) obj;
        if (description == null) {
            if (other.description != null) {
                return false;
            }
        } else if (!description.equals(other.description)) {
            return false;
        }
        return true;
    }

}
