/*
 * Copyright 2009 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.dds.model;

import org.jboss.seam.annotations.Name;

import javax.persistence.*;
import java.util.List;

/**
 * <b>Class Description :  </b>Language<br><br>
 * This class describes a Language object. It corresponds to a Language generated through DemographicDataServer application.
 * It corresponds to a medical Information for a Patient.
 * <p>
 * Language object possesses the following attributes :
 * <ul>
 * <li><b>id</b> : id corresponding to the Language</li>
 * <li><b>iso_639_1</b> : Object corresponding to the code iso_639_1 of the language</li>
 * <li><b>iso_639_2</b> : Object corresponding to the code iso_639_2 of the language</li>
 * <li><b>iso_639_3</b> : Object corresponding to the code iso_639_3 of the language</li>
 * <li><b>nativeName</b> : Object corresponding to the native name of the language</li>
 * <li><b>description</b> : Object corresponding to the description of the language</li>
 * <li><b>characterEncoding</b> : Object corresponding to the characterEncoding of a language</li>
 * <li><b>iso3166CountryCode</b> : Object corresponding to the code of country iso3166CountryCode for the language</li>
 * </ul>
 * <b>Example of Language</b> : Language(id, value, person) = (1, English, Latin) <br>
 *
 * @author Samii Ayed - Jean-Renan Chatel / INRIA Rennes IHE development Project
 * @version 1.0 - 2009, May 6th
 */

@Entity
@Name("language")
@Table(name = "dds_language", schema = "public")
@org.hibernate.annotations.Entity(selectBeforeUpdate = true)
@SequenceGenerator(name = "dds_language_sequence", sequenceName = "dds_language_id_seq", allocationSize = 1)

public class Language extends AuditModule implements java.io.Serializable {


    /**
     * Serial ID version of this object
     */
    private static final long serialVersionUID = -433311131500008790L;


    //	Attributes (existing in database as a column)

    /**
     * Id of this Language object
     */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "dds_language_sequence")
    private Integer id;


    /**
     * The code iso_639_1 of the language
     */
    @Column(name = "iso_639_1")
    private String iso_639_1;

    /**
     * The code iso_639_2 of the language
     */
    @Column(name = "iso_639_2")
    private String iso_639_2;

    /**
     * The code iso_639-3 of the language
     */
    @Column(name = "iso_639_3")
    private String iso_639_3;

    /**
     * The description of the language
     */
    @Column(name = "description")
    private String description;

    /**
     * The description of the language
     */
    @Column(name = "native_name")
    private String nativeName;

    /**
     * The CharacterEncoding object corresponding to this language
     */
    @OneToOne
    @JoinColumn(name = "character_encoding_id")
    private CharacterEncoding characterEncoding;

    /**
     * Language for this country
     */
    @ManyToMany
    @JoinTable(name = "dds_country_code_language", joinColumns = @JoinColumn(name = "language_id"), inverseJoinColumns = @JoinColumn(name = "country_code_id"))
    private List<CountryCode> countryCodeList;


    //	Constructors  //////////////////////////////////////////////////////

    public Language() {
    }

    public Language(String iso_639_1, String iso_639_2, String iso_639_3, CharacterEncoding characterEncoding, String description, String nativeName) {
        this.iso_639_1 = iso_639_1;
        this.iso_639_2 = iso_639_2;
        this.iso_639_3 = iso_639_3;
        this.characterEncoding = characterEncoding;
        this.description = description;
        this.nativeName = nativeName;
    }


    public Language(Language language) {
        if (language.iso_639_1 != null) {
            this.iso_639_1 = language.iso_639_1;
        }
        if (language.iso_639_2 != null) {
            this.iso_639_2 = language.iso_639_2;
        }
        if (language.iso_639_3 != null) {
            this.iso_639_3 = language.iso_639_3;
        }
        if (language.characterEncoding != null) {
            this.characterEncoding = language.characterEncoding;
        }
        if (language.description != null) {
            this.description = language.description;
        }
        if (language.nativeName != null) {
            this.nativeName = language.nativeName;
        }
    }

    // *********************************************************
    //  Getters and Setters : setup the DB columns properties
    // *********************************************************

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public CharacterEncoding getCharacterEncoding() {
        return characterEncoding;
    }

    public void setCharacterEncoding(CharacterEncoding characterEncoding) {
        this.characterEncoding = characterEncoding;
    }

    public String getIso_639_1() {
        return iso_639_1;
    }

    public void setIso_639_1(String iso_639_1) {
        this.iso_639_1 = iso_639_1;
    }

    public String getIso_639_2() {
        return iso_639_2;
    }

    public void setIso_639_2(String iso_639_2) {
        this.iso_639_2 = iso_639_2;
    }

    public String getIso_639_3() {
        return iso_639_3;
    }

    public void setIso_639_3(String iso_639_3) {
        this.iso_639_3 = iso_639_3;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getNativeName() {
        return nativeName;
    }

    public void setNativeName(String nativeName) {
        this.nativeName = nativeName;
    }

    public List<CountryCode> getCountryCodeList() {
        return countryCodeList;
    }

    public void setCountryCodeList(List<CountryCode> countryCodeList) {
        this.countryCodeList = countryCodeList;
    }

    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime
                * result
                + ((characterEncoding == null) ? 0 : characterEncoding
                .hashCode());
        result = prime * result
                + ((description == null) ? 0 : description.hashCode());
        result = prime * result
                + ((iso_639_1 == null) ? 0 : iso_639_1.hashCode());
        result = prime * result
                + ((iso_639_2 == null) ? 0 : iso_639_2.hashCode());
        result = prime * result
                + ((iso_639_3 == null) ? 0 : iso_639_3.hashCode());
        result = prime * result
                + ((nativeName == null) ? 0 : nativeName.hashCode());
        return result;
    }


    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Language other = (Language) obj;
        if (characterEncoding == null) {
            if (other.characterEncoding != null) {
                return false;
            }
        } else if (!characterEncoding.equals(other.characterEncoding)) {
            return false;
        }
        if (description == null) {
            if (other.description != null) {
                return false;
            }
        } else if (!description.equals(other.description)) {
            return false;
        }
        if (iso_639_1 == null) {
            if (other.iso_639_1 != null) {
                return false;
            }
        } else if (!iso_639_1.equals(other.iso_639_1)) {
            return false;
        }
        if (iso_639_2 == null) {
            if (other.iso_639_2 != null) {
                return false;
            }
        } else if (!iso_639_2.equals(other.iso_639_2)) {
            return false;
        }
        if (iso_639_3 == null) {
            if (other.iso_639_3 != null) {
                return false;
            }
        } else if (!iso_639_3.equals(other.iso_639_3)) {
            return false;
        }
        if (nativeName == null) {
            if (other.nativeName != null) {
                return false;
            }
        } else if (!nativeName.equals(other.nativeName)) {
            return false;
        }
        return true;
    }


}
