/*
 * Copyright 2009 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.dds.model;

import net.ihe.gazelle.dds.utils.RaceByCountryCodeQueryBuilder;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import org.jboss.seam.annotations.Name;

import javax.persistence.*;
import java.util.List;

/**
 * <b>Class Description :  </b>RaceByCountryCode<br><br>
 * This class describes a RaceByCountryCode object. It corresponds to a RaceByCountryCode generated through DemographicDataServer application.
 * It corresponds to a medical Information for a Patient.
 * <p>
 * RaceByCountryCode object possesses the following attributes :
 * <ul>
 * <li><b>id</b> : id corresponding to the RaceByCountryCode</li>
 * <li><b>iso</b> : the iso  corresponding to the RaceByCountryCode</li>
 * <li><b>frequency</b> : The frequency corresponding to a Race By Country</li>
 * </ul>
 * <b>Example of RaceByCountryCode</b> : RaceByCountryCode(id, iso, race,frequency ) = (1, FR, 5, 90%) <br>
 *
 * @author Abderrazek Boufahja - Samii Ayed - Jean-Renan Chatel / INRIA Rennes IHE development Project
 * @version 1.0 - 2009, May 6th
 */


@Entity
@Name("raceByCountryCode")
@Table(name = "dds_race_by_country_code", schema = "public")
@org.hibernate.annotations.Entity(selectBeforeUpdate = true)
@SequenceGenerator(name = "dds_race_by_country_code_sequence", sequenceName = "dds_race_by_country_code_id_seq", allocationSize = 1)
public class RaceByCountryCode extends AuditModule implements java.io.Serializable {


    /**
     * Serial ID version of this object
     */
    private static final long serialVersionUID = -76199912121291760L;

    //	Attributes (existing in database as a column)

    /**
     * Id of this RaceByCountryCode object
     */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "dds_race_by_country_code_sequence")
    private Integer id;

    /**
     * CountryCode object corresponding to this RaceByCountryCode
     */
    @ManyToOne
    @JoinColumn(name = "country_code_id")
    private CountryCode countryCode;

    /**
     * Race object corresponding to this RaceByCountryCode
     */
    @ManyToOne
    @JoinColumn(name = "race_id")
    private Race race;

    /**
     * The frequency of Race in this Country
     */
    @Column(name = "frequency")
    private Integer frequency;

    /**
     * The  accumulation for the frequency of the RaceByCountryCode
     */
    @Column(name = "cumul")
    private Integer cumul;


    //	Constructors

    public RaceByCountryCode() {

    }

    public RaceByCountryCode(CountryCode countryCode, Race race, Integer frequency) {
        this.race = race;
        this.frequency = frequency;
        this.countryCode = countryCode;
    }

    // *********************************************************
    //  Getters and Setters : setup the DB columns properties
    // *********************************************************

    public static List<Race> getRaceListForSpecificCountry(CountryCode country) {
        RaceByCountryCodeQuery query = new RaceByCountryCodeQuery();
        query.countryCode().eq(country);
        return query.race().getListDistinct();
    }

    public static RaceByCountryCode generateRandomRaceByCountryCode(String iso) {
        RaceByCountryCodeQueryBuilder query = new RaceByCountryCodeQueryBuilder();
        query.addEq("countryCode.iso", iso);
        Integer cumulRaceCountryCode = query.getMax("cumul");
        if (cumulRaceCountryCode != null) {
            int res = Patient.generateRandomInteger(1, cumulRaceCountryCode);
            query.addRestriction(HQLRestrictions.ge("cumul", res));
            cumulRaceCountryCode = query.getMin("cumul");
            query.addEq("cumul", cumulRaceCountryCode);
            return query.getUniqueResult();
        } else {
            return null;
        }
    }

    public CountryCode getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(CountryCode countryCode) {
        this.countryCode = countryCode;
    }

    public Race getRace() {
        return race;
    }

    public void setRace(Race race) {
        this.race = race;
    }

    public Integer getFrequency() {
        return frequency;
    }

    public void setFrequency(Integer frequency) {
        this.frequency = frequency;
    }

    public Integer getCumul() {
        return cumul;
    }

    public void setCumul(Integer cumul) {
        this.cumul = cumul;
    }

    public Integer getId() {
        return id;
    }

    // equals and hashcode ///////////////

    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((countryCode == null) ? 0 : countryCode.hashCode());
        result = prime * result + ((cumul == null) ? 0 : cumul.hashCode());
        result = prime * result
                + ((frequency == null) ? 0 : frequency.hashCode());
        result = prime * result + ((race == null) ? 0 : race.hashCode());
        return result;
    }


    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        RaceByCountryCode other = (RaceByCountryCode) obj;
        if (countryCode == null) {
            if (other.countryCode != null) {
                return false;
            }
        } else if (!countryCode.equals(other.countryCode)) {
            return false;
        }
        if (cumul == null) {
            if (other.cumul != null) {
                return false;
            }
        } else if (!cumul.equals(other.cumul)) {
            return false;
        }
        if (frequency == null) {
            if (other.frequency != null) {
                return false;
            }
        } else if (!frequency.equals(other.frequency)) {
            return false;
        }
        if (race == null) {
            if (other.race != null) {
                return false;
            }
        } else if (!race.equals(other.race)) {
            return false;
        }
        return true;
    }

}


