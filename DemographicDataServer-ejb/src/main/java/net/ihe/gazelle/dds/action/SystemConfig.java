package net.ihe.gazelle.dds.action;

import net.ihe.gazelle.dds.model.AssigningAuthority;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Pattern;
import java.util.List;


/**
 * <b>Class Description :  </b>SystemConfig<br><br>
 * This class describes a structure that is used in patientManagerBean to manage the configuration of the systems which will receive the message.
 *
 * @author Anne-Gaelle Berge / INRIA Rennes IHE development Project
 * @version 1.0 - 2010, March 11th
 *          <ul>
 *          <li><b>system</b> : The system to which the message is sent </li>
 *          <li><b>receivingApplication</b>: name of the receiving application, needed to fill MSH segment </li>
 *          <li><b>receivingFacility</b> : name of the receiving facility, needed to fill MSH segment </li>
 *          <li><b>hl7Version</b> : hl7 version supported by the actor played by this system</li>
 *          <li><b>Actor</b> : actor implemented by the system and to which refers the current configuration</li>
 *          <li><b>ip</b> : ip address of the machine hosting the system</li>
 *          <li><b>hostname</b> : hostname</li>
 *          <li><b>port</b> : port dedicated to HL7 message reception</li>
 *          <li><b>ack</b> : ack message receive by Gazelle once the message has been sent to the system</li>
 *          <li><b>ackCode</b> : acknowledgement code (AA, AE, AR ...) extracted from the message received</li>
 *          </ul>
 */

public class SystemConfig {

    public final static String _255 = "(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)";
    public final static String _stringIPRegex_ = "^(?:" + _255 + "\\.){3}" + _255 + "$";
    public final static String _portRegex = "[0-9]*";

    private String receivingApplication;
    private String receivingFacility;
    private Integer hl7Version;

    @Pattern(regexp = _stringIPRegex_, message = "This IP Address is not valid !")
    private String ip;

    private String hostname;

    @Min(value = 0, message = "This port is not valid ! Port possible values between 1 and 65535")
    @Max(value = 65535, message = "This port is not valid ! Port possible values between 1 and 65535")
    private Integer port;

    private List<MessageLog> messageLogList;
    private String name;
    private AssigningAuthority authority;

    // used in HL7v
    @Pattern(regexp = "(^$)|(?i)(^http://.+$|^https://.+$|^ftp://.+$)", message = "Please enter a valid URL (eg. http\\://www.ihe.net )")
    private String url;

    private String usage;
    private String OID;

    public SystemConfig() {

    }

    public String getIp() {
        return ip;
    }

    public void setIp(String inIp) {
        this.ip = inIp;
    }

    public String getReceivingApplication() {
        return receivingApplication;
    }

    public void setReceivingApplication(String receivingApplication) {
        this.receivingApplication = receivingApplication;
    }

    public String getReceivingFacility() {
        return receivingFacility;
    }

    public void setReceivingFacility(String receivingFacility) {
        this.receivingFacility = receivingFacility;
    }

    public Integer getHl7Version() {
        return hl7Version;
    }

    public void setHl7Version(Integer hl7Version) {
        this.hl7Version = hl7Version;
    }

    public String getHostname() {
        return hostname;
    }

    public void setHostname(String inHostName) {
        this.hostname = inHostName;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer inPort) {
        this.port = inPort;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public AssigningAuthority getAuthority() {
        return authority;
    }

    public void setAuthority(AssigningAuthority authority) {
        this.authority = authority;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUsage() {
        return usage;
    }

    public void setUsage(String usage) {
        this.usage = usage;
    }

    public String getOID() {
        return OID;
    }

    public void setOID(String oID) {
        OID = oID;
    }

    public List<MessageLog> getMessageLogList() {
        return messageLogList;
    }

    public void setMessageLogList(List<MessageLog> messageLogList) {
        this.messageLogList = messageLogList;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((OID == null) ? 0 : OID.hashCode());
        result = prime * result
                + ((hl7Version == null) ? 0 : hl7Version.hashCode());
        result = prime * result
                + ((hostname == null) ? 0 : hostname.hashCode());
        result = prime * result + ((ip == null) ? 0 : ip.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((port == null) ? 0 : port.hashCode());
        result = prime
                * result
                + ((receivingApplication == null) ? 0 : receivingApplication
                .hashCode());
        result = prime
                * result
                + ((receivingFacility == null) ? 0 : receivingFacility
                .hashCode());
        result = prime * result + ((url == null) ? 0 : url.hashCode());
        result = prime * result + ((usage == null) ? 0 : usage.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        SystemConfig other = (SystemConfig) obj;
        if (OID == null) {
            if (other.OID != null) {
                return false;
            }
        } else if (!OID.equals(other.OID)) {
            return false;
        }
        if (hl7Version == null) {
            if (other.hl7Version != null) {
                return false;
            }
        } else if (!hl7Version.equals(other.hl7Version)) {
            return false;
        }
        if (hostname == null) {
            if (other.hostname != null) {
                return false;
            }
        } else if (!hostname.equals(other.hostname)) {
            return false;
        }
        if (ip == null) {
            if (other.ip != null) {
                return false;
            }
        } else if (!ip.equals(other.ip)) {
            return false;
        }
        if (name == null) {
            if (other.name != null) {
                return false;
            }
        } else if (!name.equals(other.name)) {
            return false;
        }
        if (port == null) {
            if (other.port != null) {
                return false;
            }
        } else if (!port.equals(other.port)) {
            return false;
        }
        if (receivingApplication == null) {
            if (other.receivingApplication != null) {
                return false;
            }
        } else if (!receivingApplication.equals(other.receivingApplication)) {
            return false;
        }
        if (receivingFacility == null) {
            if (other.receivingFacility != null) {
                return false;
            }
        } else if (!receivingFacility.equals(other.receivingFacility)) {
            return false;
        }
        if (url == null) {
            if (other.url != null) {
                return false;
            }
        } else if (!url.equals(other.url)) {
            return false;
        }
        if (usage == null) {
            if (other.usage != null) {
                return false;
            }
        } else if (!usage.equals(other.usage)) {
            return false;
        }
        return true;
    }
}
