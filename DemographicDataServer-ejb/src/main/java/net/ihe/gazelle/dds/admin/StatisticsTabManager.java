package net.ihe.gazelle.dds.admin;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.Synchronized;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import java.io.Serializable;

@Stateful
@Name("statisticsTabManagerBean")
@Scope(ScopeType.SESSION)
@Synchronized(timeout = 10000)
public class StatisticsTabManager implements Serializable, StatisticsTabManagerLocal {

    private static final long serialVersionUID = -8761734853751657120L;

    public static final String USER_HISTORIC_TAB = "userHistoricIptab";

    public static final String USER_IP_TAB = "userIp";

    public static final String WEB_SERVICES_TAB = "wsInformation";


    @Destroy
    @Remove
    public void destroy() {
        // TODO Auto-generated method stub

    }

}
