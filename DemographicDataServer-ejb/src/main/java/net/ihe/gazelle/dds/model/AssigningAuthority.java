/*
 * Copyright 2010 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.dds.model;

import java.io.Serializable;


/**
 * <b>Class Description :  </b>AssigningAuthority
 * This class describes the AssigningAuthority object which is used to manage the authorities which attributes patient identifiers.
 * <p>
 * AssigningAuthority possesses the following attributes :
 * <ul>
 * <li><b>id</b> : id of the AssigningAuthority in database</li>
 * <li><b>keyword</b> : keyword</li>
 * <li><b>description</b> : short description of the authority</li>
 * </ul>
 */

public class AssigningAuthority extends AuditModule implements Serializable {


    /**
     *
     */
    private static final long serialVersionUID = -450911156581283760L;

    /**
     * Attributes (existing in database as a column)
     */


    private Integer id;

    private String keyword;

    private String name;

    private String description;

    private String rootOID;

    /**
     * Constructors
     */

    public AssigningAuthority() {

    }


    /**
     * Getters and Setters
     */

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRootOID() {
        return rootOID;
    }

    public void setRootOID(String rootOID) {
        this.rootOID = rootOID;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer inId) {
        this.id = inId;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String inKeyword) {
        this.keyword = inKeyword;
    }


    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((description == null) ? 0 : description.hashCode());
        result = prime * result + ((keyword == null) ? 0 : keyword.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((rootOID == null) ? 0 : rootOID.hashCode());
        return result;
    }


    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        AssigningAuthority other = (AssigningAuthority) obj;
        if (description == null) {
            if (other.description != null) {
                return false;
            }
        } else if (!description.equals(other.description)) {
            return false;
        }
        if (keyword == null) {
            if (other.keyword != null) {
                return false;
            }
        } else if (!keyword.equals(other.keyword)) {
            return false;
        }
        if (name == null) {
            if (other.name != null) {
                return false;
            }
        } else if (!name.equals(other.name)) {
            return false;
        }
        if (rootOID == null) {
            if (other.rootOID != null) {
                return false;
            }
        } else if (!rootOID.equals(other.rootOID)) {
            return false;
        }
        return true;
    }


    public String toString() {
        return "AssigningAuthority [description=" + description + ", id=" + id
                + ", keyword=" + keyword + ", name=" + name + ", rootOID="
                + rootOID + "]";
    }

}
