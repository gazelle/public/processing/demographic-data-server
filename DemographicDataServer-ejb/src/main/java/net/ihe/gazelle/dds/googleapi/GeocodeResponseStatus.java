package net.ihe.gazelle.dds.googleapi;

/**
 * <b>Class Description : </b>GeocodeResponseStatus<br>
 * <br>
 *
 * @author Anne-Gaelle Berge / IHE-Europe development Project
 * @version 1.0 - 12/12/16
 */
public enum GeocodeResponseStatus {
    OK,
    ZERO_RESULTS,
    OVER_QUERY_LIMIT,
    REQUEST_DENIED,
    INVALID_REQUEST,
    UNKNOWN_ERROR
}
