/*
 * Copyright 2009 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.dds.model;

import org.jboss.seam.annotations.Name;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * <b>Class Description :  </b>State<br><br>
 * This class describes a State object. It corresponds to a State of the Patient generated through DemographicDataServer application.
 * It corresponds to a medical Information for a Patient.
 * <p>
 * State object possesses the following attributes :
 * <ul>
 * <li><b>id</b> : id corresponding to the State</li>
 * <li><b>value</b> : Object corresponding to the name of State</li>
 * </ul></br>
 * <b>Example of State</b> : State(id, value) = (1, "California") <br>
 *
 * @package net.ihe.gazelle.demographic.model
 * @class State.java
 * @author Samii Ayed / INRIA Rennes IHE development Project
 * @see            > sayed@irisa.fr -  http://www.ihe-europe.org
 * @version 1.0 - 2009, May 6th
 */

@Entity
@Name("state")
@Table(name = "dds_state", schema = "public")
@org.hibernate.annotations.Entity(selectBeforeUpdate = true)
@SequenceGenerator(name = "dds_state_sequence", sequenceName = "dds_state_id_seq", allocationSize = 1)
public class State extends AuditModule implements java.io.Serializable {

    /**
     * Serial ID version of this object
     */
    private static final long serialVersionUID = -451234331541291760L;

    //	Attributes (existing in database as a column)

    /**
     * Id of this State object
     */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "dds_state_sequence")
    private Integer id;

    /**
     * The name of the state (eg. California)
     */
    @Column(name = "name")
    @Size(max = 200)
    private String name;


    /**
     * The code of the state (eg. CA)
     */
    @Column(name = "code")
    private String code;


    //	Constructors //////////////////////////////

    public State() {
    }

    public State(String name, String code) {
        super();
        this.name = name;
        this.code = code;
    }

    public State(State state) {
        if (state.name != null) {
            this.name = state.name;
        }
        if (state.code != null) {
            this.code = state.code;
        }
    }

    // getters and setters /////////////////////////

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    // hashcode and equals //////////////////////

    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((code == null) ? 0 : code.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        State other = (State) obj;
        if (code == null) {
            if (other.code != null) {
                return false;
            }
        } else if (!code.equals(other.code)) {
            return false;
        }
        if (name == null) {
            if (other.name != null) {
                return false;
            }
        } else if (!name.equals(other.name)) {
            return false;
        }
        return true;
    }

    // methods  /////////////////////////////

    public static List<State> getStateFiltered(String inCode) {
        StateQuery query = new StateQuery();
        if (inCode != null && !inCode.isEmpty()) {
            query.code().eq(inCode);
        }
        return query.getList();
    }


}
