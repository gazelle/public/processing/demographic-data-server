/*
 * Copyright 2010 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.dds.action;

import net.ihe.gazelle.dds.hl7.Hl7MessageGenerator;
import net.ihe.gazelle.dds.model.Patient;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.log.Log;
import org.jboss.seam.log.Logging;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Class Description :  PAtientIDGenerator
 * This class provides methods to build the patient Id function the patient country.
 *
 * @author Nicolas Lefebvre / INRIA Rennes IHE development Project
 * @version 1.0 - 2010, November 24th
 */
public class NationalPatientIdentifierGenerator {


    /**
     * Logger
     */
    @Logger
    private static Log log = Logging.getLog(Hl7MessageGenerator.class);


    public NationalPatientIdentifierGenerator() {

    }


    /**
     * Create the patient Id. It is the INS-C number.
     *
     * @param currentPatient patient
     * @return patientId
     */
    public static String generate(Patient currentPatient) {

        if (currentPatient == null || currentPatient.getAddress() == null
                || currentPatient.getAddress().isEmpty() || currentPatient.getAddress().get(0) == null) {
            log.info("This Patient or address is null ! It is impossible to generate ID");
            return null;
        } else if (currentPatient.getAddress().get(0).getCountry().getName().equals("FRANCE")) {
            return frenchPatientID(currentPatient);
        } else if (currentPatient.getAddress().get(0).getCountry().getName().equals("ITALY")) {
            return italianPatientID(currentPatient);
        } else if (currentPatient.getAddress().get(0).getCountry().getName().equals("DENMARK")) {
            return danishPatientID(currentPatient);
        } else if (currentPatient.getAddress().get(0).getCountry().getName().equals("BELGIUM")) {
            return belgianPatientID(currentPatient);
        } else {
            log.info("Patient ID is unknown for this country !");
            return null;
        }

    }


    /**
     * Create the patient Id for French patient. It is the INS-C number.
     *
     * @param currentPatient
     * @return patientId
     */
    private static String frenchPatientID(Patient currentPatient) {

        //Generate the NIR number :

        String patientNIR = "";

        if (currentPatient.getPerson().getSex() != null) {
            if (currentPatient.getPerson().getSex().getCode().equals("M")) {
                patientNIR = patientNIR + "1";
            } else if (currentPatient.getPerson().getSex().getCode().equals("F")) {
                patientNIR = patientNIR + "2";
            } else {
                return null;
            }
        } else {
            return null;
        }

        String birthDateY = "";
        String birthDateM = "";
        String birthDateD = "";
        if (currentPatient.getPerson().getDateOfBirth() != null) {
            Calendar birthDateCalendar = currentPatient.getPerson().getDateOfBirth();

            //Put the date in a correct format for the WebServices
            if (birthDateCalendar != null) {
                final String YEAR = "yyyy";
                final String MONTH = "MM";
                final String DAY = "dd";

                SimpleDateFormat sdfY = new SimpleDateFormat(YEAR);
                birthDateY = sdfY.format(birthDateCalendar.getTime());
                patientNIR = patientNIR + birthDateY.substring(2);

                SimpleDateFormat sdfM = new SimpleDateFormat(MONTH);
                birthDateM = sdfM.format(birthDateCalendar.getTime());
                patientNIR = patientNIR + birthDateM;

                SimpleDateFormat sdfD = new SimpleDateFormat(DAY);
                birthDateD = sdfD.format(birthDateCalendar.getTime());
            }
        } else {
            return null;
        }


        if (currentPatient.getAddress().get(0).getPostalCode() != null && !currentPatient.getAddress().get(0).getPostalCode().isEmpty()) {
            patientNIR = patientNIR + currentPatient.getAddress().get(0).getPostalCode().substring(0, 2);
        } else {
            return null;
        }


        //Generate a random number inferior to 800 for the Communal number.
        int communalNumber = (int) ((Math.random() * 1000));
        int i = 0;
        while (communalNumber > 800 && i < 10) {
            communalNumber = (int) ((Math.random() * 1000));
            i++;
        }

        if (communalNumber <= 800 && communalNumber >= 100) {
            patientNIR = patientNIR + communalNumber;
        } else if (communalNumber < 100) {
            patientNIR = patientNIR + "0" + communalNumber;
        } else {
            return null;
        }

        //Generate a random number inferior to 800 for the Birth act number.
        int birthActNumber = (int) ((Math.random() * 1000));
        i = 0;
        while (birthActNumber > 800 && i < 10) {
            birthActNumber = (int) ((Math.random() * 1000));
            i++;
        }

        if (birthActNumber <= 800 && birthActNumber >= 100) {
            patientNIR = patientNIR + birthActNumber;
        } else if (birthActNumber < 100) {
            patientNIR = patientNIR + "0" + birthActNumber;
        } else {
            return null;
        }

        //Calculate the NIR key. Not necessary to calculate the INS-C number.
        double keyNIR = Double.parseDouble(patientNIR);
        keyNIR = 97 - (keyNIR % 97);
        String keyNIRString = "";
        if (keyNIR < 10) {
            keyNIRString = "0" + (String.valueOf((int) keyNIR));
        } else {
            keyNIRString = (String.valueOf((int) keyNIR));
        }

        //Calculate the INS-C number.
        String firstName = "";
        if (currentPatient.getPerson().getFirstNameSex().getFirstName() != null) {
            firstName = currentPatient.getPerson().getFirstNameSex().getFirstName();
            firstName = normalizeFirstName(firstName);
        } else {
            return null;
        }


        //Creation of the seed
        String seed = firstName + birthDateY.substring(2) + birthDateM + birthDateD + patientNIR;
        if (seed.length() != 29) {
            log.error("seed has to be a string on 29 characters lenght");
            return null;
        }

        log.info("NIR : " + patientNIR + " " + keyNIRString);

        //get hash for seed
        String hashedSeed = "";
        if (hash(seed) != null) {
            hashedSeed = hash(seed);
        } else {
            return null;
        }

        //convert hash to a numeric value : get ins-c value
        String inscValue = covertHashToNumeric(hashedSeed);

        //Calculate the INS-C key.
        String keyINS = String.format("%02d", 97 - new BigInteger(inscValue).mod(new BigInteger("97")).longValue());

        //DDS-156: remove the extra space
        inscValue = inscValue + keyINS;

        log.info("INS-C : " + inscValue);

        return inscValue;

    }


    /**
     * Normalize First Name to calculate the INS-C Number, French Patient Id.
     *
     * @param firstName first name
     * @return preparedFirstName
     */
    public static String normalizeFirstName(String firstName) {

        String preparedFirstName = firstName.replaceAll("[Ã€ÃÃ‚ÃƒÃ„Ã…Ã†Ã Ã¡Ã¢Ã£Ã¦Ã¤Ã¥]", "A");
        preparedFirstName = preparedFirstName.replaceAll("[ÃˆÃ‰ÃŠÃ‹Ã¨Ã©ÃªÃ«]", "E");
        preparedFirstName = preparedFirstName.replaceAll("[ÃÃ°]", "D");
        preparedFirstName = preparedFirstName.replaceAll("[Ã’Ã“Ã”Ã•Ã–Ã˜Ã²Ã³Ã´ÃµÃ¶Ã¸]", "O");
        preparedFirstName = preparedFirstName.replaceAll("[Å¸ÃÃ½Ã¿]", "Y");
        preparedFirstName = preparedFirstName.replaceAll("[Ã‡Ã§]", "C");
        preparedFirstName = preparedFirstName.replaceAll("[ÃŒÃÃŽÃÃ¬Ã­Ã®Ã¯]", "I");
        preparedFirstName = preparedFirstName.replaceAll("[Ã‘Ã±]", "N");
        preparedFirstName = preparedFirstName.replaceAll("[Ã™ÃšÃ›ÃœÃ¹ÃºÃ»Ã¼]", "U");
        preparedFirstName = preparedFirstName.replaceAll("[ÃŸ]", "B");
        preparedFirstName = preparedFirstName.replaceAll("[Å’]", "OE");
        preparedFirstName = preparedFirstName.replaceAll("[Å Å¡]", "S");
        preparedFirstName = preparedFirstName.replaceAll("[Å¾Å½]", "Z");

        // every word no alaphanumeric
        preparedFirstName = preparedFirstName.replaceAll("[^a-zA-Z0-9 ]", " ");

        // remove spaces
        preparedFirstName = preparedFirstName.replace(" ", "");

        // upper case
        preparedFirstName = preparedFirstName.toUpperCase();

        // length operations
        if (preparedFirstName.length() < 10) {
            StringBuilder builder = new StringBuilder("          "); // ten spaces char
            builder.replace(0, preparedFirstName.length(), preparedFirstName);
            preparedFirstName = builder.toString();
        } else {
            preparedFirstName = preparedFirstName.substring(0, 10);
        }
        return preparedFirstName;

    }


    /**
     * Hash code to calculate the INS-C Number, French Patient Id.
     *
     * @param seed
     * @return hashedSeed
     */
    private static String hash(String seed) {
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            byte[] hash = md.digest(seed.getBytes());

            StringBuilder hashedSeed = new StringBuilder();
            for (byte b : hash) {
                hashedSeed.append(String.format("%02x", b));
            }
            return hashedSeed.toString();
        } catch (NoSuchAlgorithmException e) {
            log.error("Error while generating hash for seed" + e);
            return null;
        }
    }


    /**
     * Convert Hash to Numeric to calculate the INS-C Number, French Patient Id.
     *
     * @param hashedSeed
     * @return completeStringValue
     */
    private static String covertHashToNumeric(String hashedSeed) {
        String hexValue = hashedSeed.substring(0, 16);
        DecimalFormat df = new DecimalFormat("00000000000000000000");
        BigInteger numericValue = new BigInteger(hexValue, 16);
        String completeStringValue = numericValue.toString();
        if (completeStringValue.length() >= 20) {
            return completeStringValue.substring(0, 20);
        }
        return df.format(numericValue);
    }


    /**
     * Create the patient Id for Italian patient.
     *
     * @param currentPatient
     * @return patientId
     */
    private static String italianPatientID(Patient currentPatient) {
        String lastName = "";
        String firstName = "";
        String birthCity = "";
        String birthDate = "";
        String sex = "";

        if (currentPatient.getPerson().getLastName().getValue() != null) {
            lastName = currentPatient.getPerson().getLastName().getValue();
        } else {
            log.info("The patient LastName is missing to generate the pateint Id!");
            return null;
        }

        if (currentPatient.getPerson().getFirstNameSex().getFirstName() != null) {
            firstName = currentPatient.getPerson().getFirstNameSex().getFirstName();
        } else {
            log.info("The patient FirstName is missing to generate the pateint Id!");
            return null;
        }

        if (currentPatient.getAddress().get(0).getCity().getValue() != null) {
            birthCity = currentPatient.getAddress().get(0).getCity().getValue();
        } else {
            log.info("The patient City Address is missing to generate the pateint Id!");
            return null;
        }

        if (currentPatient.getPerson().getDateOfBirth() != null) {
            Calendar birthDateCalendar = currentPatient.getPerson().getDateOfBirth();

            //Put the date in a correct format for the WebServices
            if (birthDateCalendar != null) {
                final String DATE_FORMAT_NOW = "dd-MM-yyyy";
                SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
                birthDate = sdf.format(birthDateCalendar.getTime());
            }
        } else {
            log.info("The patient birth date is missing to generate the pateint Id!");
            return null;
        }

        if (currentPatient.getPerson().getSex() != null) {
            sex = currentPatient.getPerson().getSex().getCode();
        } else {
            log.info("The patient sex is missing to generate the pateint Id!");
            return null;
        }


        String urlIDIT = "http://webservices.dotnethell.it/codicefiscale.asmx/CalcolaCodiceFiscale?Nome=" + lastName + "&Cognome=" + firstName + "&ComuneNascita=" + birthCity + "&DataNascita=" + birthDate + "&Sesso=" + sex;

        SAXReader xmlReader = new SAXReader();
        try {
            Document document = xmlReader.read(urlIDIT);
            Node nodePostCode = document.selectSingleNode("//string");

            log.info("Italian Patient ID  " + nodePostCode.getText());

            if (!(nodePostCode.getText().equals("Error")) && !(nodePostCode.getText().equals("Il comune di nascita è inesistente!")) && nodePostCode.getText().length() == 16) {
                return nodePostCode.getText();
            } else {
                log.info("Error with the Italian Patient ID generation ! Message Error : " + nodePostCode.getText());
                return null;
            }

        } catch (DocumentException e) {
            // TODO Auto-generated catch block
            log.error("Error with the Italian Patient ID generation : " + e);
            return null;
        }

    }


    /**
     * Create the patient Id for Danish patient.
     *
     * @param currentPatient
     * @return patientId
     */
    private static String danishPatientID(Patient currentPatient) {
        String numberCPR = "";

        String birthDate = "";
        //Put the 6 first number
        if (currentPatient.getPerson().getDateOfBirth() != null) {
            Calendar birthDateCalendar = currentPatient.getPerson().getDateOfBirth();

            //Put the date in a correct format
            if (birthDateCalendar != null) {
                final String DATE_FORMAT_NOW = "ddMMyyyy";
                SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
                birthDate = sdf.format(birthDateCalendar.getTime());
            }
        } else {
            log.info("The patient birth date is missing to generate the pateint Id!");
            return null;
        }


        numberCPR = birthDate.substring(0, 4) + birthDate.substring(6);

        //Put the number 6 and 7, random numbers
        int randomNumber = (int) ((Math.random() * 1000));

        if (randomNumber < 10) {
            numberCPR = numberCPR + "00" + randomNumber;
        } else if (randomNumber < 100) {
            numberCPR = numberCPR + "0" + randomNumber;
        } else if (randomNumber < 1000) {
            numberCPR = numberCPR + randomNumber;
        } else {
            return null;
        }

        //Put the last number, it is odd for males and even for females.
        int randomNumberSex = (int) ((Math.random() * 10));
        if (randomNumberSex == 0) {
            randomNumberSex++;
        } else if (randomNumberSex == 9) {
            randomNumberSex--;
        }

        if (currentPatient.getPerson().getSex() != null) {
            if (currentPatient.getPerson().getSex().getCode().equals("M") && randomNumberSex % 2 == 0) {
                randomNumberSex++;
            } else if (currentPatient.getPerson().getSex().getCode().equals("F") && randomNumberSex % 2 != 0) {
                randomNumberSex++;
            }
        } else {
            return null;
        }


        numberCPR = numberCPR + randomNumberSex;

        log.info("Danish Patient Id : " + numberCPR);

        return numberCPR;
    }


    /**
     * Create the patient Id for Belgian patient.
     *
     * @param currentPatient
     * @return patientId
     */
    private static String belgianPatientID(Patient currentPatient) {
        String nationalNumber = "";

        String birthDate = "";
        //Put the 6 first number
        if (currentPatient.getPerson().getDateOfBirth() != null) {
            Calendar birthDateCalendar = currentPatient.getPerson().getDateOfBirth();

            //Put the date in a correct format
            if (birthDateCalendar != null) {
                final String DATE_FORMAT_NOW = "yyyyMMdd";
                SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
                birthDate = sdf.format(birthDateCalendar.getTime());
            }
        } else {
            log.info("The patient birth date is missing to generate the pateint Id!");
            return null;
        }

        nationalNumber = birthDate.substring(2);


        //Put the serial number, it is used so that men get the odd numbers, while women get the even numbers
        int randomNumberSex = (int) ((Math.random() * 1000));
        if (randomNumberSex == 0) {
            randomNumberSex++;
        } else if (randomNumberSex == 999) {
            randomNumberSex--;
        }

        if (currentPatient.getPerson().getSex() != null) {
            if (currentPatient.getPerson().getSex().getCode().equals("M") && randomNumberSex % 2 == 0) {
                randomNumberSex++;
            } else if (currentPatient.getPerson().getSex().getCode().equals("F") && randomNumberSex % 2 != 0) {
                randomNumberSex++;
            }
        } else {
            return null;
        }


        if (randomNumberSex < 10) {
            nationalNumber = nationalNumber + "00" + randomNumberSex;
        } else if (randomNumberSex < 100) {
            nationalNumber = nationalNumber + "0" + randomNumberSex;
        } else if (randomNumberSex < 1000) {
            nationalNumber = nationalNumber + randomNumberSex;
        } else {
            return null;
        }


        //Calculate the checksum.
        double checksum;
        if (birthDate.charAt(0) != '1') {
            checksum = Double.parseDouble(birthDate.charAt(0) + nationalNumber);
        } else {
            checksum = Double.parseDouble(nationalNumber);
        }

        checksum = 97 - (checksum % 97);
        if (checksum < 10) {
            nationalNumber = nationalNumber + " " + "0" + (String.valueOf((int) checksum));
        } else {
            nationalNumber = nationalNumber + " " + (String.valueOf((int) checksum));
        }


        log.info("Belgian Patient Id : " + nationalNumber);

        nationalNumber = nationalNumber.substring(0, 6) + " " + nationalNumber.substring(6);

        return nationalNumber;
    }


}
