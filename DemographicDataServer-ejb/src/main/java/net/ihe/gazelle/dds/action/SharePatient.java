package net.ihe.gazelle.dds.action;

import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.app.Connection;
import ca.uhn.hl7v2.app.ConnectionHub;
import ca.uhn.hl7v2.app.Initiator;
import ca.uhn.hl7v2.llp.ExtendedMinLowerLayerProtocol;
import ca.uhn.hl7v2.llp.LLPException;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.parser.*;
import ca.uhn.hl7v2.validation.impl.NoValidation;
import net.ihe.gazelle.common.tree.XmlTreeDataBuilder;
import net.ihe.gazelle.dds.hl7.HL7v3MessageGenerator;
import net.ihe.gazelle.dds.hl7.Hl7MessageGenerator;
import net.ihe.gazelle.dds.model.ApplicationConfiguration;
import net.ihe.gazelle.dds.model.AssigningAuthority;
import net.ihe.gazelle.dds.model.CharacterSetEncoding;
import net.ihe.gazelle.dds.model.Patient;
import net.ihe.gazelle.dds.utils.Utils;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.richfaces.model.TreeNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.bootstrap.DOMImplementationRegistry;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

@Name("sharePatientManager")
@Scope(ScopeType.PAGE)
public class SharePatient implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -8542235785397060482L;
    private static final String IP_REGEX = "^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)";
    private static final String messageType_A01_231 = "ADT^A01^ADT_A01 (v2.3.1)";
    private static final String messageType_A04_231 = "ADT^A04^ADT_A01 (v2.3.1)";
    private static final String messageType_A28_25 = "ADT^A28^ADT_A05 (v2.5)";
    private static final String messageType_3 = "PRPA_IN201301UV02 (v3)";
    private static Logger log = LoggerFactory.getLogger(SharePatient.class);
    private List<Patient> selectedPatientList;
    private String selectedMessageType;
    private String stringMessage;
    private boolean displayMessage = false;
    private String selectedMessageToDisplay;
    private SystemConfig manualConfig;
    private SystemConfig selectedConfig;
    private List<SystemConfig> configs;
    private boolean displaySystemTable = false;
    private boolean displayAckList = false;
    private CharacterSetEncoding selectedCharset = null;
    private boolean displayWarningCharacterSet = false;
    private String xmlMessageContent = null;
    private TreeNode xmlTreeData = null;

    private static boolean isReachable(String addr, int openPort, int timeOutMillis) {
        // Any Open port on other machine
        // openPort =  22 - ssh, 80 or 443 - webserver, 25 - mailserver etc.
        try {
            try (Socket soc = new Socket()) {
                soc.connect(new InetSocketAddress(addr, openPort), timeOutMillis);
                soc.close();
            }
            return true;
        } catch (IOException ex) {
            return false;
        }
    }

    public SystemConfig getSelectedConfig() {
        return selectedConfig;
    }

    public void setSelectedConfig(SystemConfig selectedConfig) {
        this.selectedConfig = selectedConfig;
    }

    @SuppressWarnings("unchecked")
    @Create
    public void init() {
        selectedPatientList = (List<Patient>) Component.getInstance("patientsToShare");
    }

    public void resetMessage() {
        this.stringMessage = null;
        this.displayMessage = false;
        this.displaySystemTable = false;
        this.displayAckList = false;
        this.configs = new ArrayList<SystemConfig>();
        this.manualConfig = new SystemConfig();
    }

    public String shortenMessage(String inMessage, int numberOfCharactToDisplay) {
        if ((inMessage == null) || (inMessage.length() == 0)) {
            return null;
        }
        if (inMessage.length() < numberOfCharactToDisplay) {
            return inMessage;
        } else {
            String messageShort = inMessage.substring(0, (numberOfCharactToDisplay - 1));
            messageShort = messageShort + "...";
            return messageShort;
        }

    }

    public void addManualConfig() {
        for (SystemConfig systemConfig : getConfigs()) {
            if (systemConfig.equals(manualConfig)) {
                FacesMessages.instance().addFromResourceBundle(StatusMessage.Severity.ERROR, "gazelle.dds.ThisConfigurationHasAlreadyBeenAdded");
                return;
            }
        }

        if (manualConfig == null) {
            return;
        }

        if (selectedMessageType.equals(messageType_3)) {
            if (manualConfig.getUrl() == null) {
                FacesMessages.instance().addFromResourceBundle(StatusMessage.Severity.ERROR, "gazelle.dds.AnyReceiverURL");
                return;
            }
        } else {
            if ((manualConfig.getIp() == null) || (manualConfig.getPort() == null)) {
                FacesMessages.instance().addFromResourceBundle(StatusMessage.Severity.ERROR, "gazelle.dds.AnyIpOrPort");
                return;
            }
        }
        if ((manualConfig.getReceivingApplication() == null) || (manualConfig.getReceivingFacility() == null)) {
            FacesMessages.instance().addFromResourceBundle(StatusMessage.Severity.ERROR, "gazelle.dds.AnyReceivingApplicationOrFacitity");
            return;
        }

        // validate IP address
        if (manualConfig.getIp() != null) {
            Pattern ipPattern = Pattern.compile(IP_REGEX);
            if (!ipPattern.matcher(manualConfig.getIp()).matches()) {
                FacesMessages.instance().addFromResourceBundle(StatusMessage.Severity.ERROR, "gazelle.dds.InvalidIp");
                return;
            }
        }

        AssigningAuthority ddsAa = this.getDDSAssigningAuthority();
        manualConfig.setAuthority(ddsAa);
        getConfigs().add(manualConfig);
        manualConfig = new SystemConfig();
        displaySystemTable = true;
    }

    private AssigningAuthority getDDSAssigningAuthority() {
        AssigningAuthority aa = new AssigningAuthority();
        String apdomain = ApplicationConfiguration.getValueOfVariable("DDS_domain");

        aa.setName(apdomain);
        aa.setKeyword(apdomain);
        log.info("apdomaine = " + apdomain);
        String apoid = ApplicationConfiguration.getValueOfVariable("DDS_OID");

        aa.setRootOID(apoid);
        log.info("apoid = " + apoid);
        return aa;
    }

    public void emptyConfigList() {
        configs = new ArrayList<SystemConfig>();
    }

    public void removeConfigFromList(SystemConfig inConfig) {
        if ((inConfig != null) && (configs != null)) {
            configs.remove(inConfig);
        }
    }

    public void removeConfigFromList() {
        if ((selectedConfig != null) && (configs != null)) {
            configs.remove(selectedConfig);
        }
    }

    private boolean isValidConfigs() {
        ArrayList<Boolean> res = new ArrayList<Boolean>();
        for (SystemConfig config : configs) {
            try {
                if (config.getIp() == null) {
                    FacesMessages.instance().add(StatusMessage.Severity.ERROR,
                            "Your IP seems to be null, please update it");
                    res.add(false);
                } else if (config.getPort() == null) {
                    FacesMessages.instance().add(StatusMessage.Severity.ERROR,
                            "Your Port seems to be null, please update it");
                    res.add(false);
                } else if (isReachable(config.getIp(), config.getPort(), 5000)) {
                    res.add(true);
                } else {
                    FacesMessages.instance().add(StatusMessage.Severity.ERROR,
                            "Your configuration (" + config.getName() + ") is not reachable, please update it");
                    res.add(false);
                }
            } catch (Exception e) {
                log.error("Failed to connect to this config , please verify it");
            }
        }
        if (res.isEmpty()) {
            return false;
        } else if (!res.isEmpty() && res.contains(false)) {
            return false;
        } else {
            return true;
        }
    }

    public void sendPatientToSystems() {
        System.setProperty(DOMImplementationRegistry.PROPERTY, org.apache.xerces.dom.DOMImplementationSourceImpl.class.getCanonicalName());
        if ((configs != null) && (configs.size() > 0)) {

            if (selectedMessageType.equals(messageType_3)) {
                URL url;
                URLConnection connection;
                DataOutputStream printout;
                DataInputStream input;

                for (SystemConfig conf : configs) {
                    List<MessageLog> messageLogList = new ArrayList<MessageLog>();

                    for (Patient selectedPatient : getSelectedPatientList()) {
                        MessageLog messageLog = new MessageLog();
                        selectedPatient = Patient.getPatientById(selectedPatient.getId());
                        HL7v3MessageGenerator hv3gen = new HL7v3MessageGenerator();
                        String messageToSend = hv3gen.generate(selectedPatient, conf);

                        messageLog.setSentMessage(messageToSend);
                        try {
                            EntityManager entityManager = (EntityManager) Component.getInstance("entityManager");
                            int timeout = Utils.getHL7v3TimeOut(entityManager);

                            url = new URL(conf.getUrl());
                            connection = url.openConnection();
                            connection.setConnectTimeout(timeout);
                            connection.setReadTimeout(timeout);
                            connection.setDoInput(true);
                            connection.setDoOutput(true);
                            connection.setUseCaches(false);
                            connection.setRequestProperty("Content-Type", "application/soap+xml");
                            printout = new DataOutputStream(connection.getOutputStream());
                            printout.writeBytes(messageToSend);
                            printout.flush();
                            printout.close();
                            input = new DataInputStream(connection.getInputStream());
                            if (input != null) {
                                String response = null;
                                StringBuilder sb = new StringBuilder();
                                while (((response = input.readLine())) != null) {
                                    sb.append("\n");
                                    sb.append(response);
                                }
                                messageLog.setAck(sb.toString());
                                input.close();
                            }

                            messageLogList.add(messageLog);

                        } catch (MalformedURLException e) {
                            log.error("bad URL" + e.getMessage());
                            messageLogList.add(messageLog);
                        } catch (EOFException e) {
                            log.error("reach EOF before the end !");
                            messageLogList.add(messageLog);
                        } catch (IOException e) {
                            log.error("error when sending message " + e.getMessage());
                            messageLogList.add(messageLog);
                        }
                    }

                    conf.setMessageLogList(messageLogList);
                }
            } else {
                if (isValidConfigs() && (selectedMessageType.equals(messageType_A01_231) ||
                        selectedMessageType.equals(messageType_A04_231) ||
                        selectedMessageType.equals(messageType_A28_25))) {
                    Parser parser = PipeParser.getInstanceWithNoValidation();

                    String targetHost = "";
                    for (SystemConfig conf : configs) {
                        List<MessageLog> messageLogList = new ArrayList<MessageLog>();

                        for (Patient selectedPatient : getSelectedPatientList()) {
                            MessageLog messageLog = new MessageLog();
                            selectedPatient = Patient.getPatientById(selectedPatient.getId());
                            if ((selectedMessageType != null) &&
                                    (selectedPatient != null)) {
                                Message messageToSend = Hl7MessageGenerator.generate(
                                        selectedMessageType,
                                        selectedPatient,
                                        conf,
                                        this.selectedCharset.getHl7Charset());

                                if (messageToSend == null) {
                                    messageLog.setSentMessage("The application has been unabled to create a message");
                                    log.error("Enable to create message");
                                    continue;
                                }
                                Connection connection = null;
                                try {
                                    messageLog.setSentMessage(parser.encode(messageToSend));
                                    log.info("-- target : " + conf.toString());
                                    if (conf.getIp() != null) {
                                        targetHost = conf.getIp();
                                    } else {
                                        targetHost = conf.getHostname();
                                    }
                                    // The connection hub connects to listening servers
                                    ConnectionHub connectionHub = ConnectionHub.getInstance();
                                    connection = connectionHub.attach(targetHost, conf.getPort(), parser,
                                            ExtendedMinLowerLayerProtocol.class);
                                    Initiator initiator = connection.getInitiator();
                                    initiator.setTimeoutMillis(10000);
                                    Message receivedMessage = initiator.sendAndReceive(messageToSend);
                                    connectionHub.detach(connection);

                                    List<String> ackInformation = validateMessage(receivedMessage);

                                    messageLog.setAckCode(ackInformation.get(0));
                                    messageLog.setAck(ackInformation.get(1));

                                    messageLogList.add(messageLog);

                                } catch (HL7Exception e) {
                                    log.error("HL7Exception when sending message to " + targetHost + " on port " + conf.getPort() + ": " + e.getMessage());
                                    FacesMessages.instance().add(StatusMessage.Severity.ERROR, targetHost + ":" + conf.getPort() + ": " + e.getMessage());
                                    messageLogList.add(messageLog);
                                } catch (IOException e) {
                                    log.error("IOException when sending message to " + targetHost + " on port " + conf.getPort() + ": " + e.getMessage());
                                    FacesMessages.instance().add(StatusMessage.Severity.ERROR, targetHost + ":" + conf.getPort() + ": " + e.getMessage());
                                    messageLogList.add(messageLog);
                                } catch (LLPException e) {
                                    log.error("LLPException when sending message to " + targetHost + " on port " + conf.getPort() + ": " + e.getMessage());
                                    FacesMessages.instance().add(StatusMessage.Severity.ERROR, targetHost + ":" + conf.getPort() + ": " + e.getMessage());
                                    messageLogList.add(messageLog);
                                } finally {
                                    if (connection != null) {
                                        connection.close();
                                    }
                                }
                            }
                        }
                        conf.setMessageLogList(messageLogList);
                    }
                } else {
                    log.error("Config not reachable ");
                    return;
                }
            }
            displayAckList = true;
        } else {
            log.error("Config list is empty");
            return;
        }

        log.info("******************Configuration message Size : " + configs.get(0).getMessageLogList().size());
    }


    private List<String> validateMessage(Message inMessage) {
        Parser parser = new PipeParser();
        List<String> ackInformation = new ArrayList<String>();
        try {
            if (inMessage instanceof ca.uhn.hl7v2.model.v231.message.ACK) {
                ca.uhn.hl7v2.model.v231.message.ACK msg = (ca.uhn.hl7v2.model.v231.message.ACK) inMessage;
                ackInformation.add(msg.getMSA().getAcknowledgementCode().getValue());
                ackInformation.add(parser.encode(msg));
            }
            if (inMessage instanceof ca.uhn.hl7v2.model.v25.message.ACK) {
                ca.uhn.hl7v2.model.v25.message.ACK msg = (ca.uhn.hl7v2.model.v25.message.ACK) inMessage;
                ackInformation.add(msg.getMSA().getAcknowledgmentCode().getValue());
                ackInformation.add(parser.encode(msg));
            }
        } catch (HL7Exception e) {
            log.error("error when parsing acknoledgments");
        }

        return ackInformation;
    }

    public void printSummary() {
        if ((configs == null) || (configs.size() == 0)) {
            FacesMessages.instance().addFromResourceBundle(StatusMessage.Severity.ERROR, "gazelle.dds.AnyDataToPrint");
            return;
        }
        StringBuilder sb = new StringBuilder();
        sb.append("Hl7 Message Type: ");
        sb.append(selectedMessageType);
        sb.append("\n\n");

        for (SystemConfig conf : configs) {
            for (MessageLog messageLog : conf.getMessageLogList()) {
                StringBuilder sb2 = new StringBuilder();
                sb2.append(conf.getName());
                sb2.append("\n\n");
                sb2.append("sent message: \n");
                sb2.append(messageLog.getSentMessage());
                sb.append("\n\n");

                if (messageLog.getAck() != null) {
                    sb2.append("received message: \n");
                    sb2.append(messageLog.getAck());
                    sb2.append("\n");
                } else {
                    sb2.append("no acknowledgement received: sending may have failed\n");
                }

                sb.append("\n---------------------------------------------\n");
                sb.append(sb2.toString());
            }
        }
        exportToTxt(sb.toString(), "patientSendingReport.txt");
    }

    private void exportToTxt(String content, String fileNameDestination) {
        try {
            FacesContext context = FacesContext.getCurrentInstance();
            HttpServletResponse response = (HttpServletResponse)
                    context.getExternalContext().getResponse();
            response.setContentType("text/plain");
            response.setHeader("Content-Disposition", "attachment;filename=" + fileNameDestination);

            ServletOutputStream servletOutputStream;
            servletOutputStream = response.getOutputStream();
            servletOutputStream.write(content.getBytes());
            servletOutputStream.flush();
            servletOutputStream.close();

            context.responseComplete();
        } catch (IOException e) {
            log.error(e.getMessage());
        }
    }


    //Return the list of available charset for this country
    public List<CharacterSetEncoding> getCharsetList() {

        //In this part we try to get the common character set encoding.
        List<CharacterSetEncoding> characterSetEncodings = new ArrayList<CharacterSetEncoding>();
        int a = 0;
        do {
            Patient patient = Patient.getPatientById(getSelectedPatientList().get(a).getId());
            if (patient.getAddress() != null && !patient.getAddress().isEmpty()) {
                if (patient.getAddress().get(0).getCountry() != null && patient.getAddress().get(0).getCountry().getCharacterSetEncoding() != null) {
                    characterSetEncodings = patient.getAddress().get(0).getCountry().getCharacterSetEncoding();
                    a = -1;
                } else {
                    a++;
                }
            } else {
                a++;
            }
        }
        while (a != -1 && a < getSelectedPatientList().size());


        for (Patient patient : getSelectedPatientList()) {
            patient = Patient.getPatientById(patient.getId());
            if (patient.getAddress() != null && !patient.getAddress().isEmpty()) {
                if (patient.getAddress().get(0).getCountry() != null && patient.getAddress().get(0).getCountry().getCharacterSetEncoding() != null) {
                    List<CharacterSetEncoding> characterSetEncodingToTest =
                            patient.getAddress().get(0).getCountry().getCharacterSetEncoding();

                    for (int i = characterSetEncodings.size() - 1; i >= 0; i--) {
                        if (!characterSetEncodingToTest.contains(characterSetEncodings.get(i))) {
                            characterSetEncodings.remove(i);
                        }
                    }
                } else {
                    characterSetEncodings.clear();
                    characterSetEncodings.add(CharacterSetEncoding.getUTF8CharacterSetEncoding());
                }
            } else {
                characterSetEncodings.clear();
                characterSetEncodings.add(CharacterSetEncoding.getUTF8CharacterSetEncoding());
            }
        }
        ///////////////////////////////////


        try {
            CharacterSetEncoding characterSetReference;
            List<CharacterSetEncoding> charsetList = new ArrayList<CharacterSetEncoding>();
            if (characterSetEncodings.isEmpty()) {
                characterSetEncodings.add(CharacterSetEncoding.getUTF8CharacterSetEncoding());
            }

            selectedCharset = characterSetEncodings.get(0);
            characterSetReference = characterSetEncodings.get(0);
            charsetList = characterSetEncodings;
            displayWarningCharacterSet = true;


            if (characterSetReference != null && charsetList != null && !charsetList.isEmpty()) {
                if (charsetList.contains(characterSetReference)) {
                    charsetList.remove(characterSetReference);
                }

                charsetList.add(0, characterSetReference);
            } else {
                if (characterSetReference == null || charsetList == null) {

                    log.error("character set encoding List is null !!!");
                    FacesMessages.instance().addFromResourceBundle(StatusMessage.Severity.ERROR, "gazelle.dds.AnyCharacterSetEncodingList");
                }
            }

            return charsetList;
        } catch (Exception e) {
            log.info("****************** ERROR");
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Error - Problem with : " + e.getMessage());
            return null;
        }
    }


    public List<SelectItem> getMessageTypeList() {
        List<SelectItem> messageTypes = new ArrayList<SelectItem>();

        messageTypes.add(new SelectItem(messageType_A01_231));
        messageTypes.add(new SelectItem(messageType_A04_231));
        messageTypes.add(new SelectItem(messageType_A28_25));
        messageTypes.add(new SelectItem(messageType_3));

        return messageTypes;
    }


    public void setHL7v2MessageToDisplay(String messageContent) {
        selectedMessageToDisplay = messageContent;
        encodeHL7MessageToXML(selectedMessageToDisplay);
    }


    public String highlightForHL7v2Message(String input) {
        String messageContent = input;

        Integer b = 0;
        List<String> listSegment = new ArrayList<String>();
        listSegment.clear();

        Integer a = messageContent.indexOf("|");
        messageContent = messageContent.replace("<", "&lt;");
        messageContent = messageContent.replace(">", "&gt;");


        if ((a < messageContent.length()) && (a != -1)) {
            String subString = messageContent.substring(b, a);
            messageContent = messageContent.replaceFirst(subString, ("<span class=\"hl-reserved\">" + subString + "</span>"));

            while (a < messageContent.length() && a > 0 && b >= 0) {
                a = messageContent.indexOf("\r", a + 1);

                if ((a + 1 < messageContent.length()) && (a != -1)) {
                    b = messageContent.indexOf("|", a + 1);

                    if ((b - a) == 4) {
                        subString = messageContent.substring(a, b);

                        if (!listSegment.contains(subString)) {
                            listSegment.add(subString);
                            messageContent = messageContent.replaceAll(subString, ("\r<span class=\"hl-reserved\">" + subString.substring(1, 4) + "</span>"));
                        }
                    }
                } else {
                    a = -1;
                }
            }

        }

        messageContent = messageContent.replace("&", "<span class=\"hl-special\">&amp;</span>");
        messageContent = messageContent.replace("#", "<span class=\"hl-special\">&#35;</span>");
        messageContent = messageContent.replace("~", "<span class=\"hl-special\">&#126;</span>");
        messageContent = messageContent.replace("|", "<span class=\"hl-special\">&#124;</span>");
        messageContent = messageContent.replace("^", "<span class=\"hl-brackets\">&#94;</span>");
        messageContent = messageContent.replace("\r", "<span class=\"hl-crlf\">[CR]</span><br/>");

        return messageContent;
    }


    public String getPlainMessageContent(String message) {
        if (message != null) {
            String messageContent = (message);

            messageContent = messageContent.replace("<", "&lt;");
            messageContent = messageContent.replace(">", "&gt;");
            messageContent = messageContent.replace("&", "&amp;");
            messageContent = messageContent.replace("#", "&#35;");
            messageContent = messageContent.replace("~", "&#126;");
            messageContent = messageContent.replace("|", "&#124;");
            messageContent = messageContent.replace("^", "&#94;");
            messageContent = messageContent.replace("\r", "<br/>");
            return messageContent;
        } else {
            return null;
        }
    }


    private void encodeHL7MessageToXML(String message) {
        xmlTreeData = null;
        xmlMessageContent = null;

        String messageContent = (message);

        messageContent = messageContent.replaceAll("\n", "\r");

        PipeParser pipeParser = new PipeParser();
        pipeParser.setValidationContext(new NoValidation());
        XMLParser xmlParser = new DefaultXMLParser();

        try {
            Message messageHL7;
            messageHL7 = pipeParser.parse(messageContent);

            xmlMessageContent = xmlParser.encode(messageHL7);

            xmlTreeData = XmlTreeDataBuilder.build(new InputSource(new ByteArrayInputStream(xmlMessageContent.getBytes("UTF-8"))));

        } catch (EncodingNotSupportedException e) {
            log.error("" + e.getMessage());
        } catch (HL7Exception e) {
            log.error("" + e.getMessage());
        } catch (SAXException e) {
            log.error("" + e.getMessage());
        } catch (IOException e) {
            log.error("" + e.getMessage());
        } catch (ClassCastException e) {
            log.error("" + e.getMessage());
        }
    }

    public String getSelectedMessageType() {
        return selectedMessageType;
    }


    public void setSelectedMessageType(String selectedMessageType) {
        this.selectedMessageType = selectedMessageType;
    }


    public String getSelectedMessageToDisplay() {
        return selectedMessageToDisplay;
    }


    public void setSelectedMessageToDisplay(String selectedMessageToDisplay) {
        this.selectedMessageToDisplay = selectedMessageToDisplay;
    }


    public SystemConfig getManualConfig() {
        return manualConfig;
    }


    public void setManualConfig(SystemConfig manualConfig) {
        this.manualConfig = manualConfig;
    }


    public CharacterSetEncoding getSelectedCharset() {
        return selectedCharset;
    }


    public void setSelectedCharset(CharacterSetEncoding selectedCharset) {
        this.selectedCharset = selectedCharset;
    }


    public List<Patient> getSelectedPatientList() {
        return selectedPatientList;
    }


    public String getStringMessage() {
        return stringMessage;
    }


    public boolean isDisplayMessage() {
        return displayMessage;
    }


    public List<SystemConfig> getConfigs() {
        return configs;
    }


    public boolean isDisplaySystemTable() {
        return displaySystemTable;
    }


    public boolean isDisplayAckList() {
        return displayAckList;
    }


    public boolean isDisplayWarningCharacterSet() {
        return displayWarningCharacterSet;
    }


    public String getXmlMessageContent() {
        return xmlMessageContent;
    }


    public TreeNode getXmlTreeData() {
        return xmlTreeData;
    }

    public String getMessageType_A01_231() {
        return messageType_A01_231;
    }

    public String getMessageType_A04_231() {
        return messageType_A04_231;
    }

    public String getMessageType_A28_25() {
        return messageType_A28_25;
    }

    public String getMessageType_3() {
        return messageType_3;
    }

}
