/*
 * Copyright 2009 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.dds.model;

import org.jboss.seam.annotations.Name;

import javax.persistence.*;
import java.util.List;

/**
 * <b>Class Description :  </b>FirstNameAlternateSpelling
 * This class describes a FirstNameAlternateSpelling object. It corresponds to an  Alternate Spelling for the FirstName  generated through DemographicDataServer application.
 * It corresponds to a medical Information for a Patient.
 *
 * FirstNameAlternateSpelling object possesses the following attributes :
 * <ul>
 * <li><b>id</b> : id corresponding to the FirstNameAlternateSpelling</li>
 * <li><b>value</b> : Object corresponding to the Alternate Spelling of firstNameSex</li>
 * <li><b>firstNameSex</b> : Object corresponding to the firstName and the sex of  a person</li>
 * </ul>
 * <b>Example of FirstNameAlternateSpelling</b> : FirstNameAlternateSpelling(id, firstNameSex, value) = (1, 6, 56, Fred ) <br>
 *
 * @author Samii Ayed - Jean-Renan Chatel / INRIA Rennes IHE development Project
 * @version 1.0 - 2009, May 6th
 */

@Entity
@Name("firstNameAlternateSpelling")
@Table(name = "dds_first_name_alternate_spelling", schema = "public")
@org.hibernate.annotations.Entity(selectBeforeUpdate = true)
@SequenceGenerator(name = "dds_first_name_alternate_spelling_sequence", sequenceName = "dds_first_name_alternate_spelling_id_seq", allocationSize = 1)

public class FirstNameAlternateSpelling extends AuditModule implements java.io.Serializable {

    /**
     * Serial ID version of this object
     */
    private static final long serialVersionUID = -450904331541569870L;

    //	Attributes (existing in database as a column)

    /**
     * Id of this FirstNameAlternateSpelling object
     */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "dds_first_name_alternate_spelling_sequence")
    private Integer id;


    /**
     * The value of the FirstNameAlternateSpelling  for a firstName
     */
    @Column(name = "value")
    private String value;

    //	 Variables used for Foreign Keys : dds_patient_address table

    /**
     * The FirstNameSex object corresponding to this firstNameAlternateSpelling
     */
    @ManyToOne
    @JoinColumn(name = "first_name_sex_id")

    private FirstNameSex firstNameSex;

    //	Constructors

    public FirstNameAlternateSpelling() {

    }

    public FirstNameAlternateSpelling(String value, FirstNameSex firstNameSex) {
        this.value = value;
        this.firstNameSex = firstNameSex;
    }

    public FirstNameAlternateSpelling(FirstNameAlternateSpelling firstNameAlternateSpelling) {
        if (firstNameAlternateSpelling.getValue() != null) {
            this.value = firstNameAlternateSpelling.getValue();
        }
        if (firstNameAlternateSpelling.getFirstNameSex() != null) {
            this.firstNameSex = new FirstNameSex(firstNameAlternateSpelling.getFirstNameSex());
        }
    }

    // *********************************************************
    //  Getters and Setters : setup the DB columns properties
    // *********************************************************

    public static List<FirstNameAlternateSpelling> getFirstNameAlternateSpellingFiltered(FirstNameSex inFirstNameSex) {
        FirstNameAlternateSpellingQuery query = new FirstNameAlternateSpellingQuery();
        if (inFirstNameSex != null) {
            query.firstNameSex().eq(inFirstNameSex);
        }
        return query.getList();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public FirstNameSex getFirstNameSex() {
        return firstNameSex;
    }

    public void setFirstNameSex(FirstNameSex firstNameSex) {
        this.firstNameSex = firstNameSex;
    }

    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((firstNameSex == null) ? 0 : firstNameSex.hashCode());
        result = prime * result + ((value == null) ? 0 : value.hashCode());
        return result;
    }

    //  function ///////////////////////////////////////////////////////////////////////////

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        FirstNameAlternateSpelling other = (FirstNameAlternateSpelling) obj;
        if (firstNameSex == null) {
            if (other.firstNameSex != null) {
                return false;
            }
        } else if (!firstNameSex.equals(other.firstNameSex)) {
            return false;
        }
        if (value == null) {
            if (other.value != null) {
                return false;
            }
        } else if (!value.equals(other.value)) {
            return false;
        }
        return true;
    }

}
