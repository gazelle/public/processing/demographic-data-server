package net.ihe.gazelle.dds.admin;

import net.ihe.gazelle.common.interfacegenerator.GenerateInterface;
import net.ihe.gazelle.dds.model.ApplicationConfiguration;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.*;
import org.jboss.seam.security.Identity;

import javax.ejb.Remove;
import java.io.Serializable;

/**
 * This class is used to store some of the preferences in the
 * application state instead of querying several times the database
 *
 * @author aberge
 */

@AutoCreate
@Name("applicationConfigurationProvider")
@Scope(ScopeType.APPLICATION)
@GenerateInterface("ApplicationConfigurationProviderLocal")
public class ApplicationConfigurationProvider implements Serializable, ApplicationConfigurationProviderLocal {

    /**
     *
     */
    private static final long serialVersionUID = 5004043953931007769L;

    private static final String DEFAULT_WS_REQUEST_MAXIMUM_NUMBER = "30";
    private static final String DEFAULT_WS_TIME_INTERVAL = "60000";
    private static final String DEFAULT_WS_MAXIMUM_REQUEST_PER_USER = "10000";
    private static final String DEFAULT_WS_DAY_INTERVAL = "86400000";

    private String applicationMode;
    private int wsRequestMaximumNumber;
    private long wsTimeInterval;
    private int wsMaximumRequestPerUser;
    private long wsDayInterval;
    private Boolean applicationWorksWithoutCas;
    private Boolean casEnable;
    private Boolean ipLogin;
    private String ipLoginAdmin;
    private String applicationUrl;
    private String casUrl;
    private String documentation;
    private String applicationReleaseNotesUrl;
    private String applicationAdminTitle;
    private String applicationAdminEmail;
    private String applicationAdminName;
    private String issueTrackerUrl;

    @Create
    public void init() {
        applicationMode = null;
        wsDayInterval = -1;
        wsRequestMaximumNumber = -1;
        wsMaximumRequestPerUser = -1;
        wsTimeInterval = -1;
        applicationWorksWithoutCas = null;
        casEnable = null;
        ipLogin = null;
        ipLoginAdmin = null;
        casUrl = null;
        applicationUrl = null;
        documentation = null;
    }

    @Destroy
    @Remove
    public void destroy() {
        init();
    }

    public static ApplicationConfigurationProviderLocal instance() {
        return (ApplicationConfigurationProviderLocal) Component.getInstance("applicationConfigurationProvider");
    }

    public String getApplicationMode() {
        if (applicationMode == null) {
            applicationMode = ApplicationConfiguration.getValueOfVariable("application_mode");
            if (applicationMode != null) {
                applicationMode = applicationMode.trim().toLowerCase();
            } else {
                applicationMode = "minimal";
                ApplicationConfiguration conf = new ApplicationConfiguration();
                conf.setValue(applicationMode);
                conf.setVariable("application_mode");
                conf.save();
            }
        }
        return applicationMode;
    }

    public int getWsRequestMaximumNumber() {
        if (wsRequestMaximumNumber < 0) {
            String wsRequestMaximumNumberString = ApplicationConfiguration.getValueOfVariable("ws_request_maximum_number");
            if (wsRequestMaximumNumberString == null) {
                ApplicationConfiguration conf = new ApplicationConfiguration();
                conf.setValue(DEFAULT_WS_REQUEST_MAXIMUM_NUMBER);
                conf.setVariable("ws_request_maximum_number");
                conf.save();
                wsRequestMaximumNumberString = DEFAULT_WS_REQUEST_MAXIMUM_NUMBER;
            }
            wsRequestMaximumNumber = Integer.parseInt(wsRequestMaximumNumberString);
        }
        return wsRequestMaximumNumber;
    }

    public long getWsTimeInterval() {
        if (wsTimeInterval < 0) {
            String wsTimeIntervalString = ApplicationConfiguration.getValueOfVariable("ws_time_interval");
            if (wsTimeIntervalString == null) {
                ApplicationConfiguration conf = new ApplicationConfiguration();
                conf.setValue(DEFAULT_WS_TIME_INTERVAL);
                conf.setVariable("ws_time_interval");
                conf.save();
                wsTimeIntervalString = DEFAULT_WS_TIME_INTERVAL;
            }
            wsTimeInterval = (long) Integer.parseInt(wsTimeIntervalString);
        }
        return wsTimeInterval;
    }

    public int getWsMaximumRequestPerUser() {
        if (wsMaximumRequestPerUser < 0) {
            String wsMaximumRequestPerUserString = ApplicationConfiguration.getValueOfVariable("ws_maximum_request_per_user");
            if (wsMaximumRequestPerUserString == null) {
                ApplicationConfiguration conf = new ApplicationConfiguration();
                conf.setValue(DEFAULT_WS_MAXIMUM_REQUEST_PER_USER);
                conf.setVariable("ws_maximum_request_per_user");
                conf.save();
                wsMaximumRequestPerUserString = DEFAULT_WS_MAXIMUM_REQUEST_PER_USER;
            }
            wsMaximumRequestPerUser = Integer.parseInt(wsMaximumRequestPerUserString);
        }
        return wsMaximumRequestPerUser;
    }

    public long getWsDayInterval() {
        if (wsDayInterval < 0) {
            String wsDayIntervalString = ApplicationConfiguration.getValueOfVariable("ws_day_interval");
            if (wsDayIntervalString == null) {
                ApplicationConfiguration conf = new ApplicationConfiguration();
                conf.setValue(DEFAULT_WS_DAY_INTERVAL);
                conf.setVariable("ws_day_interval");
                conf.save();
                wsDayIntervalString = DEFAULT_WS_DAY_INTERVAL;
            }
            wsTimeInterval = (long) Integer.parseInt(wsDayIntervalString);
        }
        return wsDayInterval;
    }

    public Boolean getApplicationWorksWithoutCas() {
        if (applicationWorksWithoutCas == null) {
            String valueAsString = ApplicationConfiguration.getValueOfVariable("application_works_without_cas");
            if (valueAsString == null) {
                applicationWorksWithoutCas = false;
            } else {
                applicationWorksWithoutCas = Boolean.valueOf(valueAsString);
            }
        }
        return applicationWorksWithoutCas;
    }

    public Boolean getCasEnable() {
        if (casEnable == null) {
            String valueAsString = ApplicationConfiguration.getValueOfVariable("cas_enable");
            if (valueAsString == null) {
                casEnable = false;
            } else {
                casEnable = Boolean.valueOf(valueAsString);
            }
        }
        return casEnable;
    }

    public String loginByIP() {
        Identity identity = (Identity) Component.getInstance("org.jboss.seam.security.identity");
        if ("loggedIn".equals(identity.login())) {
            return "/home.xhtml";
        } else {
            return null;
        }
    }

    public Boolean getIpLogin() {
        if (ipLogin == null) {
            String valueAsString = ApplicationConfiguration.getValueOfVariable("ip_login");
            if (valueAsString == null) {
                ipLogin = false;
            } else {
                ipLogin = Boolean.valueOf(valueAsString);
            }
        }
        return ipLogin;
    }

    public String getIpLoginAdmin() {
        if (ipLoginAdmin == null) {
            ipLoginAdmin = ApplicationConfiguration.getValueOfVariable("ip_login_admin");
        }
        return ipLoginAdmin;
    }

    public String getApplicationUrl() {
        if (applicationUrl == null) {
            applicationUrl = ApplicationConfiguration.getValueOfVariable("application_url");
        }
        return applicationUrl;
    }

    public String getCasUrl() {
        if (casUrl == null) {
            casUrl = ApplicationConfiguration.getValueOfVariable("cas_url");
        }
        return casUrl;
    }

    public boolean isUserAllowedAsAdmin() {
        return Identity.instance().isLoggedIn() && Identity.instance().hasRole("admin_role");
    }

    public String getDocumentation() {
        if (this.documentation == null) {
            this.documentation = ApplicationConfiguration.getValueOfVariable("application_documentation");
        }
        if (this.documentation == null) {
            this.documentation = "https://gazelle.ihe.net";
        }
        return this.documentation;
    }

    public String getApplicationReleaseNotesUrl(){
        if (this.applicationReleaseNotesUrl == null) {
            this.applicationReleaseNotesUrl = ApplicationConfiguration.getValueOfVariable("application_release_notes_url");
        }
        return this.applicationReleaseNotesUrl;
    }

    public String getApplicationAdminTitle(){
        if (this.applicationAdminTitle == null) {
            this.applicationAdminTitle = ApplicationConfiguration.getValueOfVariable("application_admin_title");
        }
        return this.applicationAdminTitle;
    }

    public String getApplicationAdminEmail(){
        if (this.applicationAdminEmail == null) {
            this.applicationAdminEmail = ApplicationConfiguration.getValueOfVariable("application_admin_email");
        }
        return this.applicationAdminEmail;
    }

    public String getApplicationAdminName(){
        if (this.applicationAdminName == null) {
            this.applicationAdminName = ApplicationConfiguration.getValueOfVariable("application_admin_name");
        }
        return this.applicationAdminName;
    }

    public String getIssueTrackerUrl(){
        if (this.issueTrackerUrl == null) {
            this.issueTrackerUrl = ApplicationConfiguration.getValueOfVariable("issue_tracker_url");
        }
        return this.issueTrackerUrl;
    }
}
