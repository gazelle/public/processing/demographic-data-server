/*
 * Copyright 2009 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.dds.model;

import net.ihe.gazelle.dds.utils.ReligionByCountryCodeQueryBuilder;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import org.jboss.seam.annotations.Name;

import javax.persistence.*;
import java.util.List;

/**
 * <b>Class Description :  </b>ReligionByCountry<br><br>
 * This class describes a ReligionByCountry object. It corresponds to a ReligionByCountry generated through DemographicDataServer application.
 * It corresponds to a medical Information for a Patient.
 * <p>
 * ReligionByCountry object possesses the following attributes :
 * <ul>
 * <li><b>id</b> : id corresponding to the ReligionByCountry</li>
 * <li><b>iso</b> : the iso  corresponding to the ReligionByCountry</li>
 * <li><b>frequency</b> : The frequency corresponding to a Religion By Country</li>
 * </ul></br>
 * <b>Example of ReligionByCountry</b> : ReligionByCountry(id, iso, Religion,frequency ) = (1, FR, 60, 10%) <br>
 *
 * @author Abderrazek Boufahja - Samii Ayed - Jean-Renan Chatel / INRIA Rennes IHE development Project
 * @version 1.0 - 2009, May 6th
 * @class ReligionByCountry.java
 * @package net.ihe.gazelle.demographic.model
 * @see > sayed@irisa.fr - jchatel@irisa.fr  -  http://www.ihe-europe.org
 */


@Entity
@Name("religionByCountryCode")
@Table(name = "dds_religion_by_country_code", schema = "public")
@org.hibernate.annotations.Entity(selectBeforeUpdate = true)
@SequenceGenerator(name = "dds_religion_by_country_code_sequence", sequenceName = "dds_religion_by_country_code_id_seq", allocationSize = 1)
public class ReligionByCountryCode extends AuditModule implements java.io.Serializable {


    /**
     * Serial ID version of this object
     */
    private static final long serialVersionUID = -76199991541231760L;

    //	Attributes (existing in database as a column)

    /**
     * Id of this ReligionByCountry object
     */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "dds_religion_by_country_code_sequence")
    private Integer id;

    /**
     * CountryCode object corresponding to this ReligionByCountry
     */
    @ManyToOne
    @JoinColumn(name = "country_code_id")
    private CountryCode countryCode;

    /**
     * Religion object corresponding to this ReligionByCountry
     */
    @ManyToOne
    @JoinColumn(name = "religion_id")
    private Religion religion;

    /**
     * The frequency of Religion in this Country
     */
    @Column(name = "frequency")
    private Integer frequency;

    /**
     * The  accumulation for the frequency of the Religion
     */
    @Column(name = "cumul")
    private Integer cumul;

    //	Constructors

    public ReligionByCountryCode() {

    }

    public ReligionByCountryCode(CountryCode countryCode, Religion religion, Integer frequency) {
        this.religion = religion;
        this.frequency = frequency;
        this.countryCode = countryCode;
    }

    // *********************************************************
    //  Getters and Setters : setup the DB columns properties
    // *********************************************************


    public CountryCode getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(CountryCode countryCode) {
        this.countryCode = countryCode;
    }

    public Religion getReligion() {
        return religion;
    }

    public void setReligion(Religion religion) {
        this.religion = religion;
    }

    public Integer getFrequency() {
        return frequency;
    }

    public void setFrequency(Integer frequency) {
        this.frequency = frequency;
    }

    public Integer getCumul() {
        return cumul;
    }

    public void setCumul(Integer cumul) {
        this.cumul = cumul;
    }

    public Integer getId() {
        return id;
    }


    public static List<Religion> getReligionListForSpecificCountry(CountryCode country) {
        ReligionByCountryCodeQuery query = new ReligionByCountryCodeQuery();
        query.countryCode().eq(country);
        return query.religion().getListDistinct();
    }


    public static ReligionByCountryCode generateRandomReligionByCountryCode(String iso) {
        ReligionByCountryCodeQueryBuilder query = new ReligionByCountryCodeQueryBuilder();
        query.addEq("countryCode.iso", iso);
        Integer cumulReligionByCountryCode = query.getMax("cumul");
        if (cumulReligionByCountryCode != null) {
            int res = Patient.generateRandomInteger(1, cumulReligionByCountryCode);
            query.addRestriction(HQLRestrictions.ge("cumul", res));
            cumulReligionByCountryCode = query.getMin("cumul");
            query.addEq("cumul", cumulReligionByCountryCode);
            return query.getUniqueResult();
        } else {
            return null;
        }
    }

    //equals and hashcode //////////////

    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((countryCode == null) ? 0 : countryCode.hashCode());
        result = prime * result + ((cumul == null) ? 0 : cumul.hashCode());
        result = prime * result
                + ((frequency == null) ? 0 : frequency.hashCode());
        result = prime * result
                + ((religion == null) ? 0 : religion.hashCode());
        return result;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        ReligionByCountryCode other = (ReligionByCountryCode) obj;
        if (countryCode == null) {
            if (other.countryCode != null) {
                return false;
            }
        } else if (!countryCode.equals(other.countryCode)) {
            return false;
        }
        if (cumul == null) {
            if (other.cumul != null) {
                return false;
            }
        } else if (!cumul.equals(other.cumul)) {
            return false;
        }
        if (frequency == null) {
            if (other.frequency != null) {
                return false;
            }
        } else if (!frequency.equals(other.frequency)) {
            return false;
        }
        if (religion == null) {
            if (other.religion != null) {
                return false;
            }
        } else if (!religion.equals(other.religion)) {
            return false;
        }
        return true;
    }

}

