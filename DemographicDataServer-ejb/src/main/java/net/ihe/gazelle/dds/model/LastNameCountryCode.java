/*
 * Copyright 2009 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.dds.model;

import net.ihe.gazelle.dds.utils.LastNameCountryCodeQueryBuilder;
import net.ihe.gazelle.hql.restrictions.HQLRestrictionLikeMatchMode;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import org.jboss.seam.annotations.Name;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import java.util.List;

/**
 * <b>Class Description :  </b>LastNameCountryCode<br><br>
 * This class describes a LastNameCountryCode object. It corresponds to a LastNameCountryCode generated through DemographicDataServer application.
 * It corresponds to a medical Information for a Patient.
 * <p>
 * LastNameCountryCode object possesses the following attributes :
 * <ul>
 * <li><b>id</b> : id corresponding to the LastNameCountryCode</li>
 * <li><b>lastName</b> : Object corresponding to a LastName</li>
 * <li><b>iso</b> : Object corresponding to the iso3166CountryCode of a lastNameCountryCode, with some informations (eg. street, city, country, etc...)</li>
 * </ul>
 * <b>Example of LastNameCountryCode</b> : LastNameCountryCode(id, lastName, iso) = (1, 2, FR) <br>
 *
 * @author Abderrazek boufahja - Samii Ayed - Jean-Renan Chatel / INRIA Rennes IHE development Project
 * @version 1.0 - 2009, May 6th
 */

@Entity
@Name("lastNameCountryCode")
@Table(name = "dds_last_name_country_code", schema = "public")
@SequenceGenerator(name = "dds_last_name_country_code_sequence", sequenceName = "dds_last_name_country_code_id_seq", allocationSize = 1)
public class LastNameCountryCode extends AuditModule implements java.io.Serializable {


    /**
     * Serial ID version of this object
     */
    private static final long serialVersionUID = -543913331541293760L;


    /**
     * Logger
     */
    private static Logger log = LoggerFactory.getLogger(LastNameCountryCode.class);

    //	Attributes (existing in database as a column)

    /**
     * Id of this LastNameCountryCode object
     */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "dds_last_name_country_code_sequence")
    private Integer id;

    //	 Variables used for Foreign Keys : dds_patient_address table

    /**
     * LastName object corresponding to this LastNameCountryCode
     */
    @ManyToOne
    @JoinColumn(name = "last_name_id")
    private LastName lastName;


    /**
     * Iso3166CountryCode object corresponding to this LastNameCountryCode
     */
    @ManyToOne
    @JoinColumn(name = "country_code_id")
    private CountryCode countryCode;

    /**
     * The frequency of the lastName
     */
    @Column(name = "frequency")
    private Integer frequency;

    /**
     * The  accumulation for the frequency of the lastName
     */
    @Column(name = "cumul")
    private Integer cumul;

    //	Constructors

    public LastNameCountryCode() {

    }

    public LastNameCountryCode(LastName lastName, CountryCode countryCode) {
        this.lastName = lastName;
        this.countryCode = countryCode;
    }

    public LastNameCountryCode(LastNameCountryCode lastNameCountryCode) {
        if (lastNameCountryCode.getLastName() != null) {
            this.lastName = new LastName(lastNameCountryCode.getLastName());
        }
        if (lastNameCountryCode.getCountryCode() != null) {
            this.countryCode = new CountryCode(lastNameCountryCode.getCountryCode());
        }
    }

    // *********************************************************
    //  Getters and Setters : setup the DB columns properties
    // *********************************************************

    public static LastNameCountryCode generateRandomLastNameCountryCode(String iso, String lastNameNearby, String lastNameLike, Gender gender) {
        return getBestMatchingLNCC(iso, lastNameNearby, lastNameLike, gender);
    }

    private static LastNameCountryCode getBestMatchingLNCC(String iso, String lastNameNearby, String lastNameLike, Gender gender) {
        CountryCodeQuery query4Country = new CountryCodeQuery();
        query4Country.iso().eq(iso);
        CountryCode country = query4Country.getUniqueResult();
        int maxCumul;
        LastNameCountryCodeQueryBuilder query = new LastNameCountryCodeQueryBuilder();
        if (iso != null) {
            query.addEq("countryCode", country);
        }
        if (country.getIsLastNameSexed() != null
                && country.getIsLastNameSexed()
                && gender != null) {
            query.addEq("lastName.gender.description", gender.getDescription());
        }
        if (lastNameLike != null && !lastNameLike.isEmpty()) {
            query.addRestriction(HQLRestrictions.like("lastName.value", lastNameLike, HQLRestrictionLikeMatchMode.ANYWHERE));
        }

        Integer cumulLastNameCountryCode = query.getMax("cumul");
        if (cumulLastNameCountryCode != null && cumulLastNameCountryCode > 0) {
            int res = Patient.generateRandomInteger(1, cumulLastNameCountryCode);
            query.addRestriction(HQLRestrictions.ge("cumul", res));
            cumulLastNameCountryCode = query.getMin("cumul");
            query.addEq("cumul", cumulLastNameCountryCode);
        }

        List<LastNameCountryCode> listFNCCMatching = query.getList();
        if (listFNCCMatching.size() > 0) {
            maxCumul = updateCumulOfListLNCC(listFNCCMatching);
            return getBestLNCCByCumul(listFNCCMatching, maxCumul);
        } else {
            return null;
        }
    }

    private static int updateCumulOfListLNCC(List<LastNameCountryCode> listLNCC) {
        int cumulation = 0;
        for (LastNameCountryCode LNCC : listLNCC) {
            cumulation = cumulation + LNCC.getFrequency().intValue();
            LNCC.setCumul(cumulation);
        }
        return cumulation;
    }

    private static LastNameCountryCode getBestLNCCByCumul(List<LastNameCountryCode> listLncc, int maxCumul) {
        LastNameCountryCode lastNameCountryCode = new LastNameCountryCode();
        if (listLncc.size() > 0) {
            int indexLncc = getBestIndexByCumul(listLncc, maxCumul);
            log.info("index_lncc = " + indexLncc);
            lastNameCountryCode = listLncc.get(indexLncc);
        }
        return lastNameCountryCode;
    }

    private static int getBestIndexByCumul(List<LastNameCountryCode> list_lncc, int maxCumul) {
        int minCumul = LastNameCountryCode.getMinCumul(maxCumul);
        return LastNameCountryCode.getNearestIndex(list_lncc, maxCumul, minCumul);
    }

    private static int getMinCumul(int maxCumul) {
        int res = 0;
        res = (int) ((Math.random()) * maxCumul);
        log.info("mincumul = " + res);
        return res;
    }

    @Deprecated
    private static int getMaxCumul(List<LastNameCountryCode> listLncc) {
        int maxCumul = 0;
        for (LastNameCountryCode LNCC : listLncc) {
            if (LNCC.getCumul() > maxCumul) {
                maxCumul = LNCC.getCumul();
            }
        }
        return maxCumul;
    }

    private static int getNearestIndex(List<LastNameCountryCode> list_lncc, int maxCumul, int minCumul) {
        int count = maxCumul;
        int res = 0;
        for (LastNameCountryCode lncc : list_lncc) {
            if ((lncc.getCumul() <= count) && (lncc.getCumul() >= minCumul)) {
                res = list_lncc.indexOf(lncc);
                count = lncc.getCumul();
            }
        }
        log.info("getNearestIndex = " + res);
        return res;
    }

    public static List<CountryCode> getCountryCodeList() {
        LastNameCountryCodeQuery query = new LastNameCountryCodeQuery();
        query.countryCode().printableName().order(true);
        return query.countryCode().getListDistinctOrdered();
    }

    public Integer getId() {
        return id;
    }

    public LastName getLastName() {
        return lastName;
    }

    public void setLastName(LastName lastName) {
        this.lastName = lastName;
    }

    public CountryCode getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(CountryCode countryCode) {
        this.countryCode = countryCode;
    }

    public Integer getFrequency() {
        return frequency;
    }

    public void setFrequency(Integer frequency) {
        this.frequency = frequency;
    }

    public Integer getCumul() {
        return cumul;
    }

    public void setCumul(Integer cumul) {
        this.cumul = cumul;
    }

    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((countryCode == null) ? 0 : countryCode.hashCode());
        result = prime * result + ((cumul == null) ? 0 : cumul.hashCode());
        result = prime * result + ((frequency == null) ? 0 : frequency.hashCode());
        result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
        return result;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        LastNameCountryCode other = (LastNameCountryCode) obj;
        if (countryCode == null) {
            if (other.countryCode != null) {
                return false;
            }
        } else if (!countryCode.equals(other.countryCode)) {
            return false;
        }
        if (cumul == null) {
            if (other.cumul != null) {
                return false;
            }
        } else if (!cumul.equals(other.cumul)) {
            return false;
        }
        if (frequency == null) {
            if (other.frequency != null) {
                return false;
            }
        } else if (!frequency.equals(other.frequency)) {
            return false;
        }
        if (lastName == null) {
            if (other.lastName != null) {
                return false;
            }
        } else if (!lastName.equals(other.lastName)) {
            return false;
        }
        return true;
    }

}





