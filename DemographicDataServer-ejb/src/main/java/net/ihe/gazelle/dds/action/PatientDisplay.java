package net.ihe.gazelle.dds.action;

import net.ihe.gazelle.dds.model.Patient;
import net.ihe.gazelle.dds.model.PatientQuery;
import net.ihe.gazelle.dds.utils.Utils;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.contexts.Contexts;

import javax.faces.context.FacesContext;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Name("patientDisplay")
@Scope(ScopeType.PAGE)
public class PatientDisplay implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 2992990152736660152L;

    private Patient selectedPatient;

    @Create
    public void getPatientFromUrl() {
        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        if (params.containsKey("id")) {
            PatientQuery query = new PatientQuery();
            query.id().eq(Integer.parseInt(params.get("id")));
            selectedPatient = query.getUniqueResult();
        }
    }

    public Patient getSelectedPatient() {
        return selectedPatient;
    }

    public String sharePatient() {
        if (this.selectedPatient != null) {
            List<Patient> patientsToShare = new ArrayList<Patient>();
            patientsToShare.add(selectedPatient);
            Contexts.getSessionContext().set("patientsToShare", patientsToShare);
            return "/patient/sharePatient.xhtml";
        } else {
            return null;
        }
    }

    public String getNameTypeDescription(String value) {
        return Utils.getNameTypeDescription(value);
    }
}
