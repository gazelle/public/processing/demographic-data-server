/*
 * Copyright 2009 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.dds.action;

import net.ihe.gazelle.dds.geonames.GeoName;
import net.ihe.gazelle.dds.googleapi.AddressComponent;
import net.ihe.gazelle.dds.googleapi.AddressComponentType;
import net.ihe.gazelle.dds.googleapi.GeocodeResponse;
import net.ihe.gazelle.dds.googleapi.GeocodeResponseStatus;
import net.ihe.gazelle.dds.googleapi.Result;
import net.ihe.gazelle.dds.model.CountryCode;
import net.ihe.gazelle.dds.model.CountryCodeQuery;
import net.ihe.gazelle.dds.model.PatientAddress;
import net.ihe.gazelle.dds.model.PatientAddressQuery;
import net.ihe.gazelle.dds.model.UserIpAddress;
import net.ihe.gazelle.dds.model.WebServicesInformation;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;


public class AddressInfoBuilder {


    // attributes  ///////////////////////////////////////////////////////////////////////////


    private static final String SUCESS_STATUS_CODE = "200";
    private static final String NOMINATIM = "Nominatim";

    private static Logger log = LoggerFactory.getLogger(AddressInfoBuilder.class);
    private static SAXBuilder parser = new SAXBuilder();

    private static int counterNominatim = 0;


    private static int lastIpUserId = 0;

    // constructors  /////////////////////////////////////////////////////////////////////////

    public AddressInfoBuilder() {
        super();
    }

    /// getter and setter ////////////////////////////////////////////////////////////////////

    public static SAXBuilder getParser() {
        return parser;
    }

    public static void setParser(SAXBuilder parser) {
        AddressInfoBuilder.parser = parser;
    }

    public static PatientAddress constructNearbyAddress(String countryCode, double lattitude, double longitude) throws JDOMException, IOException {
        return constructNearbyAddress(countryCode, lattitude, longitude, false);
    }

    public static PatientAddress constructNearbyAddress(String countryCode, double lattitude, double longitude, boolean isBirthPlace) throws JDOMException, IOException {
        AddressInfo addressInfo = new AddressInfo();
        List<String> listLanguage = CountryCode.getcountryLanguageByCountryCode(countryCode);
        addressInfo.setCountryLanguages(listLanguage);
        addressInfo.setCountryNameCode(countryCode);
        Integer counter = 0;
        Integer tmp = 0;
        counterNominatim = 0;

        //To increase the request number of DDS Ws since the last 24 hours.
        toIncreaseWsrequestNumber("DDS_Ws");


        do {
            Double lat = lattitude + 0.005 * (Math.random() * 2 - 1);
            Double lng = longitude + 0.005 * (Math.random() * 2 - 1);
            addressInfo.setCityLat(lat);
            addressInfo.setCityLng(lng);
            tmp = AddressInfoBuilder.findAddress(addressInfo, isBirthPlace);
            log.debug(tmp.toString());
            counterNominatim++;
        } while (tmp < 8 && counter++ < 20);

        log.info("Accuracy = " + tmp + " counter = " + counter);

        if (counter > 19) {
            log.info("counter = " + counter + " * " + countryCode);
            CountryCode country = CountryCode.getCountryByCountryCode(countryCode);
            return new PatientAddress(null, null, null, null, country, null, null, null);
        } else {
            return addressInfo.generatePatientAddress(countryCode);
        }
    }

    // methods  ///////////////////////////////////////////////////////////////////////////

    public static PatientAddress constructAddress(String countryCode) throws JDOMException, IOException {
        return AddressInfoBuilder.constructAddress(countryCode, false);
    }

    public static PatientAddress constructAddress(String countryCode, boolean isBirthPlace) throws JDOMException, IOException {
        GeoName town = AddressInfoBuilder.findPopulatedCity(countryCode);
        return AddressInfoBuilder.constructAddress(town, isBirthPlace);
    }

    public static PatientAddress constructAddress(GeoName inGeoName) throws JDOMException, IOException {
        return constructAddress(inGeoName, false);
    }


    public static PatientAddress constructAddress(GeoName inGeoName, boolean isBirthPlace) throws JDOMException, IOException {

        String countryCode = inGeoName.getCountry();
        PatientAddress patient = AddressInfoBuilder.constructNearbyAddress(countryCode,
                inGeoName.getLatitude().doubleValue(),
                inGeoName.getLongitude().doubleValue(), isBirthPlace);
        return patient;
    }

    public static PatientAddress constructBirthplaceAddress(String countryCode) throws JDOMException, IOException {
        GeoName town = AddressInfoBuilder.findPopulatedCity(countryCode);
        return AddressInfoBuilder.constructAddress(town, true);
    }

    private static GeoName findPopulatedCity(String countryCode) {
        List<GeoName> cityList = GeoName.getGeoNameFiltered(countryCode, null);
        int i = (int) (Math.random() * cityList.size());
        return cityList.get(i);
    }

    public static CountryCode getCountryByIso2Code(String countryCode) {
        CountryCodeQuery query = new CountryCodeQuery();
        query.iso().eq(countryCode);
        return query.getUniqueResult();
    }

    protected static InputStream connect(String url, List<String> list_countryCode) throws IOException, Exception {
        StringBuilder sb = new StringBuilder();
        String acceptedLang;
        for (String clang : list_countryCode) {
            sb.append(clang);
            sb.append(",");
        }
        sb.append("en");
        acceptedLang = sb.toString();

        try {
            URL urlObject = new URL(url);
            HttpURLConnection conn = (HttpURLConnection) urlObject.openConnection();
            conn.setRequestProperty("Accept-Language", acceptedLang);
            conn.setInstanceFollowRedirects(true);
            log.debug(url);
            return conn.getInputStream();
        } catch (IOException e) {
            log.warn("problems connecting to geonames server " + url, e);
            throw e;
        }
    }

    public static String webServiceGeoname(double lat, double lng) {
        String postCode = "";
        String url = "http://ws.geonames.org/findNearbyPostalCodes?lat=" + lat + "&lng=" + lng + "&style=SHORT&maxRows=1";
        toIncreaseWsrequestNumber("Geoname");
        UserIpAddress user = UserIpAddress.findUserIpAddress(lastIpUserId);
        user.setGeonameRequestNumber((user.getGeonameRequestNumber() + 1));
        UserIpAddress.mergeUserIpAddress(user);

        SAXReader xmlReader = new SAXReader();
        try {
            Document document = xmlReader.read(url);
            Node nodePostCode = document.selectSingleNode("//geonames/code/postalcode");
            if (nodePostCode == null) {
                return "";
            } else {
                postCode = nodePostCode.getText();
            }

        } catch (DocumentException e) {
            log.error("" + e.getMessage());
        }

        log.info("Postal Code calculate with Geoname");
        return postCode;
    }

    private static void toIncreaseWsrequestNumber(String wsName) {
        //To increase the request number of wsName Ws since the last 24 hours.
        List<WebServicesInformation> wsInformation = WebServicesInformation.getAllWsInformation();
        int j = wsInformation.size() - 1;
        while (j >= 0) {
            if (wsInformation.get(j).getWsName().equals(wsName)) {
                WebServicesInformation ws = wsInformation.get(j);
                ws.setRequestSinceLastDay((ws.getRequestSinceLastDay() + 1));

                if (wsName.equals("DDS_Ws")) {
                    ws.setTotalRequest(WebServicesInformation.getNumberOfRequestForDDS());
                }
                if (wsName.equals(NOMINATIM)) {
                    ws.setTotalRequest(WebServicesInformation.getNumberOfRequestForNominatim());
                }
                if (wsName.equals("Google")) {
                    ws.setTotalRequest(WebServicesInformation.getNumberOfRequestForGoogle());
                }
                if (wsName.equals("Geoname")) {
                    ws.setTotalRequest(WebServicesInformation.getNumberOfRequestForGeoname());
                }

                WebServicesInformation.mergeWebServicesInformation(ws);
                j = -1;

                findLastIpUserId(wsName, ws);
            } else {
                j--;
            }
        }
    }

    private static void nominatimWsFailed() {

        List<WebServicesInformation> wsInformation = WebServicesInformation.getAllWsInformation();
        Date currentTime = new Date();

        int j = wsInformation.size() - 1;
        while (j >= 0) {
            if (wsInformation.get(j).getWsName().equals(NOMINATIM)) {
                WebServicesInformation ws = wsInformation.get(j);
                ws.setLastRequestTime(currentTime);

                ws.setLastUserIpId(-1);

                WebServicesInformation.mergeWebServicesInformation(ws);
                j = -1;
            } else {
                j--;
            }
        }
    }

    private static boolean nominatimLastWsFailed() {

        List<WebServicesInformation> wsInformation = WebServicesInformation.getAllWsInformation();
        Date currentTime = new Date();

        int j = wsInformation.size() - 1;
        while (j >= 0) {
            if (wsInformation.get(j).getWsName().equals(NOMINATIM)) {
                WebServicesInformation ws = wsInformation.get(j);

                if (ws.getLastUserIpId() == 0) {
                    return true;
                } else {
                    if (ws.getLastRequestTime().getTime() < (currentTime.getTime() - 3600000))  //Verify every hour
                    {
                        ws.setLastUserIpId(0);
                        WebServicesInformation.mergeWebServicesInformation(ws);
                        return true;
                    } else {
                        return false;
                    }
                }
            } else {
                j--;
            }
        }
        return false;
    }

    //To access to the last user Ip Id.
    private static void findLastIpUserId(String wsName, WebServicesInformation ws) {
        if (wsName.equals("DDS_Ws")) {
            lastIpUserId = ws.getLastUserIpId();
        }
    }

    public static String getAddressService(AddressInfo inAddressInfo) {
        String url = "";
        Double lat = inAddressInfo.getCityLat();
        Double lng = inAddressInfo.getCityLng();
        WebServicesInformation webServicesInformation = new WebServicesInformation();
        String priorityService = webServicesInformation.getPriorityService();
        List<WebServicesInformation> listWs = webServicesInformation.getAllWsInformation();

        for (WebServicesInformation wsInfo : listWs) {
            String serviceName = wsInfo.getWsName();
            if (serviceName.equals(priorityService)) {
                url = wsInfo.getWsURL();
                break;
            }
        }
        if (priorityService.equals("Nominatim")) {
            url = url.replace("lat=", "lat=" + lat);
            url = url.replace("lon=", "lon=" + lng);
        } else if (priorityService.equals("Geoname")) {
            url = url.replace("lat=", "lat=" + lat);
            url = url.replace("lng=", "lng=" + lng);
        } else if (priorityService.equals("Google")) {
            url = url.replace("latlng=", "latlng=" + lat + "," + lng);
        }
        return url;
    }

    public static String getAddressServiceForInseeCode(String town){
        String url = "";
        WebServicesInformation webServicesInformation = new WebServicesInformation();
        String priorityService = webServicesInformation.getPriorityService();
        List<WebServicesInformation> listWs = webServicesInformation.getAllWsInformation();

        for (WebServicesInformation wsInfo : listWs) {
            String serviceName = wsInfo.getWsName();
            if (serviceName.equals(priorityService)) {
                url = wsInfo.getWsURL();
                break;
            }
        }
        if (priorityService.equals("Nominatim")) {
            url = url.replace("reverse", "search");
            String encodedTown = null;
            try {
                encodedTown = URLEncoder.encode(town, StandardCharsets.UTF_8.name());
            } catch (UnsupportedEncodingException e) {
                log.error(e.getMessage());
            }
            url = url.replace("lat=&lon=&zoom=18&addressdetails=1", "city="+encodedTown+"&country=France&extratags=1");
        }
        return url;
    }

    public static String findTownInseeCode(String town){
        String url = getAddressServiceForInseeCode(town);
        InputStream is = null;
        SAXReader readerXml = new SAXReader();
        Document document;
        try {
            is = connect(url, new ArrayList<String>());
            document = readerXml.read(is);
            Node inseeCodeNode = document.selectSingleNode("//place/extratags/tag[@key='ref:INSEE']");
            return ((Element)inseeCodeNode).attributeValue("value");
        } catch (Exception e) {
            return null;
        }
    }

    public static String findCountryInseeCode(String countryCode){
        List<CountryCode> countryCodes = CountryCode.getCountryCodeFiltered(countryCode);
        if(countryCodes != null && countryCodes.get(0) != null){
            return countryCodes.get(0).getInseeCode();
        } else {
            return null;
        }
    }

    public static Integer findAddress(AddressInfo inAddressInfo) throws JDOMException, IOException {
        return findAddress(inAddressInfo, false);
    }

    public static Integer findAddress(AddressInfo inAddressInfo, boolean birthPlace) throws JDOMException, IOException {
        String url = getAddressService(inAddressInfo);
        Double lat = inAddressInfo.getCityLat();
        Double lng = inAddressInfo.getCityLng();
        log.debug("entering find address");
        int accuracy = 0;

        if (webServiceNominatim(lat, lng, inAddressInfo, birthPlace)) {
            accuracy = 33;
        } else if (counterNominatim > 1) {
            accuracy = 33;
            log.info("Address finding url=" + url);
            InputStream is;
            try {
                is = connect(url, inAddressInfo.getCountryLanguages());
            } catch (Exception e1) {
                return 0;
            }

            try {
                JAXBContext jc = JAXBContext.newInstance("net.ihe.gazelle.dds.googleapi");
                Unmarshaller unmarshaller = jc.createUnmarshaller();
                GeocodeResponse geocodeResponse = (GeocodeResponse) unmarshaller.unmarshal(is);
                String geoStatus = geocodeResponse.getStatus();

                //To increase the request number of Google Ws since the last 24 hours.
                toIncreaseWsrequestNumber("Google");
                UserIpAddress user = UserIpAddress.findUserIpAddress(lastIpUserId);
                user.setGoogleRequestNumber((user.getGoogleRequestNumber() + 1));
                UserIpAddress.mergeUserIpAddress(user);

                try {
                    GeocodeResponseStatus status = GeocodeResponseStatus.valueOf(geoStatus);
                    switch (status) {
                        case OK:
                            log.info("parsing response from Google maps");
                            accuracy = setAddressFromGoogleResponse(inAddressInfo, geocodeResponse);
                            break;
                        case ZERO_RESULTS:
                            log.warn("No result found by Google maps API for coordinates: " + lat + ", " + lng);
                            accuracy = 0;
                            break;
                        case OVER_QUERY_LIMIT:
                            log.warn("Too many queries performed to Google maps API");
                            accuracy = 0;
                            break;
                        case REQUEST_DENIED:
                            log.warn("Request to Google maps API has been denied");
                            accuracy = 0;
                            break;
                        case INVALID_REQUEST:
                            log.warn("Request to Google maps API is invalid");
                            accuracy = 0;
                        case UNKNOWN_ERROR:
                            log.warn("Unknown error occurred when calling Google Maps API");
                            accuracy = 0;
                            break;
                    }
                } catch (IllegalArgumentException e) {
                    return 0;
                }
            } catch (JAXBException e) {
                log.error("" + e.getMessage());
            }
        }
        return accuracy;
    }

    /* public static AddressInfo findCity(String city) throws JDOMException, IOException {

        String url = "https://maps.googleapis.com/maps/api/geocode/xml?address=" + city.replaceAll("\\s+", "");
        log.info("Address finding url=" + url);

        InputStream is;
        AddressInfo inAddressInfo = new AddressInfo();
        try {
            URLConnection conn = new URL(url).openConnection();
            is =  conn.getInputStream();
        } catch (Exception e1) {
            log.error("" + e1.getMessage());
            return null;
        }

        AddressInfo addressInfo = null;
        try {


            AddressInfo addressInfo = new AddressInfo();
            try {
                DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
                DocumentBuilder db = dbf.newDocumentBuilder();
                org.w3c.dom.Document w3cDoc = db.parse(new URL(url).openConnection().getInputStream());
                DOMReader reader = new DOMReader();
                org.dom4j.Document doc = reader.read(w3cDoc);


                Attribute attribute = (Attribute) doc.selectNodes("//geometry/location/lat").get(0);
                Double lat = Double.parseDouble(attribute.getValue());
                attribute = (Attribute) doc.selectNodes("//geometry/location/lng").get(0);
                Double lng = Double.parseDouble(attribute.getValue());

                attribute = (Attribute) doc.selectNodes("//address_component/type[contains(., \"country\")]/parent::etoileici/short_name").get(0);
                String countryCode = attribute.getValue();

                 addressInfo = new AddressInfo();
            addressInfo.setCityLat(lat);
            addressInfo.setCityLng(lng);
            addressInfo.setCountryNameCode(countryCode);

        } catch (Exception e) {
            log.error("" + e.getMessage());
        }

        if (addressInfo != null) {
            return addressInfo;
        } else {
            return null;patient/CreatePatient.seam
        }
    }*/

    private static Integer setAddressFromGoogleResponse(AddressInfo inAddressInfo, GeocodeResponse geocodeResponse) {
        List<Result> results = geocodeResponse.getResult();
        for (Result result : results) {
            AddressComponentType resultType = AddressComponentType.getComponentTypeFromResponse(result.getType());
            if (AddressComponentType.street_address.equals(resultType)) {
                parseGoogleMapResponse(inAddressInfo, result);
                return 33;
            } else {
                continue;
            }
        }
        return 0;
    }

    private static void parseGoogleMapResponse(AddressInfo inAddressInfo, Result result) {
        List<AddressComponent> components = result.getAddressComponent();
        for (AddressComponent addrComponent : components) {
            AddressComponentType addrType = AddressComponentType.getComponentTypeFromResponse(addrComponent.getType());
            if (addrType != null) {
                switch (addrType) {
                    case street_number:
                        break;
                    case route:
                        inAddressInfo.setThoroughfareName(addrComponent.getLongName());
                        break;
                    case country:
                        inAddressInfo.setCountry(addrComponent.getLongName());
                        inAddressInfo.setCountryNameCode(addrComponent.getShortName());
                        break;
                    case postal_code:
                        inAddressInfo.setPostalCodeNumber(addrComponent.getLongName());
                        break;
                    case locality:
                        inAddressInfo.setLocality(addrComponent.getLongName());
                        inAddressInfo.setLocalityName(addrComponent.getLongName());
                        break;
                    case sublocality:
                        if (inAddressInfo.getLocality() == null) {
                            inAddressInfo.setLocality(addrComponent.getLongName());
                            inAddressInfo.setLocalityName(addrComponent.getLongName());
                        }
                        break;
                    case sublocality_level_1:
                        if (inAddressInfo.getLocality() == null) {
                            inAddressInfo.setLocality(addrComponent.getLongName());
                            inAddressInfo.setLocalityName(addrComponent.getLongName());
                        }
                        break;
                    case administrative_area_level_2:
                        inAddressInfo.setSubAdministrativeArea(addrComponent.getLongName());
                        break;
                    case administrative_area_level_1:
                        inAddressInfo.setAdministrativeAreaName(addrComponent.getLongName());
                        break;
                    default:
                        break;
                }
            } else {
                // this component is not used by DDS, go to the next one
                continue;
            }
        }
    }


    private static Boolean webServiceNominatim(double lat, double lng, AddressInfo inAddressInfo, boolean birthPlace) {

        if (counterNominatim < 2) {
            String url = getAddressService(inAddressInfo);
            //Dom4j is used here to parse the XML document
            SAXReader readerXml = new SAXReader();
            Document document;
            try {
                log.info("Address Nominatim finding url=" + url);
                InputStream is = connect(url, inAddressInfo.getCountryLanguages());
                document = readerXml.read(is);
                //To increase the request number of Nominatim Ws for this current request.
                toIncreaseWsrequestNumber(NOMINATIM);
                UserIpAddress user = UserIpAddress.findUserIpAddress(lastIpUserId);

                int nominatimRequest = user.getNominatimRequestNumber();
                user.setNominatimRequestNumber(nominatimRequest + 1);
                UserIpAddress.mergeUserIpAddress(user);

                Node nodeAddress = document.selectSingleNode("//reversegeocode/result");

                Node node = document.selectSingleNode("//reversegeocode/addressparts");

                    //XML parser
                    try {

                        inAddressInfo.setAddress(nodeAddress.getText());

                        Node nodeCountry = node.selectSingleNode("country");
                        if (nodeCountry != null) {
                            inAddressInfo.setCountryName(nodeCountry.getText());
                        } else {
                            return false;
                        }

                        if (!birthPlace) {
                            Node nodeRoad = node.selectSingleNode("road");
                            if (nodeRoad != null) {
                                inAddressInfo.setThoroughfareName(nodeRoad.getText());
                            } else {
                                log.debug("Road element is missing!");
                                return false;
                            }
                        }

                        Node nodeCity = node.selectSingleNode("city");
                        if (nodeCity == null) {
                            log.debug("City tag is missing!");
                            Node nodeTown = node.selectSingleNode("town");
                            if (nodeTown == null) {
                                log.debug("Town tag is missing!");
                                Node nodeSuburb = node.selectSingleNode("suburb");
                                if (nodeSuburb == null) {
                                    log.debug("Suburb tag is missing!");
                                    Node nodeLocality = node.selectSingleNode("locality");
                                    if (nodeLocality == null) {
                                        log.debug("Locality tag is missing!");
                                        Node nodeHamlet = node.selectSingleNode("hamlet");
                                        if (nodeHamlet == null) {
                                            log.debug("Hamlet tag is missing!");
                                            Node nodeVillage = node.selectSingleNode("village");
                                            if (nodeVillage != null) {
                                                inAddressInfo.setLocalityName(nodeVillage.getText());
                                            } else {
                                                log.debug("Village tag is missing!");
                                                return false;
                                            }
                                        } else {
                                            inAddressInfo.setLocalityName(nodeHamlet.getText());
                                        }
                                    } else {
                                        inAddressInfo.setLocalityName(nodeLocality.getText());
                                    }
                                } else {
                                    inAddressInfo.setLocalityName(nodeSuburb.getText());
                                }
                            } else {
                                inAddressInfo.setLocalityName(nodeTown.getText());
                            }
                        } else {
                            inAddressInfo.setLocalityName(nodeCity.getText());
                        }

                        //Other optional informations
                        Node nodePostcode = node.selectSingleNode("postcode");
                        if (nodePostcode == null) {
                            inAddressInfo.setPostalCodeNumber(webServiceGeoname(lat, lng));
                        } else {
                            inAddressInfo.setPostalCodeNumber(nodePostcode.getText());
                        }

                        //Optional
                        Node nodeState = node.selectSingleNode("state");
                        if (nodeState != null) {
                            inAddressInfo.setAdministrativeAreaName(nodeState.getText());
                        }

                        //Optional
                        Node nodeCounty = node.selectSingleNode("county");
                        if (nodeCounty != null) {
                            inAddressInfo.setSubAdministrativeAreaName(nodeCounty.getText());
                        }


                    } catch (NullPointerException e) {
                        log.error("Tag Postcode or road or an other are missing, need to obtain a new address = " + e.getMessage());
                        return false;
                    }

                    log.info("Address finding url=" + url);
                    return true;

                } catch (DocumentException e1) {
                    log.error("Can't read the XML document ! " + e1.getMessage());
                    nominatimWsFailed();
                    return false;
                } catch (IOException e) {
                    e.printStackTrace();
                    return false;
                } catch (Exception e) {
                    e.printStackTrace();
                    return false;
                }

            } else {
                return false;
            }
    }


    public static PatientAddress getAddressFromDatabase(String countryCode) {
        PatientAddressQuery query = new PatientAddressQuery();
        query.country().iso().eq(countryCode);
        List<Integer> ids = query.id().getListDistinct();
        if (ids != null && !ids.isEmpty()) {
            Random random = new Random();
            Integer id = random.nextInt(ids.size());
            query = new PatientAddressQuery();
            query.id().eq(ids.get(id));
            return query.getUniqueResult();
        } else {
            return new PatientAddress();
        }
    }

    public String getSUCESS_STATUS_CODE() {
        return SUCESS_STATUS_CODE;
    }

}
