package net.ihe.gazelle.dds.admin;

import net.ihe.gazelle.common.Preferences.PreferencesKey;
import net.ihe.gazelle.common.servletfilter.CSPHeaderFilter;
import net.ihe.gazelle.common.servletfilter.CSPPoliciesPreferences;
import net.ihe.gazelle.dds.action.DDSPreferenceProvider;
import net.ihe.gazelle.dds.model.ApplicationConfiguration;
import net.ihe.gazelle.dds.model.ApplicationConfigurationQuery;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.jboss.seam.log.Log;
import org.kohsuke.MetaInfServices;

import javax.persistence.EntityManager;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Name("applicationManager")
@Scope(ScopeType.PAGE)
@MetaInfServices(CSPPoliciesPreferences.class)
public class ApplicationManager implements Serializable, CSPPoliciesPreferences {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Logger
    private static Log log;

    private ApplicationConfiguration preference = null;

    public List<ApplicationConfiguration> getAllPreferences() {
        HQLQueryBuilder<ApplicationConfiguration> builder = new HQLQueryBuilder<ApplicationConfiguration>(ApplicationConfiguration.class);
        builder.addOrder("variable", true);
        return builder.getList();
    }

    public void savePreference(ApplicationConfiguration inPreference) {
        EntityManager entityManager = (EntityManager) Component.getInstance("entityManager");
        entityManager.merge(inPreference);
        entityManager.flush();
        setPreference(null);
        ApplicationConfigurationProvider.instance().init();
        FacesMessages.instance().add(StatusMessage.Severity.INFO, "Preference " + inPreference.getVariable() + " saved");
    }
    public  String getTosLink(){
        return ApplicationConfiguration.getValueOfVariable("link_to_cgu");
    }

    public void createNewPreference() {
        setPreference(new ApplicationConfiguration());
    }


    public void setPreference(ApplicationConfiguration preference) {
        this.preference = preference;
    }

    public ApplicationConfiguration getPreference() {
        return preference;
    }

    @Override
    public boolean isContentPolicyActivated() {
        return (new DDSPreferenceProvider()).getBoolean("security_policies");
    }

    @Override
    public Map<String, String> getHttpSecurityPolicies() {
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put(PreferencesKey.SECURITY_POLICIES.getFriendlyName(),
                String.valueOf((new DDSPreferenceProvider()).getBoolean(PreferencesKey.SECURITY_POLICIES.getFriendlyName())));
        headers.put(PreferencesKey.X_FRAME_OPTIONS.getFriendlyName(),
                ApplicationConfiguration.getValueOfVariable(PreferencesKey.X_FRAME_OPTIONS.getFriendlyName()));
        headers.put(PreferencesKey.CACHE_CONTROL.getFriendlyName(),
                ApplicationConfiguration.getValueOfVariable(PreferencesKey.CACHE_CONTROL.getFriendlyName()));
        headers.put(PreferencesKey.SECURITY_CONTENT_SECURITY_POLICIES.getFriendlyName(),
                ApplicationConfiguration.getValueOfVariable(PreferencesKey.SECURITY_CONTENT_SECURITY_POLICIES.getFriendlyName()));
        headers.put(PreferencesKey.SECURITY_CONTENT_SECURITY_POLICIES_REPORT_ONLY.getFriendlyName(),
                ApplicationConfiguration.getValueOfVariable(PreferencesKey.SECURITY_CONTENT_SECURITY_POLICIES_REPORT_ONLY.getFriendlyName()));
        headers.put(PreferencesKey.STRICT_TRANSPORT_SECURITY.getFriendlyName(),
                ApplicationConfiguration.getValueOfVariable(PreferencesKey.STRICT_TRANSPORT_SECURITY.getFriendlyName()));
        headers.put(PreferencesKey.SECURITY_CONTENT_SECURITY_POLICIES_CHROME.getFriendlyName(),
                ApplicationConfiguration.getValueOfVariable(PreferencesKey.SECURITY_CONTENT_SECURITY_POLICIES.getFriendlyName()));
        headers.put(PreferencesKey.SECURITY_CONTENT_SECURITY_POLICIES_REPORT_ONLY_CHROME.getFriendlyName(),
                ApplicationConfiguration.getValueOfVariable(PreferencesKey.SECURITY_CONTENT_SECURITY_POLICIES_REPORT_ONLY.getFriendlyName()));
        return headers;
    }

    @Override
    public boolean getSqlInjectionFilterSwitch() {
        return (new DDSPreferenceProvider()).getBoolean(PreferencesKey.SQL_INJECTION_FILTER_SWITCH.getFriendlyName());
    }

    public void resetHttpHeaders() {
        log.info("Reset http headers to default values");
        EntityManager entityManager = EntityManagerService.provideEntityManager();

        ApplicationConfiguration ap = null;
        for (PreferencesKey prefKey : PreferencesKey.values()) {
            ApplicationConfigurationQuery applicationConfigurationQuery = new ApplicationConfigurationQuery();
            String friendlyName = prefKey.getFriendlyName();
            applicationConfigurationQuery.variable().eq(friendlyName);
            ap = applicationConfigurationQuery.getUniqueResult();

            if (ap == null) {
                ap = new ApplicationConfiguration();
            }
            ap.setVariable(friendlyName);
            ap.setValue(prefKey.getDefaultValue());
            entityManager.merge(ap);
            entityManager.flush();
        }
        CSPHeaderFilter.clearCache();
        FacesMessages.instance().add(StatusMessage.Severity.INFO, "Http headers reseted");
    }

    public void updateHttpHeaders() {
        CSPHeaderFilter.clearCache();
    }

    public void removeSelectedPreference() {
        String preferenceName;
        if (this.preference != null) {
            EntityManager entityManager = (EntityManager) Component.getInstance("entityManager");
            this.preference = entityManager.find(ApplicationConfiguration.class, this.preference.getId());
            if (this.preference != null) {
                preferenceName = this.preference.getVariable();
                entityManager.remove(this.preference);
                entityManager.flush();
                FacesMessages.instance().add(StatusMessage.Severity.INFO, "Preference '" + preferenceName + "' deleted.");
            }
        } else {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "There was no Preference selected for deletion!");
        }
    }

    public void setValidPref(boolean validPref) {
        this.validPref = validPref;
    }

    public boolean isValidPref() {
        return validPref;
    }

    private boolean validPref = false;

    public boolean isValidPrefName() {
        if (this.preference != null) {
            ApplicationConfigurationQuery q = new ApplicationConfigurationQuery();
            q.variable().eq(preference.getVariable());
            if (q.getListNullIfEmpty() == null) {
                validPref = true;
            } else {
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, "This preference already exist");
                validPref = false;
            }
        } else {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "There is no Preference selected");
        }
        return validPref;
    }
}
