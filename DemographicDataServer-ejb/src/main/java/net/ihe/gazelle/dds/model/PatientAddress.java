/*
 * Copyright 2009 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.dds.model;

import org.jboss.seam.annotations.Name;

import javax.persistence.*;

/**
 * <b>Class Description :  </b>PatientAddress<br><br>
 * This class describes a PatientAddress object. It corresponds to an Address for a Patient generated through DemographicDataServer application.
 * It corresponds to a medical Information for an Address of a Patient (eg: PatientAddress with street, city, country, etc...).
 * <p>
 * PatientAddress object possesses the following attributes :
 * <ul>
 * <li><b>id</b> : id corresponding to the PatientAddress</li>
 * <li><b>number</b> : Object corresponding to a number of the sreet </li>
 * <li><b>street</b> : Object corresponding to the street for PatientAddress(eg. street Charles Foulon, etc...)</li>
 * <li><b>city</b> : Object corresponding to the city for PatientAddress (eg. california, etc...)</li>
 * <li><b>postalCode</b> : Object corresponding to a postalCode of the city</li>
 * <li><b>iso3166CountryCode</b> : Object corresponding to an Iso3166 country code, with the name of country  (eg. FR, France )</li>
 * </ul>
 * <b>Example of PatientAddress</b> : PatientAddress(id, number, street, city, postalCode, iso3166CountryCode) = (1 ,7 ,23 ,3, 35000, FR) <br>
 *
 * @author Samii Ayed - Jean-Renan Chatel / INRIA Rennes IHE development Project
 * @version 1.0 - 2009, May 6th
 */


@Entity
@Name("patientAddress")
@Table(name = "dds_patient_address", schema = "public")
@org.hibernate.annotations.Entity(selectBeforeUpdate = true)
@SequenceGenerator(name = "dds_patient_address_sequence", sequenceName = "dds_patient_address_id_seq", allocationSize = 1)
public class PatientAddress extends AuditModule implements java.io.Serializable {


    /**
     * Serial ID version of this object
     */
    private static final long serialVersionUID = -450904331541291760L;

    //	Attributes (existing in database as a column)

    /**
     * Id of this PatientAddress object
     */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "dds_patient_address_sequence")
    private Integer id;

    /**
     * Number object of street for this patientAddress
     */
    @Column(name = "number")
    private String number;

    /**
     * Street object for this patientAddress
     */
    @ManyToOne
    @JoinColumn(name = "street_id")
    private Street street;

    /**
     * City Object for this patientAddress
     */
    @ManyToOne
    @JoinColumn(name = "city_id")
    private City city;

    /**
     * Iso3166CountryCode object for this patientAddress
     */
    @ManyToOne
    @JoinColumn(name = "country_id")
    private CountryCode country;

    /**
     * PostalCode object for this patientAddress
     */
    @Column(name = "postal_code_id")
    private String postalCode;

    /**
     * State Object for this patientAddress
     */
    @ManyToOne
    @JoinColumn(name = "state_id")
    private State state;

    @Column(name = "lattitude")
    private Double lattitude;

    @Column(name = "longitude")
    private Double longitude;

    //	Constructors

    public PatientAddress() {

    }


    public PatientAddress(String number, Street street, String postalCode, City city, CountryCode country, State state, Double lattitude, Double longitude) {
        this.number = number;
        this.street = street;
        this.city = city;
        this.postalCode = postalCode;
        this.country = country;
        this.state = state;
        this.lattitude = lattitude;
        this.longitude = longitude;
    }


    public PatientAddress(PatientAddress address) {
        this.number = address.getNumber();
        this.street = new Street(address.getStreet());
        this.city = new City(address.getCity());
        this.postalCode = address.getPostalCode();
        this.country = new CountryCode(address.getCountry());
        this.state = new State(address.getState());
        this.lattitude = address.getLattitude();
        this.longitude = address.getLongitude();
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    // *********************************************************
    //  Getters and Setters : setup the DB columns properties
    // *********************************************************

    public String toString() {
        return "PatientAddress [city=" + city + ", country=" + country
                + ", lattitude=" + lattitude + ", longitude=" + longitude
                + ", number=" + number + ", postalCode=" + postalCode
                + ", state=" + state + ", street=" + street + "]";
    }


    public Double getLattitude() {
        return lattitude;
    }


    public void setLattitude(Double lattitude) {
        this.lattitude = lattitude;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Street getStreet() {
        return street;
    }

    public void setStreet(Street street) {
        this.street = street;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public CountryCode getCountry() {
        return country;
    }

    public void setCountry(CountryCode country) {
        this.country = country;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public State getState() {
        return state;
    }


    public void setState(State state) {
        this.state = state;
    }

    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((city == null) ? 0 : city.hashCode());
        result = prime * result + ((country == null) ? 0 : country.hashCode());
        result = prime * result
                + ((lattitude == null) ? 0 : lattitude.hashCode());
        result = prime * result
                + ((longitude == null) ? 0 : longitude.hashCode());
        result = prime * result + ((number == null) ? 0 : number.hashCode());
        result = prime * result
                + ((postalCode == null) ? 0 : postalCode.hashCode());
        result = prime * result + ((state == null) ? 0 : state.hashCode());
        result = prime * result + ((street == null) ? 0 : street.hashCode());
        return result;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        PatientAddress other = (PatientAddress) obj;
        if (city == null) {
            if (other.city != null) {
                return false;
            }
        } else if (!city.equals(other.city)) {
            return false;
        }
        if (country == null) {
            if (other.country != null) {
                return false;
            }
        } else if (!country.equals(other.country)) {
            return false;
        }
        if (lattitude == null) {
            if (other.lattitude != null) {
                return false;
            }
        } else if (!lattitude.equals(other.lattitude)) {
            return false;
        }
        if (longitude == null) {
            if (other.longitude != null) {
                return false;
            }
        } else if (!longitude.equals(other.longitude)) {
            return false;
        }
        if (number == null) {
            if (other.number != null) {
                return false;
            }
        } else if (!number.equals(other.number)) {
            return false;
        }
        if (postalCode == null) {
            if (other.postalCode != null) {
                return false;
            }
        } else if (!postalCode.equals(other.postalCode)) {
            return false;
        }
        if (state == null) {
            if (other.state != null) {
                return false;
            }
        } else if (!state.equals(other.state)) {
            return false;
        }
        if (street == null) {
            if (other.street != null) {
                return false;
            }
        } else if (!street.equals(other.street)) {
            return false;
        }
        return true;
    }

}
