package net.ihe.gazelle.dds.googleapi;

import java.util.List;

/**
 * <b>Class Description : </b>AddressComponentType<br>
 * <br>
 *
 * @author Anne-Gaelle Berge / IHE-Europe development Project
 * @version 1.0 - 12/12/16
 */
public enum AddressComponentType {
    street_address, // level of accuracy we are trying to reach
    street_number,
    route,
    country,
    postal_code,
    locality, // city
    sublocality, // might be used as city if no locality provided
    sublocality_level_1,
    administrative_area_level_2, // equivalent to French region
    administrative_area_level_1; // equivalent to French department = state in the USA

    /**
     * Parses the list of types returned in the response for a given entry and try to match with
     * one described here. If none matches, return null: this element is not used in DDS and shall not
     * be processed further.
     *
     * @param types the list of types
     * @return If none matches, return null: this element is not used in DDS and shall not be processed further.
     */
    public static AddressComponentType getComponentTypeFromResponse(List<String> types) {
        if (types != null) {
            for (String type : types) {
                try {
                    return valueOf(type);
                } catch (IllegalArgumentException e) {
                    continue;
                }
            }
        }
        return null;
    }
}
