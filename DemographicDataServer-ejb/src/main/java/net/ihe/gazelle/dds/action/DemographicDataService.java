package net.ihe.gazelle.dds.action;

import net.ihe.gazelle.dds.admin.ApplicationConfigurationProvider;
import net.ihe.gazelle.dds.model.CountryCode;
import net.ihe.gazelle.dds.model.PatientAddress;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jdom.JDOMException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.xml.soap.SOAPException;
import java.io.IOException;

public class DemographicDataService {

    private static final String MINIMAL = "minimal";
    private static final String SOAP_MESSAGE_ERROR = "This instance of Demographic Data Server tool is running in minimal mode thus this operation is not permitted";
    private static final String SOAP_MESSAGE_ERROR2 = "You need to specify a valid country code (FR, DE, IT, US, JP...).";

    public static PatientAddress returnAddress(String countryCode, Boolean birthPlace) throws JDOMException,
            IOException, SOAPException {
        String countryCodeKW = CountryCode.getCountryCodeKeyword(countryCode);
        if (countryCodeKW == null) {
            SOAPException soapEx = new SOAPException(SOAP_MESSAGE_ERROR2);
            throw soapEx;
        }
        if (birthPlace == null) {
            birthPlace = false;
        }
        PatientAddress patientAddress = null;
        if (ApplicationConfigurationProvider.instance().getApplicationMode().equals("full")) {
            patientAddress = AddressInfoBuilder.constructAddress(countryCodeKW, birthPlace.booleanValue());
        } else {
            patientAddress = AddressInfoBuilder.getAddressFromDatabase(countryCodeKW);
        }
        patientAddress.getCountry().setLanguage(null);
        return patientAddress;
    }

    public static String returnCountryInseeCode(String countryCode) throws SOAPException {
        if (countryCode == null || countryCode.isEmpty()) {
            SOAPException soapEx = new SOAPException(
                    "You need to specify a country name");
            throw soapEx;
        }
        return AddressInfoBuilder.findCountryInseeCode(countryCode);
    }

    public static String returnTownInseeCode(String townName) throws SOAPException {
        if (ApplicationConfigurationProvider.instance().getApplicationMode().equals(MINIMAL)) {
            throw new SOAPException(
                    SOAP_MESSAGE_ERROR);
        } else {
            if (townName == null || townName.isEmpty()) {
                SOAPException soapEx = new SOAPException(
                        "You need to specify a town name");
                throw soapEx;
            }
        }
        return AddressInfoBuilder.findTownInseeCode(townName);
    }

}
