package net.ihe.gazelle.dds.model;

import org.jboss.seam.Component;
import org.jboss.seam.annotations.Name;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

/**
 * <b>Class Description : </b>Iso3166CountryCode<br>
 * <br>
 * This class is useful to have a request historic of users.
 * <p>
 * UserRequestHistoric possesses the following attributes :
 * <ul>
 * <li><b>id</b> : UserIpAddress id</li>
 * <li><b>ipAddress</b> : Ip address of the user</li>
 * <li><b>requestTime</b> : Time of the request</li>
 * <li><b>nominatimRequestNumber : Number of request with Nominatim.</li>
 * <li><b>googleRequestNumber : Number of request with google.</li>
 * <li><b>geonameRequestNumber : Number of request with geoname.</li>
 * </ul>
 * </br> <b>Example of UserIpAddress</b> : (11,'127.0.0.1', '10:22:11',2,3,0); <br>
 *
 * @author Nicolas Lefebvre / INRIA Rennes IHE development Project
 * @version 1.0 - 2010, october 22th
 * @class UserIpAddress.java
 * @package net.ihe.gazelle.users
 * @see > nicolas.lefebvre@inria.fr - http://www.ihe-europe.org
 */

@Entity
@Name("UserRequestHistoric")
@Table(name = "dds_user_request_historic", schema = "public")
@SequenceGenerator(name = "dds_user_request_historic_sequence", sequenceName = "dds_user_request_historic_id_seq", allocationSize = 1)
public class UserRequestHistoric implements java.io.Serializable {

    /**
     * Serial ID version of this object
     */
    private static final long serialVersionUID = -450979971541256760L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "dds_user_request_historic_sequence")
    private Integer id;

    @Column(name = "ip_address", nullable = false)
    @NotNull
    private String ipAddress;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "last_request_time", nullable = false)
    @NotNull
    private Date lastRequestTime;

    @Column(name = "request_number", nullable = false)
    @NotNull
    private Integer requestNumber;

    @Column(name = "failed_request_number", nullable = false)
    @NotNull
    private Integer failedRequestNumber;

    @Column(name = "unlimited")
    private boolean unlimited = false;

    // Constructor
    public UserRequestHistoric() {

    }

    public UserRequestHistoric(String ipAddress, Date lastRequestTime, Integer requestNumber, Integer failedRequestNumber) {
        super();
        this.ipAddress = ipAddress;
        this.lastRequestTime = lastRequestTime;
        this.requestNumber = requestNumber;
        this.failedRequestNumber = failedRequestNumber;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public Date getLastRequestTime() {
        return lastRequestTime;
    }

    public void setLastRequestTime(Date lastRequestTime) {
        this.lastRequestTime = lastRequestTime;
    }

    public Integer getRequestNumber() {
        return requestNumber;
    }

    public void setRequestNumber(Integer requestNumber) {
        this.requestNumber = requestNumber;
    }

    public Integer getFailedRequestNumber() {
        return failedRequestNumber;
    }

    public void setFailedRequestNumber(Integer failedRequestNumber) {
        this.failedRequestNumber = failedRequestNumber;
    }

    public boolean isUnlimited() {
        return unlimited;
    }

    public void setUnlimited(boolean unlimited) {
        this.unlimited = unlimited;
    }


    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((ipAddress == null) ? 0 : ipAddress.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        UserRequestHistoric other = (UserRequestHistoric) obj;
        if (ipAddress == null) {
            if (other.ipAddress != null) {
                return false;
            }
        } else if (!ipAddress.equals(other.ipAddress)) {
            return false;
        }
        return true;
    }

    // methods //////////////////////////////////////////
    public static UserRequestHistoric mergeUserRequestHistoric(UserRequestHistoric user) {
        EntityManager entityManager = (EntityManager) Component.getInstance("entityManager");

        if (user.getIpAddress() != null) {
            user = entityManager.merge(user);
            entityManager.flush();
        }

        return user;
    }

    public static void emptyTable(UserRequestHistoric user) {
        EntityManager entityManager = (EntityManager) Component.getInstance("entityManager");
        entityManager.remove(user);
    }

    public static List<UserRequestHistoric> getAllUserRequestHistoric() {
        UserRequestHistoricQuery query = new UserRequestHistoricQuery();
        query.id().order(true);
        return query.getList();
    }

    public static UserRequestHistoric findUserRequestHistoric(int userId) {
        UserRequestHistoricQuery query = new UserRequestHistoricQuery();
        query.id().eq(userId);
        return query.getUniqueResult();
    }

    public static UserRequestHistoric findUserRequestHistoric(String ip) {
        UserRequestHistoricQuery query = new UserRequestHistoricQuery();
        query.ipAddress().eq(ip);
        return query.getUniqueResult();
    }

}
