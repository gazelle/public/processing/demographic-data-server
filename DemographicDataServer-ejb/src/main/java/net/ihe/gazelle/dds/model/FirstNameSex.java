/*
 * Copyright 2009 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.dds.model;

import org.jboss.seam.annotations.Name;

import javax.persistence.*;

/**
 * <b>Class Description :  </b>FirstNameSex
 * This class describes a FirstNameSex object. It corresponds to a firstName and gender generated through DemographicDataServer application.
 * It corresponds to a medical Information for a FirstNameSex (ex: FirstNameSex with firstName, Gender).
 *
 * FirstNameSex object possesses the following attributes :
 * <ul>
 * <li><b>id</b> : id corresponding to the Patient</li>
 * <li><b>firstName</b> : Object corresponding to a first name of the person</li>
 * <li><b>Gender</b> : Object corresponding to the gender of a person</li>
 * </ul>
 * <b>Example of FirstNameSex</b> : FirstNameSex(id, firstName, Gender) = (1, 'Robert', 'Male')
 *
 * @author Samii Ayed - Jean-Renan Chatel / INRIA Rennes IHE development Project
 * @version 1.0 - 2009, May 6th
 */

@Entity
@Name("firstNameSex")
@Table(name = "dds_first_name_sex", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = {"gender_id", "value"}))
@org.hibernate.annotations.Entity(selectBeforeUpdate = true)
@SequenceGenerator(name = "dds_first_name_sex_sequence", sequenceName = "dds_first_name_sex_id_seq", allocationSize = 1)

public class FirstNameSex extends AuditModule implements java.io.Serializable {

    /**
     * Serial ID version of this object
     */
    private static final long serialVersionUID = -450904000001291760L;

    //	Attributes (existing in database as a column)

    /**
     * Id of this FirstNameSex object
     */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "dds_first_name_sex_sequence")
    private Integer id;

    /**
     * The firstName of the Patient
     */
    @Column(name = "value")
    private String firstName;


    //	 Variables used for Foreign Keys : dds_patient_address table

    /**
     * Gender object corresponding to this firstNameSex
     */
    @ManyToOne
    @JoinColumn(name = "gender_id")
    private Gender gender;


    //	Constructors
    public FirstNameSex() {

    }

    public FirstNameSex(Gender gender, String firstName) {
        this.gender = gender;
        this.firstName = firstName;
    }

    public FirstNameSex(String firstName) {

        this.firstName = firstName;
    }

    public FirstNameSex(FirstNameSex firstNameSex) {
        if (firstNameSex.getGender() != null) {
            this.gender = firstNameSex.getGender();
        }
        if (firstNameSex.getFirstName() != null) {
            this.firstName = firstNameSex.getFirstName();
        }
    }

    // *********************************************************
    //  Getters and Setters : setup the DB columns properties
    // *********************************************************

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }


    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        FirstNameSex firstNameSexTest = (FirstNameSex) obj;
        if (this.firstName == null) {
            if (firstNameSexTest.firstName != null) {
                return false;
            }
        } else if (!firstName.equals(firstNameSexTest.firstName)) {
            return false;
        }
        if (gender == null) {
            if (firstNameSexTest.gender != null) {
                return false;
            }
        } else if (!gender.equals(firstNameSexTest.gender)) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}