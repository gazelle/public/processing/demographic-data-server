/*
 * Copyright 2009 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.dds.model;

import net.ihe.gazelle.dds.action.AddressInfoBuilder;
import net.ihe.gazelle.dds.action.NationalPatientIdentifierGenerator;
import net.ihe.gazelle.dds.geonames.GeoName;
import org.jboss.seam.annotations.Name;
import org.jdom.JDOMException;

import javax.persistence.*;
import java.io.IOException;
import java.util.*;


/**
 * <b>Class Description :  </b>Patient<br><br>
 * This class describes a Patient object. It corresponds to a Patient generated through DemographicDataServer application.
 * It corresponds to a medical Information for a Patient (eg: Patient with name, address, etc...).
 * <p>
 * Patient object possesses the following attributes :
 * <ul>
 * <li><b>id</b> : id corresponding to the Patient</li>
 * <li><b>person</b> : Object corresponding to a person, with some informations (eg. firstname, lastname, race, religion, etc...)</li>
 * <li><b>address</b> : Object corresponding to the address of a person, with some informations (eg. street, city, country, etc...)</li>
 * <li><b>comment</b> : Object corresponding to a comment on this patient (eg. "Generated through DDS")</li>
 * <li><b>patientId</b> : String corresponding to the Identification of this patient. It depends of each country</li>
 * </ul>
 * <b>Example of Patient</b> : Patient(id, person, address, comment) = (1, 25, 56, "Generated through DDS") <br>
 *
 * @author Nicolas Lefebvre - Abderrazek Boufahja - Samii Ayed - Jean-Renan Chatel / INRIA Rennes IHE development Project
 * @version 2.0 - 2010, November 24th
 */


@Entity
@Name("patient")
@Table(name = "dds_patient", schema = "public")
@org.hibernate.annotations.Entity(selectBeforeUpdate = true)
@SequenceGenerator(name = "dds_patient_sequence", sequenceName = "dds_patient_id_seq", allocationSize = 1)
public class Patient extends AuditModule implements java.io.Serializable {


    /**
     * Serial ID version of this object
     */
    private static final long serialVersionUID = -450913331541293760L;

    //	Attributes (existing in database as a column)
    /**
     * Addresses for this patient
     */
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "dds_patient_patient_address", joinColumns = @JoinColumn(name = "patient_id"), inverseJoinColumns = @JoinColumn(name = "patient_address_id"), uniqueConstraints = @UniqueConstraint(columnNames = {"patient_id", "patient_address_id"}))
    List<PatientAddress> address;
    /**
     * Id of this Patient object
     */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "dds_patient_sequence")
    private Integer id;

    //	 Variables used for Foreign Keys : dds_patient_address table
    /**
     * person_dds object corresponding to this patient
     */
    @ManyToOne
    @JoinColumn(name = "person_dds_id")
    private Person person;
    /**
     * Date of creation of this user corresponding to this user
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "creation_date")
    private Date creationDate;

    /**
     * The Patient Id
     */
    @Column(name = "national_patient_identifier")
    private String nationalPatientIdentifier;

    @OneToOne(fetch = FetchType.LAZY)
    private CountryCode country;

    @Column(name = "dds_patient_identifier")
    private String ddsPatientIdentifier;

    //Only for NewBorns
    @OneToOne
    private Patient motherPatient;


    //	Constructors

    public Patient() {

    }

    public Patient(Person person, List<PatientAddress> address) {
        this.person = person;
        this.address = address;
    }

    public Patient(Patient patient) {
        this.person = new Person(patient.getPerson());
        this.creationDate = (Date) patient.getCreationDate();

        List<PatientAddress> patientAddresses = new Vector<PatientAddress>();

        for (int i = 0; i < patient.getAddress().size(); i++) {
            patientAddresses.add(new PatientAddress(patient.getAddress().get(i)));
        }
        this.address = patientAddresses;

    }

    public static Patient generateRandomPatient(
            String countryCode,
            boolean lastNameOption,
            boolean firstNameOption,
            boolean motherMaidenNameOption,
            boolean religionOption,
            boolean raceOption,
            boolean birthDayOption,
            boolean addressOption,
            Gender genderOption,
            String firstNameNearby,
            String lastNameNearby,
            String firstNameLike,
            String lastNameLike,
            String maritalSatusOption,
            boolean deadPatientOption,
            boolean maidenNameOption,
            boolean aliasNameOption,
            boolean displayNameOption,
            boolean newBornMotherOption,
            String lastNameIsForNewBorn,
            EntityManager entityManager) throws JDOMException, IOException {
        Patient patient = new Patient();
        Patient mother = null;
        List<PatientAddress> patientAddressList = new Vector<PatientAddress>();

        if (newBornMotherOption) {
            mother = new Patient();
            //To have a Married Mother or not
            if (Math.random() > 0.5) {
                String motherMaritalStatus = "M";

                mother = generateRandomPatient(
                        countryCode,
                        true,
                        true,
                        true,
                        religionOption,
                        raceOption,
                        true,
                        false,
                        Gender.getGender_Female(),
                        null,
                        null,
                        null,
                        null,
                        motherMaritalStatus,
                        false,
                        true,
                        false,
                        false,
                        false,
                        null,
                        entityManager);
            } else {
                String motherMaritalStatus = "S";

                mother = generateRandomPatient(
                        countryCode,
                        true,
                        true,
                        true,
                        religionOption,
                        raceOption,
                        true,
                        false,
                        Gender.getGender_Female(),
                        null,
                        null,
                        null,
                        null,
                        motherMaritalStatus,
                        false,
                        false,
                        false,
                        false,
                        false,
                        null,
                        entityManager);
            }

            mother.getPerson().setDateOfBirth(Person.getDateBetweenTwoAge(15, 45));

        }


        Person person = Person.generateRandomPerson(
                countryCode,
                lastNameOption,
                firstNameOption,
                motherMaidenNameOption,
                religionOption,
                raceOption,
                birthDayOption,
                genderOption,
                firstNameNearby,
                lastNameNearby,
                firstNameLike,
                lastNameLike,
                maritalSatusOption,
                deadPatientOption,
                maidenNameOption,
                aliasNameOption,
                displayNameOption,
                newBornMotherOption,
                mother);

        CountryCode country = CountryCode.getCountryByCountryCode(countryCode);
        patient.setCountry(country);
        patient.setPerson(person);

        if (addressOption) {
            PatientAddress patientAddress = AddressInfoBuilder.constructAddress(countryCode);
            if (patientAddress != null) {
                patientAddressList.add(patientAddress);
            }
        }
        patient.setAddress(patientAddressList);
        Calendar cal = Calendar.getInstance();
        patient.setCreationDate(cal.getTime());

        //Generate and set the Patient ID
        patient.setNationalPatientIdentifier(NationalPatientIdentifierGenerator.generate(patient));

        if (newBornMotherOption) {
            mother.setAddress(patient.getAddress());
            //The mother get the child name and NOT the opposite. This is because of the
            //"Is Like" and "Like" Option...
            if (!lastNameOption && (lastNameIsForNewBorn != null && !lastNameIsForNewBorn.isEmpty())) {
                mother.getPerson().setLastName(null);
                mother.getPerson().setLastAlternativeName(lastNameIsForNewBorn);
            } else {
                mother.getPerson().setLastName(patient.getPerson().getLastName());
                mother.getPerson().setLastAlternativeName(patient.getPerson().getLastAlternativeName());
            }

            mother.setNationalPatientIdentifier(NationalPatientIdentifierGenerator.generate(mother));

            mother = mergePatient(mother, entityManager);
            patient.setMotherPatient(mother);
        }

        return patient;

    }

    //New Method
    public static Patient generateRandomPatient(
            String countryCode,
            GeoName inGeoName,
            boolean lastNameOption,
            boolean firstNameOption,
            boolean motherMaidenNameOption,
            boolean religionOption,
            boolean raceOption,
            boolean birthDayOption,
            boolean addressOption,
            Gender genderOption,
            String firstNameNearby,
            String lastNameNearby,
            String firstNameLike,
            String lastNameLike,
            String maritalSatusOption,
            boolean deadPatientOption,
            boolean maidenNameOption,
            boolean aliasNameOption,
            boolean displayNameOption,
            boolean newBornMotherOption,
            String lastNameIs,
            EntityManager entityManager) throws JDOMException, IOException {

        Patient patient = new Patient();
        Patient mother = null;
        List<PatientAddress> patientAddressList = new Vector<PatientAddress>();

        if (newBornMotherOption) {
            mother = new Patient();
            //To have a Married Mother or not
            if (Math.random() > 0.5) {
                String motherMaritalStatus = "M";

                mother = generateRandomPatient(
                        countryCode,
                        inGeoName,
                        true,
                        true,
                        true,
                        religionOption,
                        raceOption,
                        true,
                        false,
                        Gender.getGender_Female(),
                        null,
                        null,
                        null,
                        null,
                        motherMaritalStatus,
                        false,
                        true,
                        false,
                        false,
                        false,
                        null,
                        entityManager);

            } else {
                String motherMaritalStatus = "S";

                mother = generateRandomPatient(
                        countryCode,
                        inGeoName,
                        true,
                        true,
                        true,
                        religionOption,
                        raceOption,
                        true,
                        false,
                        Gender.getGender_Female(),
                        null,
                        null,
                        null,
                        null,
                        motherMaritalStatus,
                        false,
                        false,
                        false,
                        false,
                        false,
                        null,
                        entityManager);
            }

            mother.getPerson().setDateOfBirth(Person.getDateBetweenTwoAge(15, 45));

        }


        Person person = Person.generateRandomPerson(
                countryCode,
                lastNameOption,
                firstNameOption,
                motherMaidenNameOption,
                religionOption,
                raceOption,
                birthDayOption,
                genderOption,
                firstNameNearby,
                lastNameNearby,
                firstNameLike,
                lastNameLike,
                maritalSatusOption,
                deadPatientOption,
                maidenNameOption,
                aliasNameOption,
                displayNameOption,
                newBornMotherOption,
                mother);

        CountryCode country = CountryCode.getCountryByCountryCode(countryCode);
        patient.setCountry(country);
        patient.setPerson(person);

        if (addressOption) {
            PatientAddress patientAddress = AddressInfoBuilder.constructAddress(inGeoName);
            if (patientAddress != null) {
                patientAddressList.add(patientAddress);
            }
        }
        patient.setAddress(patientAddressList);
        Calendar cal = Calendar.getInstance();
        patient.setCreationDate(cal.getTime());

        //Generate and set the Patient ID
        patient.setNationalPatientIdentifier(NationalPatientIdentifierGenerator.generate(patient));

        if (newBornMotherOption) {
            mother.setAddress(patient.getAddress());
            //The mother get the child name and NOT the opposite. This is because of the
            //"Is Like" and "Like" Option...
            if (!lastNameOption && (lastNameIs != null && !lastNameIs.isEmpty())) {
                mother.getPerson().setLastName(null);
                mother.getPerson().setLastAlternativeName(lastNameIs);
            } else {
                mother.getPerson().setLastName(patient.getPerson().getLastName());
                mother.getPerson().setLastAlternativeName(patient.getPerson().getLastAlternativeName());
            }

            mother.setNationalPatientIdentifier(NationalPatientIdentifierGenerator.generate(mother));

            mother = mergePatient(mother, entityManager);
            patient.setMotherPatient(mother);
        }

        return patient;
    }


    // *********************************************************
    //  Getters and Setters : setup the DB columns properties
    // *********************************************************

    public static Patient generateRandomPatient(
            String countryCode,
            Double latitude,
            Double longitude,
            boolean lastNameOption,
            boolean firstNameOption,
            boolean motherMaidenNameOption,
            boolean religionOption,
            boolean raceOption,
            boolean birthDayOption,
            boolean addressOption,
            Gender genderOption,
            String firstNameNearby,
            String lastNameNearby,
            String firstNameLike,
            String lastNameLike,
            String maritalSatusOption,
            boolean deadPatientOption,
            boolean maidenNameOption,
            boolean aliasNameOption,
            boolean displayNameOption,
            boolean newBornMotherOption,
            String lastNameIs,
            EntityManager entityManager) throws JDOMException, IOException {

        Patient patient = new Patient();
        Patient mother = null;
        List<PatientAddress> patientAddressList = new Vector<PatientAddress>();

        if (newBornMotherOption) {
            mother = new Patient();
            //To have a Married Mother or not
            if (Math.random() > 0.5) {
                String motherMaritalStatus = "M";

                mother = generateRandomPatient(
                        countryCode,
                        latitude,
                        longitude,
                        true,
                        true,
                        true,
                        religionOption,
                        raceOption,
                        true,
                        false,
                        Gender.getGender_Female(),
                        null,
                        null,
                        null,
                        null,
                        motherMaritalStatus,
                        false,
                        true,
                        false,
                        false,
                        false,
                        null,
                        entityManager);
            } else {
                String motherMaritalStatus = "S";

                mother = generateRandomPatient(
                        countryCode,
                        latitude,
                        longitude,
                        true,
                        true,
                        true,
                        religionOption,
                        raceOption,
                        true,
                        false,
                        Gender.getGender_Female(),
                        null,
                        null,
                        null,
                        null,
                        motherMaritalStatus,
                        false,
                        true,
                        false,
                        false,
                        false,
                        null,
                        entityManager);
            }

            mother.getPerson().setDateOfBirth(Person.getDateBetweenTwoAge(15, 45));
        }


        Person person = Person.generateRandomPerson(
                countryCode,
                lastNameOption,
                firstNameOption,
                motherMaidenNameOption,
                religionOption,
                raceOption,
                birthDayOption,
                genderOption,
                firstNameNearby,
                lastNameNearby,
                firstNameLike,
                lastNameLike,
                maritalSatusOption,
                deadPatientOption,
                maidenNameOption,
                aliasNameOption,
                displayNameOption,
                newBornMotherOption,
                mother);


        CountryCode country = CountryCode.getCountryByCountryCode(countryCode);
        patient.setCountry(country);
        patient.setPerson(person);

        if (addressOption) {
            PatientAddress patientAddress = AddressInfoBuilder.constructNearbyAddress(
                    countryCode,
                    latitude.doubleValue(),
                    longitude.doubleValue());

            if (patientAddress != null) {
                patientAddressList.add(patientAddress);
            }
        }
        patient.setAddress(patientAddressList);
        Calendar cal = Calendar.getInstance();
        patient.setCreationDate(cal.getTime());

        //Generate and set the Patient ID
        patient.setNationalPatientIdentifier(NationalPatientIdentifierGenerator.generate(patient));

        if (newBornMotherOption) {
            mother.setAddress(patient.getAddress());
            //The mother get the child name and NOT the opposite. This is because of the
            //"Is Like" and "Like" Option...
            if (!lastNameOption && (lastNameIs != null && !lastNameIs.isEmpty())) {
                mother.getPerson().setLastName(null);
                mother.getPerson().setLastAlternativeName(lastNameIs);
            } else {
                mother.getPerson().setLastName(patient.getPerson().getLastName());
                mother.getPerson().setLastAlternativeName(patient.getPerson().getLastAlternativeName());
            }

            mother.setNationalPatientIdentifier(NationalPatientIdentifierGenerator.generate(mother));

            mother = mergePatient(mother, entityManager);
            patient.setMotherPatient(mother);
        }

        return patient;

    }

    public static int generateRandomInteger(int min, int max) {
        return (int) (min + Math.random() * (max - min + 1));
    }

    public static Patient mergePatient(Patient patient, EntityManager entityManager) {
        List<PatientAddress> patientAddressList = new ArrayList<PatientAddress>();
        if (patient != null) {
            Person person = patient.getPerson();
            person = entityManager.merge(person);
            patient.setPerson(person);

            List<PatientAddress> patientAddressesList = patient.getAddress();
            if (patientAddressesList != null) {
                for (PatientAddress patientAddress : patientAddressesList) {
                    Street street = patientAddress.getStreet();
                    if (street != null) {
                        street = entityManager.merge(street);
                    }
                    City city = patientAddress.getCity();
                    if (city != null) {
                        city = entityManager.merge(city);
                    }
                    State state = patientAddress.getState();
                    if (state != null) {
                        state = entityManager.merge(state);
                    }
                    patientAddress.setState(state);
                    patientAddress.setCity(city);
                    patientAddress.setStreet(street);

                    patientAddress = entityManager.merge(patientAddress);

                    patientAddressList.add(patientAddress);
                }
            }
            patient.setAddress(patientAddressList);
            patient = entityManager.merge(patient);
            entityManager.flush();

            String apdomaine = ApplicationConfiguration.getValueOfVariable("DDS_domain");
            String apoid = ApplicationConfiguration.getValueOfVariable("DDS_OID");

            String ddsIdentifier = apdomaine + "-" + patient.getId().toString() + "^^^" + apdomaine + "&" + apoid + "&ISO";
            //"DDS-6996^^^DDS&1.3.6.1.4.1.12559.11.1.4.1.2&ISO"
            patient.setDdsPatientIdentifier(ddsIdentifier);

            if (patient.getCountry().getCountryOID() != null && patient.getNationalPatientIdentifier() != null) {
                String nationalPatientId = patient.getNationalPatientIdentifier() + "^^^" + patient.getCountry().getCx_4_code() + "&" + patient.getCountry().getCountryOID() + "&ISO";
                patient.setNationalPatientIdentifier(nationalPatientId);
            }

            patient = entityManager.merge(patient);
            entityManager.flush();
        }
        return patient;
    }

    public static Patient getPatientById(Integer id) {
        PatientQuery query = new PatientQuery();
        query.id().eq(id);
        return query.getUniqueResult();
    }

    public static Patient getRandomPatientFromDatabase(String countryCode) {
        PatientQuery query = new PatientQuery();
        query.country().iso().eq(countryCode);
        List<Integer> ids = query.id().getListDistinct();
        if (ids != null && !ids.isEmpty()) {
            Random random = new Random();
            Integer id = random.nextInt(ids.size());
            query = new PatientQuery();
            query.id().eq(ids.get(id));
            return query.getUniqueResult();
        } else {
            return null;
        }
    }

    public String getDdsPatientIdentifier() {
        return ddsPatientIdentifier;
    }

    public void setDdsPatientIdentifier(String ddsPatientIdentifier) {
        this.ddsPatientIdentifier = ddsPatientIdentifier;
    }

    public Integer getId() {
        return id;
    }

    public CountryCode getCountry() {
        return country;
    }

    public void setCountry(CountryCode country) {
        this.country = country;
    }

    public Person getPerson() {
        return person;
    }


    // hashCode and equals ///////////////////////////////////////////////////////

    public void setPerson(Person person) {
        this.person = person;
    }

    public List<PatientAddress> getAddress() {
        return address;
    }

    public void setAddress(List<PatientAddress> address) {
        this.address = address;
    }


    // methods //////////////////////////////////////////

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public String getNationalPatientIdentifier() {
        return nationalPatientIdentifier;
    }

    public void setNationalPatientIdentifier(String nationalPatientId) {
        this.nationalPatientIdentifier = nationalPatientId;
    }

    public Patient getMotherPatient() {
        return motherPatient;
    }

    public void setMotherPatient(Patient motherPatient) {
        this.motherPatient = motherPatient;
    }

    public String toString() {
        return "Patient [address=" + address + ", creationDate=" + creationDate
                + ", id=" + id + ", person=" + person + ", patientID=" + nationalPatientIdentifier + "]";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((address == null) ? 0 : address.hashCode());
        result = prime * result + ((country == null) ? 0 : country.hashCode());
        result = prime * result
                + ((creationDate == null) ? 0 : creationDate.hashCode());
        result = prime
                * result
                + ((ddsPatientIdentifier == null) ? 0 : ddsPatientIdentifier
                .hashCode());
        result = prime
                * result
                + ((nationalPatientIdentifier == null) ? 0
                : nationalPatientIdentifier.hashCode());
        result = prime * result + ((person == null) ? 0 : person.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Patient other = (Patient) obj;
        if (address == null) {
            if (other.address != null) {
                return false;
            }
        } else if (!address.equals(other.address)) {
            return false;
        }
        if (country == null) {
            if (other.country != null) {
                return false;
            }
        } else if (!country.equals(other.country)) {
            return false;
        }
        if (creationDate == null) {
            if (other.creationDate != null) {
                return false;
            }
        } else if (!creationDate.equals(other.creationDate)) {
            return false;
        }
        if (ddsPatientIdentifier == null) {
            if (other.ddsPatientIdentifier != null) {
                return false;
            }
        } else if (!ddsPatientIdentifier.equals(other.ddsPatientIdentifier)) {
            return false;
        }
        if (nationalPatientIdentifier == null) {
            if (other.nationalPatientIdentifier != null) {
                return false;
            }
        } else if (!nationalPatientIdentifier
                .equals(other.nationalPatientIdentifier)) {
            return false;
        }
        if (person == null) {
            if (other.person != null) {
                return false;
            }
        } else if (!person.equals(other.person)) {
            return false;
        }
        return true;
    }

}
