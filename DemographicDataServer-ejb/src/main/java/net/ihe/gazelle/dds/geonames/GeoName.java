/*
 * Copyright 2009 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.dds.geonames;

import net.ihe.gazelle.dds.model.AuditModule;
import org.jboss.seam.annotations.Name;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.math.BigInteger;
import java.util.List;

/**
 * @author abderrazek boufahja
 */
@Entity
@Name("geoName")
@Table(name = "geoname")
@org.hibernate.annotations.Entity(selectBeforeUpdate = true)
@SequenceGenerator(name = "geoname_sequence", sequenceName = "geoname_id_seq", allocationSize = 1)
public class GeoName extends AuditModule implements Serializable, Comparable<GeoName> {

    //~ Statics variables and Class initializer ///////////////////////////////////////////////////////////////////////

    /**
     * Serial version Id of this object!
     */
    private static final long serialVersionUID = 1881678500711441951L;

    //~ Attributes ////////////////////////////////////////////////////////////////////////////////////////////////////
    @Id
    @Column(name = "geonameid", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "geoname_sequence")
    private Integer id;

    @Column(name = "name", unique = false, nullable = true)
    @Size(max = 200)
    private String name;

    @Column(name = "asciiname", unique = false, nullable = true)
    @Size(max = 200)
    private String asciiname;

    @Column(name = "alternatenames", unique = false, nullable = true)
    @Size(max = 4000)
    private String alternatenames;

    @Column(name = "latitude", unique = false, nullable = true)
    private Double latitude;

    @Column(name = "longitude", unique = false, nullable = true)
    private Double longitude;

    @Column(name = "fclass", unique = false, nullable = true)
    private Character fclass;

    @Column(name = "fcode", unique = false, nullable = true)
    @Size(max = 10)
    private String fcode;

    @Column(name = "country", unique = false, nullable = true)
    @Size(max = 2)
    private String country;

    @Column(name = "cc2", unique = false, nullable = true)
    @Size(max = 60)
    private String cc2;

    @Column(name = "admin1", unique = false, nullable = true)
    @Size(max = 20)
    private String admin1;

    @Column(name = "admin2", unique = false, nullable = true)
    @Size(max = 80)
    private String admin2;

    @Column(name = "admin3", unique = false, nullable = true)
    @Size(max = 20)
    private String admin3;

    @Column(name = "admin4", unique = false, nullable = true)
    @Size(max = 20)
    private String admin4;

    @Column(name = "population", unique = false, nullable = true)
    private BigInteger population;

    @Column(name = "elevation", unique = false, nullable = true)
    private Integer elevation;

    @Column(name = "gtopo30", unique = false, nullable = true)
    private Integer gtopo30;

    @Column(name = "timezone", unique = false, nullable = true)
    @Size(max = 40)
    private String timezone;

    @Column(name = "moddate", unique = false, nullable = true)
    @Size(max = 40)
    private String moddate;

    // constructors  ////////////////////////////////////////////////////////////////////////

    public GeoName() {
    }

    public GeoName(String name, String asciiname, String alternatenames,
                   Double latitude, Double longitude, Character fclass, String fcode,
                   String country, String cc2, String admin1, String admin2,
                   String admin3, String admin4, BigInteger population, Integer elevation,
                   Integer gtopo30, String timezone, String moddate) {
        super();
        this.name = name;
        this.asciiname = asciiname;
        this.alternatenames = alternatenames;
        this.latitude = latitude;
        this.longitude = longitude;
        this.fclass = fclass;
        this.fcode = fcode;
        this.country = country;
        this.cc2 = cc2;
        this.admin1 = admin1;
        this.admin2 = admin2;
        this.admin3 = admin3;
        this.admin4 = admin4;
        this.population = population;
        this.elevation = elevation;
        this.gtopo30 = gtopo30;
        this.timezone = timezone;
        this.moddate = moddate;
    }

    public static List<GeoName> getGeoNameFiltered(String inCountry, String townName) {
        GeoNameQuery query = new GeoNameQuery();
        if (inCountry != null && !inCountry.isEmpty()) {
            query.country().eq(inCountry);
        }
        if (townName != null && !townName.isEmpty()) {
            query.name().eq(townName);
        }
        return query.getListDistinct();
    }

    // getters and setters //////////////////////////////////////////////////////////////////
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAsciiname() {
        return asciiname;
    }

    public void setAsciiname(String asciiname) {
        this.asciiname = asciiname;
    }

    public String getAlternatenames() {
        return alternatenames;
    }

    public void setAlternatenames(String alternatenames) {
        this.alternatenames = alternatenames;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public char getFclass() {
        return fclass;
    }

    public void setFclass(Character fclass) {
        this.fclass = fclass;
    }

    public String getFcode() {
        return fcode;
    }

    public void setFcode(String fcode) {
        this.fcode = fcode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCc2() {
        return cc2;
    }

    public void setCc2(String cc2) {
        this.cc2 = cc2;
    }

    public String getAdmin1() {
        return admin1;
    }

    public void setAdmin1(String admin1) {
        this.admin1 = admin1;
    }

    public String getAdmin2() {
        return admin2;
    }

    public void setAdmin2(String admin2) {
        this.admin2 = admin2;
    }

    public String getAdmin3() {
        return admin3;
    }

    public void setAdmin3(String admin3) {
        this.admin3 = admin3;
    }

    public String getAdmin4() {
        return admin4;
    }

    public void setAdmin4(String admin4) {
        this.admin4 = admin4;
    }

    public BigInteger getPopulation() {
        return population;
    }

    public void setPopulation(BigInteger population) {
        this.population = population;
    }

    public Integer getElevation() {
        return elevation;
    }

    public void setElevation(Integer elevation) {
        this.elevation = elevation;
    }

    public int getGtopo30() {
        return gtopo30;
    }

    public void setGtopo30(Integer gtopo30) {
        this.gtopo30 = gtopo30;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public String getModdate() {
        return moddate;
    }


    /// equals and hashCode //////////////////////////////////////////////////////////////////

    public void setModdate(String moddate) {
        this.moddate = moddate;
    }

    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((admin1 == null) ? 0 : admin1.hashCode());
        result = prime * result + ((admin2 == null) ? 0 : admin2.hashCode());
        result = prime * result + ((admin3 == null) ? 0 : admin3.hashCode());
        result = prime * result + ((admin4 == null) ? 0 : admin4.hashCode());
        result = prime * result
                + ((alternatenames == null) ? 0 : alternatenames.hashCode());
        result = prime * result
                + ((asciiname == null) ? 0 : asciiname.hashCode());
        result = prime * result + ((cc2 == null) ? 0 : cc2.hashCode());
        result = prime * result + ((country == null) ? 0 : country.hashCode());
        result = prime * result
                + ((elevation == null) ? 0 : elevation.hashCode());
        result = prime * result + ((fclass == null) ? 0 : fclass.hashCode());
        result = prime * result + ((fcode == null) ? 0 : fcode.hashCode());
        result = prime * result + ((gtopo30 == null) ? 0 : gtopo30.hashCode());
        result = prime * result
                + ((latitude == null) ? 0 : latitude.hashCode());
        result = prime * result
                + ((longitude == null) ? 0 : longitude.hashCode());
        result = prime * result + ((moddate == null) ? 0 : moddate.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result
                + ((population == null) ? 0 : population.hashCode());
        result = prime * result
                + ((timezone == null) ? 0 : timezone.hashCode());
        return result;
    }

    // methods  ///////////////////////////////////////////////////////////////////////////

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        GeoName other = (GeoName) obj;
        if (admin1 == null) {
            if (other.admin1 != null) {
                return false;
            }
        } else if (!admin1.equals(other.admin1)) {
            return false;
        }
        if (admin2 == null) {
            if (other.admin2 != null) {
                return false;
            }
        } else if (!admin2.equals(other.admin2)) {
            return false;
        }
        if (admin3 == null) {
            if (other.admin3 != null) {
                return false;
            }
        } else if (!admin3.equals(other.admin3)) {
            return false;
        }
        if (admin4 == null) {
            if (other.admin4 != null) {
                return false;
            }
        } else if (!admin4.equals(other.admin4)) {
            return false;
        }
        if (alternatenames == null) {
            if (other.alternatenames != null) {
                return false;
            }
        } else if (!alternatenames.equals(other.alternatenames)) {
            return false;
        }
        if (asciiname == null) {
            if (other.asciiname != null) {
                return false;
            }
        } else if (!asciiname.equals(other.asciiname)) {
            return false;
        }
        if (cc2 == null) {
            if (other.cc2 != null) {
                return false;
            }
        } else if (!cc2.equals(other.cc2)) {
            return false;
        }
        if (country == null) {
            if (other.country != null) {
                return false;
            }
        } else if (!country.equals(other.country)) {
            return false;
        }
        if (elevation == null) {
            if (other.elevation != null) {
                return false;
            }
        } else if (!elevation.equals(other.elevation)) {
            return false;
        }
        if (fclass == null) {
            if (other.fclass != null) {
                return false;
            }
        } else if (!fclass.equals(other.fclass)) {
            return false;
        }
        if (fcode == null) {
            if (other.fcode != null) {
                return false;
            }
        } else if (!fcode.equals(other.fcode)) {
            return false;
        }
        if (gtopo30 == null) {
            if (other.gtopo30 != null) {
                return false;
            }
        } else if (!gtopo30.equals(other.gtopo30)) {
            return false;
        }
        if (latitude == null) {
            if (other.latitude != null) {
                return false;
            }
        } else if (!latitude.equals(other.latitude)) {
            return false;
        }
        if (longitude == null) {
            if (other.longitude != null) {
                return false;
            }
        } else if (!longitude.equals(other.longitude)) {
            return false;
        }
        if (moddate == null) {
            if (other.moddate != null) {
                return false;
            }
        } else if (!moddate.equals(other.moddate)) {
            return false;
        }
        if (name == null) {
            if (other.name != null) {
                return false;
            }
        } else if (!name.equals(other.name)) {
            return false;
        }
        if (population == null) {
            if (other.population != null) {
                return false;
            }
        } else if (!population.equals(other.population)) {
            return false;
        }
        if (timezone == null) {
            if (other.timezone != null) {
                return false;
            }
        } else if (!timezone.equals(other.timezone)) {
            return false;
        }
        return true;
    }

    @Override
    public int compareTo(GeoName o) {
        return getName().compareTo(o.getName());
    }

}
