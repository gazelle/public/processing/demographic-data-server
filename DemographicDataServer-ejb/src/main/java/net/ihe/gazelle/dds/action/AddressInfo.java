/*
 * Copyright 2009 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.dds.action;

import net.ihe.gazelle.dds.model.*;

import java.util.List;

/**
 * @author Abderrazek boufahja
 */
public class AddressInfo {

    private String countryGeonameId;
    private Double countryBoundingBoxWest;
    private Double countryBoundingBoxEast;
    private Double countryBoundingBoxNorth;
    private Double countryBoundingBoxSouth;
    private Double entityLat;
    private Double entityLng;
    private Double cityLat;
    private Double cityLng;
    private String address;
    private String countryNameCode;
    private String country;
    private String countryName;
    private String localityName;
    private String locality;
    private String postalCodeNumber;
    private String postalCode;
    private String thoroughfareName;
    private String administrativeAreaName;
    private String subAdministrativeArea;
    private String subAdministrativeAreaName;
    private String thoroughfare;
    private List<String> countryLanguages;


    // construcotrs  //////////////////////////////////////////////////////////////////////

    public AddressInfo() {
    }

    public AddressInfo(String countryGeonameId, Double countryBoundingBoxWest,
                       Double countryBoundingBoxEast, Double countryBoundingBoxNorth,
                       Double countryBoundingBoxSouth, Double entityLat, Double entityLng,
                       Double cityLat, Double cityLng, String address,
                       String countryNameCode, String country, String countryName,
                       String localityName, String locality, String postalCodeNumber,
                       String postalCode, String thoroughfareName,
                       String administrativeAreaName, String subAdministrativeArea,
                       String subAdministrativeAreaName, String thoroughfare,
                       List<String> countryLanguages) {
        this.countryGeonameId = countryGeonameId;
        this.countryBoundingBoxWest = countryBoundingBoxWest;
        this.countryBoundingBoxEast = countryBoundingBoxEast;
        this.countryBoundingBoxNorth = countryBoundingBoxNorth;
        this.countryBoundingBoxSouth = countryBoundingBoxSouth;
        this.entityLat = entityLat;
        this.entityLng = entityLng;
        this.cityLat = cityLat;
        this.cityLng = cityLng;
        this.address = address;
        this.countryNameCode = countryNameCode;
        this.country = country;
        this.countryName = countryName;
        this.localityName = localityName;
        this.locality = locality;
        this.postalCodeNumber = postalCodeNumber;
        this.postalCode = postalCode;
        this.thoroughfareName = thoroughfareName;
        this.administrativeAreaName = administrativeAreaName;
        this.subAdministrativeArea = subAdministrativeArea;
        this.subAdministrativeAreaName = subAdministrativeAreaName;
        this.thoroughfare = thoroughfare;
        this.countryLanguages = countryLanguages;
    }

    // getters and setters //////////////////////////////////////////////////////////////////////

    public String getCountryGeonameId() {
        return countryGeonameId;
    }

    public void setCountryGeonameId(String countryGeonameId) {
        this.countryGeonameId = countryGeonameId;
    }

    public Double getCountryBoundingBoxWest() {
        return countryBoundingBoxWest;
    }

    public void setCountryBoundingBoxWest(Double countryBoundingBoxWest) {
        this.countryBoundingBoxWest = countryBoundingBoxWest;
    }

    public Double getCountryBoundingBoxEast() {
        return countryBoundingBoxEast;
    }

    public void setCountryBoundingBoxEast(Double countryBoundingBoxEast) {
        this.countryBoundingBoxEast = countryBoundingBoxEast;
    }

    public Double getCountryBoundingBoxNorth() {
        return countryBoundingBoxNorth;
    }

    public void setCountryBoundingBoxNorth(Double countryBoundingBoxNorth) {
        this.countryBoundingBoxNorth = countryBoundingBoxNorth;
    }

    public Double getCountryBoundingBoxSouth() {
        return countryBoundingBoxSouth;
    }

    public void setCountryBoundingBoxSouth(Double countryBoundingBoxSouth) {
        this.countryBoundingBoxSouth = countryBoundingBoxSouth;
    }

    public Double getEntityLat() {
        return entityLat;
    }

    public void setEntityLat(Double entityLat) {
        this.entityLat = entityLat;
    }

    public Double getEntityLng() {
        return entityLng;
    }

    public void setEntityLng(Double entityLng) {
        this.entityLng = entityLng;
    }

    public Double getCityLat() {
        return cityLat;
    }

    public void setCityLat(Double cityLat) {
        this.cityLat = cityLat;
    }

    public Double getCityLng() {
        return cityLng;
    }

    public void setCityLng(Double cityLng) {
        this.cityLng = cityLng;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCountryNameCode() {
        return countryNameCode;
    }

    public void setCountryNameCode(String countryNameCode) {
        this.countryNameCode = countryNameCode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getLocalityName() {
        return localityName;
    }

    public void setLocalityName(String localityName) {
        this.localityName = localityName;
    }

    public String getLocality() {
        return locality;
    }

    public void setLocality(String locality) {
        this.locality = locality;
    }

    public String getPostalCodeNumber() {
        return postalCodeNumber;
    }

    public void setPostalCodeNumber(String postalCodeNumber) {
        this.postalCodeNumber = postalCodeNumber;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getThoroughfareName() {
        return thoroughfareName;
    }

    public void setThoroughfareName(String thoroughfareName) {
        this.thoroughfareName = thoroughfareName;
    }

    public String getAdministrativeAreaName() {
        return administrativeAreaName;
    }

    public void setAdministrativeAreaName(String administrativeAreaName) {
        this.administrativeAreaName = administrativeAreaName;
    }

    public String getSubAdministrativeArea() {
        return subAdministrativeArea;
    }

    public void setSubAdministrativeArea(String subAdministrativeArea) {
        this.subAdministrativeArea = subAdministrativeArea;
    }

    public String getSubAdministrativeAreaName() {
        return subAdministrativeAreaName;
    }

    public void setSubAdministrativeAreaName(String subAdministrativeAreaName) {
        this.subAdministrativeAreaName = subAdministrativeAreaName;
    }

    public String getThoroughfare() {
        return thoroughfare;
    }

    public void setThoroughfare(String thoroughfare) {
        this.thoroughfare = thoroughfare;
    }

    public List<String> getCountryLanguages() {
        return countryLanguages;
    }

    public void setCountryLanguages(List<String> countryLanguages) {
        this.countryLanguages = countryLanguages;
    }

    // equals and hashCode //////////////////////////////////////////////////////////

    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((address == null) ? 0 : address.hashCode());
        result = prime
                * result
                + ((administrativeAreaName == null) ? 0
                : administrativeAreaName.hashCode());
        result = prime * result + ((cityLat == null) ? 0 : cityLat.hashCode());
        result = prime * result + ((cityLng == null) ? 0 : cityLng.hashCode());
        result = prime * result + ((country == null) ? 0 : country.hashCode());
        result = prime
                * result
                + ((countryBoundingBoxEast == null) ? 0
                : countryBoundingBoxEast.hashCode());
        result = prime
                * result
                + ((countryBoundingBoxNorth == null) ? 0
                : countryBoundingBoxNorth.hashCode());
        result = prime
                * result
                + ((countryBoundingBoxSouth == null) ? 0
                : countryBoundingBoxSouth.hashCode());
        result = prime
                * result
                + ((countryBoundingBoxWest == null) ? 0
                : countryBoundingBoxWest.hashCode());
        result = prime
                * result
                + ((countryGeonameId == null) ? 0 : countryGeonameId.hashCode());
        result = prime
                * result
                + ((countryLanguages == null) ? 0 : countryLanguages.hashCode());
        result = prime * result
                + ((countryName == null) ? 0 : countryName.hashCode());
        result = prime * result
                + ((countryNameCode == null) ? 0 : countryNameCode.hashCode());
        result = prime * result
                + ((entityLat == null) ? 0 : entityLat.hashCode());
        result = prime * result
                + ((entityLng == null) ? 0 : entityLng.hashCode());
        result = prime * result
                + ((locality == null) ? 0 : locality.hashCode());
        result = prime * result
                + ((localityName == null) ? 0 : localityName.hashCode());
        result = prime * result
                + ((postalCode == null) ? 0 : postalCode.hashCode());
        result = prime
                * result
                + ((postalCodeNumber == null) ? 0 : postalCodeNumber.hashCode());
        result = prime
                * result
                + ((subAdministrativeArea == null) ? 0 : subAdministrativeArea
                .hashCode());
        result = prime
                * result
                + ((subAdministrativeAreaName == null) ? 0
                : subAdministrativeAreaName.hashCode());
        result = prime * result
                + ((thoroughfare == null) ? 0 : thoroughfare.hashCode());
        result = prime
                * result
                + ((thoroughfareName == null) ? 0 : thoroughfareName.hashCode());
        return result;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        AddressInfo other = (AddressInfo) obj;
        if (address == null) {
            if (other.address != null) {
                return false;
            }
        } else if (!address.equals(other.address)) {
            return false;
        }
        if (administrativeAreaName == null) {
            if (other.administrativeAreaName != null) {
                return false;
            }
        } else if (!administrativeAreaName.equals(other.administrativeAreaName)) {
            return false;
        }
        if (cityLat == null) {
            if (other.cityLat != null) {
                return false;
            }
        } else if (!cityLat.equals(other.cityLat)) {
            return false;
        }
        if (cityLng == null) {
            if (other.cityLng != null) {
                return false;
            }
        } else if (!cityLng.equals(other.cityLng)) {
            return false;
        }
        if (country == null) {
            if (other.country != null) {
                return false;
            }
        } else if (!country.equals(other.country)) {
            return false;
        }
        if (countryBoundingBoxEast == null) {
            if (other.countryBoundingBoxEast != null) {
                return false;
            }
        } else if (!countryBoundingBoxEast.equals(other.countryBoundingBoxEast)) {
            return false;
        }
        if (countryBoundingBoxNorth == null) {
            if (other.countryBoundingBoxNorth != null) {
                return false;
            }
        } else if (!countryBoundingBoxNorth
                .equals(other.countryBoundingBoxNorth)) {
            return false;
        }
        if (countryBoundingBoxSouth == null) {
            if (other.countryBoundingBoxSouth != null) {
                return false;
            }
        } else if (!countryBoundingBoxSouth
                .equals(other.countryBoundingBoxSouth)) {
            return false;
        }
        if (countryBoundingBoxWest == null) {
            if (other.countryBoundingBoxWest != null) {
                return false;
            }
        } else if (!countryBoundingBoxWest.equals(other.countryBoundingBoxWest)) {
            return false;
        }
        if (countryGeonameId == null) {
            if (other.countryGeonameId != null) {
                return false;
            }
        } else if (!countryGeonameId.equals(other.countryGeonameId)) {
            return false;
        }
        if (countryLanguages == null) {
            if (other.countryLanguages != null) {
                return false;
            }
        } else if (!countryLanguages.equals(other.countryLanguages)) {
            return false;
        }
        if (countryName == null) {
            if (other.countryName != null) {
                return false;
            }
        } else if (!countryName.equals(other.countryName)) {
            return false;
        }
        if (countryNameCode == null) {
            if (other.countryNameCode != null) {
                return false;
            }
        } else if (!countryNameCode.equals(other.countryNameCode)) {
            return false;
        }
        if (entityLat == null) {
            if (other.entityLat != null) {
                return false;
            }
        } else if (!entityLat.equals(other.entityLat)) {
            return false;
        }
        if (entityLng == null) {
            if (other.entityLng != null) {
                return false;
            }
        } else if (!entityLng.equals(other.entityLng)) {
            return false;
        }
        if (locality == null) {
            if (other.locality != null) {
                return false;
            }
        } else if (!locality.equals(other.locality)) {
            return false;
        }
        if (localityName == null) {
            if (other.localityName != null) {
                return false;
            }
        } else if (!localityName.equals(other.localityName)) {
            return false;
        }
        if (postalCode == null) {
            if (other.postalCode != null) {
                return false;
            }
        } else if (!postalCode.equals(other.postalCode)) {
            return false;
        }
        if (postalCodeNumber == null) {
            if (other.postalCodeNumber != null) {
                return false;
            }
        } else if (!postalCodeNumber.equals(other.postalCodeNumber)) {
            return false;
        }
        if (subAdministrativeArea == null) {
            if (other.subAdministrativeArea != null) {
                return false;
            }
        } else if (!subAdministrativeArea.equals(other.subAdministrativeArea)) {
            return false;
        }
        if (subAdministrativeAreaName == null) {
            if (other.subAdministrativeAreaName != null) {
                return false;
            }
        } else if (!subAdministrativeAreaName
                .equals(other.subAdministrativeAreaName)) {
            return false;
        }
        if (thoroughfare == null) {
            if (other.thoroughfare != null) {
                return false;
            }
        } else if (!thoroughfare.equals(other.thoroughfare)) {
            return false;
        }
        if (thoroughfareName == null) {
            if (other.thoroughfareName != null) {
                return false;
            }
        } else if (!thoroughfareName.equals(other.thoroughfareName)) {
            return false;
        }
        return true;
    }


    // methods //////////////////////////////////////////////////////////////////////

    public PatientAddress generatePatientAddress(String countryCode) {
        CountryCode cc = CountryCode.getCountryByCountryCode(countryCode);
        City city = null;
        if (this.getLocalityName() != null) {
            city = new City(this.getLocalityName());
        } else if (this.getSubAdministrativeAreaName() != null) {
            city = new City(this.getSubAdministrativeAreaName());
        } else if (this.getAdministrativeAreaName() != null) {
            city = new City(this.getAdministrativeAreaName());
        }

        Street street = new Street(this.getThoroughfareName());
        String pcn = this.getPostalCodeNumber();
        State state = null;
        if (countryCode.equals("US")) {
            List<State> listState = State.getStateFiltered(this.getAdministrativeAreaName());
            if (listState != null) {
                if (listState.size() > 0) {
                    state = listState.get(0);
                } else {
                    state = new State(this.getAdministrativeAreaName(), this.getAdministrativeAreaName());
                }
            } else {
                state = new State(this.getAdministrativeAreaName(), this.getAdministrativeAreaName());
            }
        }
        return new PatientAddress(null, street, pcn, city, cc, state, this.getCityLat(), this.getCityLng());
    }

}
