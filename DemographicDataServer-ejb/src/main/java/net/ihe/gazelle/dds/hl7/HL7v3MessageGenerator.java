/*
 * Copyright 2010 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.dds.hl7;

import net.ihe.gazelle.dds.action.SystemConfig;
import net.ihe.gazelle.dds.model.ApplicationConfiguration;
import net.ihe.gazelle.dds.model.AssigningAuthority;
import net.ihe.gazelle.dds.model.OtherName;
import net.ihe.gazelle.dds.model.Patient;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.log.Log;
import org.jboss.seam.log.Logging;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;


/**
 * <b>Class Description :  </b>HL7v3MessageGenerator<br><br>
 * This class provides methods to build HL7 v3 messages for registering a patient.
 *
 * @author Anne-Gaelle Berge / INRIA Rennes IHE development Project
 * @version 1.0 - 2010, April 6th
 *          <p>
 *          Sending HL7v3 message for registering patients using Gazelle requires a system implementing PIX-V3/PAT_IDENTITY_SRC actor and configured as HL7 V3 initiator
 *          Suffix keyword of this system must be "hl7v3Sender"
 */

public class HL7v3MessageGenerator {

    private static final String CODE_SYSTEM = "2.16.840.1.113883.1.6";
    private static final String CODE_ACTION = "PRPA_IN201301UV02";
    private static final String v3Namespace = "urn:hl7-org:v3";
    private static final String SENDING_APPLICATION = "Gazelle";
    private static final String DDS_NAME = "DDS";
    private static final String CODE = "code";
    private static final String V3ID = "v3:id";
    private static final String ROOT = "root";
    private static final String VALUE = "value";
    private static final String TYPECODE = "typeCode";
    private static final String CLASSCODE = "classCode";
    private static final String DETERMINERCODE = "determinerCode";
    private static final String INSTANCE = "INSTANCE";


    @Logger
    private static Log log = Logging.getLog(HL7v3MessageGenerator.class);
    private SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss", Locale.ENGLISH);
    private Calendar calendar = Calendar.getInstance();

    /**
     * Converts the patient's date of birth to string
     *
     * @param inDate
     * @return
     */
    private static String getDateAsString(Calendar inDate) {
        final String DATE_FORMAT_NOW = "yyyyMMdd";
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
        if (inDate != null) {
            return sdf.format(inDate.getTime());
        } else {
            return null;
        }
    }

    /**
     * Transform the dom document (XML) to a string, the one will be sent to systems
     *
     * @param document
     * @return
     */
    private static String xmlToString(Document document) {
        if (document == null) {
            return null;
        }
        try {
            Source source = new DOMSource(document);
            StringWriter stringWriter = new StringWriter();
            Result result = new StreamResult(stringWriter);
            TransformerFactory factory = TransformerFactory.newInstance();
            Transformer transformer = factory.newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
            transformer.transform(source, result);
            return stringWriter.getBuffer().toString();
        } catch (TransformerConfigurationException e) {
            log.error("error when converting XML to String: " + e.getMessage());
            return null;
        } catch (TransformerException e) {
            log.error("error when converting XML to String: " + e.getMessage());
            return null;
        }
    }

    private static Document stringToXml(String inString) throws ParserConfigurationException, SAXException, IOException {
        if ((inString == null) || (inString.length() == 0)) {
            return null;
        }

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        return builder.parse(new InputSource(new StringReader(inString)));
    }

    /**
     * Extract the ack code from the response
     *
     * @param inAck
     * @return
     */
    public static String parseResponse(String inAck) {
        if ((inAck == null) || (inAck.length() == 0)) {
            return null;
        } else {
            try {
                Document response = stringToXml(inAck);
                Element ackPart = (Element) response.getElementsByTagName("acceptAckCode");
                if (ackPart == null) {
                    log.error("cannot find element acceptAckCode");
                    return null;
                } else {
                    return ackPart.getAttribute(CODE);
                }

            } catch (ParserConfigurationException e) {
                log.error("error when parsing response " + e.getMessage());
                return null;
            } catch (SAXException e) {
                log.error("error when parsing response " + e.getMessage());
                return null;
            } catch (IOException e) {
                log.error("error when parsing response " + e.getMessage());
                return null;
            }
        }
    }

    /**
     * Build an HL7v3 message to register the given patient into the system defined by the given config
     *
     * @param inPatient: patient to register
     * @param inConfig:  configuration of the system which will receive the message
     * @return a string representing the HL7v3 message
     */
    public String generate(Patient inPatient, SystemConfig inConfig) {
        if ((inPatient == null) || (inConfig == null)) {
            return null;
        }

        try {

            List<AssigningAuthority> authorities = null;
            AssigningAuthority authority = null;

            authority = inConfig.getAuthority();


            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document message = builder.newDocument();

            // message properties
            message.setXmlVersion("1.0");
            message.setXmlStandalone(true);

            //root
            Element root = message.createElement("soapenv:Envelope");
            root.setAttribute("xmlns:soapenv", "http://www.w3.org/2003/05/soap-envelope");

            //soapenv:Header
            Element header = message.createElement("soapenv:Header");
            header.setAttribute("xmlns:wsa", "http://www.w3.org/2005/08/addressing");
            // wsa:To
            Element to = message.createElement("wsa:To");
            to.setTextContent(inConfig.getUrl());
            header.appendChild(to);
            // wsa:ReplayTo
            Element replyTo = message.createElement("wsa:ReplyTo");
            Element address = message.createElement("wsa:Address");
            address.setTextContent("http://www.w3.org/2005/08/addressing/anonymous");
            replyTo.appendChild(address);
            header.appendChild(replyTo);
            // wsa:MessageID
            Element messageId = message.createElement("wsa:MessageID");


            messageId.setTextContent("urn:uuid:875561b2-c750-3dc2-7567-31a0e03373ab");
            header.appendChild(messageId);
            //wsa:Action
            Element action = message.createElement("wsa:Action");
            action.setTextContent(v3Namespace + " :" + CODE_ACTION);
            header.appendChild(action);

            root.appendChild(header);

            //soapenv:Body
            Element body = message.createElement("soapenv:Body");
            Element v3root = message.createElement("v3:PRPA_IN201301UV02");
            //v3
            v3root.setAttribute("xmlns:v3", v3Namespace);
            v3root.setAttribute("ITSVersion", "XML_1.0");
            //id
            Element id = message.createElement(V3ID);

            id.setAttribute(ROOT, inConfig.getReceivingApplication());
            v3root.appendChild(id);
            //creationTime
            Element creationTime = message.createElement("v3:creationTime");
            creationTime.setAttribute(VALUE, sdf.format(calendar.getTime()));
            v3root.appendChild(creationTime);
            //interactionID
            Element interactionId = message.createElement("v3:interactionId");
            interactionId.setAttribute("extension", CODE_ACTION);
            interactionId.setAttribute(ROOT, CODE_SYSTEM);
            v3root.appendChild(interactionId);
            //processingCode
            Element processingCode = message.createElement("v3:processingCode");
            processingCode.setAttribute(CODE, "P");
            v3root.appendChild(processingCode);
            //processingModeCode
            Element processingModeCode = message.createElement("v3:processingModeCode");
            processingModeCode.setAttribute(CODE, "T");
            v3root.appendChild(processingModeCode);
            //acceptAckCode
            Element acceptAckCode = message.createElement("v3:acceptAckCode");
            acceptAckCode.setAttribute(CODE, "AL");
            v3root.appendChild(acceptAckCode);
            //receiver
            Element receiver = message.createElement("v3:receiver");
            receiver.setAttribute(TYPECODE, "RCV");
            //device
            Element rdevice = message.createElement("v3:device");
            rdevice.setAttribute(CLASSCODE, "DEV");
            rdevice.setAttribute(DETERMINERCODE, INSTANCE);
            //id
            Element deviceId = message.createElement(V3ID);
            deviceId.setAttribute(ROOT, inConfig.getReceivingApplication());
            rdevice.appendChild(deviceId);
            //asAgent
            Element ragent = message.createElement("v3:asAgent");
            ragent.setAttribute(CLASSCODE, "AGNT");
            //representedOrganization
            Element representedOrganization = message.createElement("v3:representedOrganization");
            representedOrganization.setAttribute(CLASSCODE, "ORG");
            representedOrganization.setAttribute(DETERMINERCODE, INSTANCE);
            //id
            Element representedOrganizationid = message.createElement(V3ID);
            representedOrganizationid.setAttribute(ROOT, inConfig.getReceivingFacility());
            representedOrganization.appendChild(representedOrganizationid);
            ragent.appendChild(representedOrganization);
            rdevice.appendChild(ragent);
            receiver.appendChild(rdevice);
            v3root.appendChild(receiver);
            //sender
            Element sender = message.createElement("v3:sender");
            sender.setAttribute(TYPECODE, "SND");
            //device
            Element sdevice = message.createElement("v3:device");
            sdevice.setAttribute(CLASSCODE, "DEV");
            sdevice.setAttribute(DETERMINERCODE, INSTANCE);

            Element senderId = message.createElement(V3ID);
            senderId.setAttribute(ROOT, SENDING_APPLICATION);
            sdevice.appendChild(senderId);

            sender.appendChild(sdevice);
            v3root.appendChild(sender);
            //controlActProcess
            Element controlActProcess = message.createElement("v3:controlActProcess");
            controlActProcess.setAttribute(CLASSCODE, "CACT");
            controlActProcess.setAttribute("moodCode", "EVN");
            //code
            Element code = message.createElement("v3:code");
            code.setAttribute(CODE, "PRPA_TE201301UV02");
            code.setAttribute("codeSystem", CODE_SYSTEM);
            controlActProcess.appendChild(code);
            // subject
            Element subject = message.createElement("v3:subject");
            subject.setAttribute(TYPECODE, "SUBJ");

            subject.setAttribute("contextConductionInd", "false");
            // registraction Event
            Element registrationEvent = message.createElement("v3:registrationEvent");
            registrationEvent.setAttribute(CLASSCODE, "REG");
            registrationEvent.setAttribute("moodCode", "EVN");
            //eventid
            Element eventId = message.createElement(V3ID);
            eventId.setAttribute("nullFlavor", "NA");
            registrationEvent.appendChild(eventId);
            //statusCode
            Element statusCode = message.createElement("v3:statusCode");
            statusCode.setAttribute(CODE, "active");
            registrationEvent.appendChild(statusCode);
            //subject1
            Element subject1 = message.createElement("v3:subject1");
            subject1.setAttribute(TYPECODE, "SBJ");
            //patient
            Element patient = message.createElement("v3:patient");
            patient.setAttribute(CLASSCODE, "PAT");

            ////////NEW
            Element custodian = message.createElement("v3:custodian");
            custodian.setAttribute(TYPECODE, "CST");

            Element assignedEntity = message.createElement("v3:assignedEntity");
            assignedEntity.setAttribute(CLASSCODE, "ASSIGNED");
            custodian.appendChild(assignedEntity);

            Element assignedEntityId = message.createElement(V3ID);
            String ddsOID = ApplicationConfiguration.getValueOfVariable("DDS_OID");
            if (ddsOID.isEmpty()) {
                assignedEntityId.setAttribute(ROOT, "");
            } else {
                assignedEntityId.setAttribute(ROOT, ddsOID);
            }


            assignedEntity.appendChild(assignedEntityId);

            Element assignedOrganization = message.createElement("v3:assignedOrganization");
            assignedOrganization.setAttribute(DETERMINERCODE, INSTANCE);
            assignedOrganization.setAttribute(CLASSCODE, "ORG");
            assignedEntity.appendChild(assignedOrganization);

            Element assignedOrganizationName = message.createElement("v3:name");
            assignedOrganizationName.setTextContent(DDS_NAME);
            assignedOrganization.appendChild(assignedOrganizationName);

            /////////NEW End


            if (authority != null) {
                //patient ID
                Element patientId = message.createElement(V3ID);
                patientId.setAttribute("assigningAuthorityName", authority.getName());
                patientId.setAttribute("extension", authority.getKeyword() + inPatient.getId().toString());
                patientId.setAttribute(ROOT, authority.getRootOID());
                patient.appendChild(patientId);
            } else {
                log.error("no authority defined");
                return null;
            }
            //status code
            Element patientStatusCode = message.createElement("v3:statusCode");
            patientStatusCode.setAttribute(CODE, "active");
            patient.appendChild(patientStatusCode);
            //person
            Element person = message.createElement("v3:patientPerson");
            person.setAttribute(CLASSCODE, "PSN");
            person.setAttribute(DETERMINERCODE, INSTANCE);
            //name
            Element name = message.createElement("v3:name");

            if (inPatient.getPerson() != null) {
                if (inPatient.getPerson().getNameType() != null) {
                    name.setAttribute("use", inPatient.getPerson().getNameType());
                }

                if (inPatient.getPerson().getLastName() != null) {
                    if (inPatient.getPerson().getLastName().getValue() != null) {
                        Element familyName = message.createElement("v3:family");
                        familyName.setTextContent(inPatient.getPerson().getLastName().getValue());
                        name.appendChild(familyName);
                    }
                } else {
                    Element familyName = message.createElement("v3:family");
                    familyName.setTextContent(inPatient.getPerson().getLastAlternativeName());
                    name.appendChild(familyName);
                }

                if (inPatient.getPerson().getFirstNameSex() != null) {
                    if (inPatient.getPerson().getFirstNameSex().getFirstName() != null) {
                        Element givenName = message.createElement("v3:given");
                        givenName.setTextContent(inPatient.getPerson().getFirstNameSex().getFirstName());
                        name.appendChild(givenName);
                    }
                } else {
                    Element givenName = message.createElement("v3:given");
                    givenName.setTextContent(inPatient.getPerson().getFirstAlternativeName());
                    name.appendChild(givenName);
                }

                person.appendChild(name);

                //For the Other Name
                if (inPatient.getPerson().getOtherNameList() != null
                        && !inPatient.getPerson().getOtherNameList().isEmpty()) {
                    for (OtherName otherName : inPatient.getPerson().getOtherNameList()) {
                        name = message.createElement("v3:name");

                        //Don't know how sill the maiden name and the display name.
                        //Just indicate the Alias name.
                        if (otherName != null
                                && otherName.getNameType() != null
                                && (otherName.getNameType().equals("A"))) {
                            if (otherName.getNameType() != null) {
                                if (otherName.getNameType().equals("A")) {
                                    name.setAttribute("use", "P");
                                    //P like Pseudonyme
                                } else {
                                    name.setAttribute("use", otherName.getNameType());
                                }
                            }

                            if (otherName.getLastName() != null && otherName.getLastName().getValue() != null) {
                                Element familyName = message.createElement("v3:family");
                                familyName.setTextContent(otherName.getLastName().getValue());
                                name.appendChild(familyName);
                            }

                            if (otherName.getFirstNameSex() != null) {
                                Element givenName = message.createElement("v3:given");
                                givenName.setTextContent(otherName.getFirstNameSex().getFirstName());
                                name.appendChild(givenName);
                            }

                            person.appendChild(name);
                        }
                    }
                }

                if (inPatient.getPerson().getSex() != null) {
                    Element gender = message.createElement("v3:administrativeGenderCode");
                    gender.setAttribute(CODE, inPatient.getPerson().getSex().getCode());
                    person.appendChild(gender);
                }

                if (inPatient.getPerson().getDateOfBirth() != null) {
                    Element birth = message.createElement("v3:birthTime");
                    birth.setAttribute(VALUE, getDateAsString(inPatient.getPerson().getDateOfBirth()));
                    person.appendChild(birth);
                }

                if (inPatient.getPerson().getDateOfDeath() != null) {
                    Element deadIndicator = message.createElement("v3:deceasedInd");
                    deadIndicator.setAttribute(VALUE, "true");
                    person.appendChild(deadIndicator);

                    Element deadtime = message.createElement("v3:deceasedTime");
                    deadtime.setAttribute(VALUE, getDateAsString(inPatient.getPerson().getDateOfDeath()));
                    person.appendChild(deadtime);
                }


                if (inPatient.getPerson().getReligion() != null) {
                    Element religion = message.createElement("v3:religiousAffiliationCode");
                    religion.setAttribute(CODE, inPatient.getPerson().getReligion().getCode());
                    person.appendChild(religion);
                }

                if (inPatient.getPerson().getRace() != null) {
                    Element race = message.createElement("v3:raceCode");
                    race.setAttribute(CODE, inPatient.getPerson().getRace().getCode());
                    person.appendChild(race);
                }
            }

            Element personAddress = message.createElement("v3:addr");

            if (inPatient.getAddress() != null && inPatient.getAddress().size() > 0) {
                if (inPatient.getAddress().get(0).getStreet() != null) {
                    Element street = message.createElement("v3:streetAddressLine");
                    street.setTextContent(inPatient.getAddress().get(0).getStreet().getValue());
                    personAddress.appendChild(street);
                }
                if (inPatient.getAddress().get(0).getCity() != null) {
                    Element city = message.createElement("v3:city");
                    city.setTextContent(inPatient.getAddress().get(0).getCity().getValue());
                    personAddress.appendChild(city);
                }
                if (inPatient.getAddress().get(0).getState() != null) {
                    Element state = message.createElement("v3:state");
                    state.setTextContent(inPatient.getAddress().get(0).getState().getCode());
                    personAddress.appendChild(state);
                }
                if (inPatient.getAddress().get(0).getPostalCode() != null) {
                    Element zipCode = message.createElement("v3:postalCode");
                    zipCode.setTextContent(inPatient.getAddress().get(0).getPostalCode());
                    personAddress.appendChild(zipCode);
                }
                if (inPatient.getAddress().get(0).getCountry() != null) {
                    Element country = message.createElement("v3:country");
                    country.setTextContent(inPatient.getAddress().get(0).getCountry().getIso());
                    personAddress.appendChild(country);
                }
            }

            person.appendChild(personAddress);

            if (inPatient.getPerson().getMaritalStatus() != null
                    && !inPatient.getPerson().getMaritalStatus().isEmpty()) {
                Element maritalStatus = message.createElement("v3:maritalStatusCode");
                maritalStatus.setAttribute(CODE, inPatient.getPerson().getMaritalStatus());
                person.appendChild(maritalStatus);
            }

            //end person
            patient.appendChild(person);
            //providerOrganization
            Element provider = message.createElement("v3:providerOrganization");

            //////NEW
            provider.setAttribute(CLASSCODE, "ORG");
            provider.setAttribute(DETERMINERCODE, INSTANCE);

            //NEW END

            Element providerId = message.createElement(V3ID);
            providerId.setAttribute(ROOT, authority.getRootOID());
            provider.appendChild(providerId);


            /////////////NEW
            Element contactParty = message.createElement("v3:contactParty");
            contactParty.setAttribute(CLASSCODE, "CON");
            provider.appendChild(contactParty);

            Element telecom = message.createElement("v3:telecom");
            telecom.setAttribute(VALUE, "1-630-571-3333");
            contactParty.appendChild(telecom);
            ///////////////NEW END

            // end provider
            patient.appendChild(provider);
            // end patient
            subject1.appendChild(patient);
            registrationEvent.appendChild(subject1);
            // end registration event
            registrationEvent.appendChild(custodian);
            // end subject
            subject.appendChild(registrationEvent);
            // end Control Act Process
            controlActProcess.appendChild(subject);
            // end v3
            v3root.appendChild(controlActProcess);
            // end body
            body.appendChild(v3root);
            // end root
            root.appendChild(body);
            // end document
            message.appendChild(root);


            // convert to String
            return xmlToString(message);

        } catch (Exception e) {
            log.info("An error occurred when generating HL7v3 message" + e.getMessage());
            return null;
        }
    }

}
