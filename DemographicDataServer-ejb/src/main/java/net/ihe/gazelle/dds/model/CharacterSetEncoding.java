package net.ihe.gazelle.dds.model;

import org.jboss.seam.annotations.Name;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;


/**
 * <b>Class Description :  </b>CharacterEncoding<br><br>
 * This class describes a CharacterEncoding object. It corresponds to a CharacterEncoding generated through DemographicDataServer application.
 * It corresponds to a medical Information for a Patient.
 * <p>
 * Patient object possesses the following attributes :
 * <ul>
 * <li><b>id</b> : id corresponding to the characterEncoding</li>
 * <li><b>value</b> : Object corresponding to the characterEncoding</li>
 * </ul></br>
 * <b>Example of CharacterEncoding</b> : CharacterEncoding(id, value) = (1, ASCII) <br>
 *
 * @author Nicolas Lefebvre / INRIA Rennes IHE development Project
 * @version 1.0 - 2010, November 10th
 * @class CharacterSetEncoding.java
 * @package net.ihe.gazelle.demographic.model
 * @see > sayed@irisa.fr - jchatel@irisa.fr  -  http://www.ihe-europe.org
 */

@Entity
@XmlRootElement(name = "CharacterSetEncoding")
@XmlAccessorType(XmlAccessType.NONE)
@Name("CharacterSetEncoding")
@Table(name = "dds_character_set_encoding", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "id"))
@org.hibernate.annotations.Entity(selectBeforeUpdate = true)
@SequenceGenerator(name = "dds_character_set_encoding_sequence", sequenceName = "dds_character_set_encoding_id_seq", allocationSize = 1)

public class CharacterSetEncoding implements java.io.Serializable {


    /**
     * Serial ID version of this object
     */
    private static final long serialVersionUID = -450011335507896760L;

    /**
     * Id of this CharacterEncoding object
     */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "dds_character_set_encoding_sequence")
    @NotNull
    @Column(name = "id", nullable = false, unique = true)
    private Integer id;

    /**
     * The value of the carachterEncoding  to describe a language (eg. ASCII)
     */
    @NotNull
    @XmlElement(name = "charset")
    @Column(name = "value", nullable = false)
    private String charset;

    /**
     * this is the value to put into the MSH segment of the HL7 message in order to declare the charset
     */
    @Column(name = "hl7_charset")
    private String hl7Charset;

    /**
     * CharacterSetEncoding aivailable for this country
     */
    @ManyToMany
    @JoinTable(name = "dds_country_code_character_set", joinColumns = @JoinColumn(name = "character_set_encoding_id"), inverseJoinColumns = @JoinColumn(name = "country_code_id"))
    private List<CountryCode> countryCodeList;

    //Constructor	
    public CharacterSetEncoding() {

    }


    public CharacterSetEncoding(Integer id, String charset) {
        super();
        this.id = id;
        this.charset = charset;
    }


    public CharacterSetEncoding(String charset) {
        super();
        this.charset = charset;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCharset() {
        return charset;
    }

    public void setCharset(String charset) {
        this.charset = charset;
    }

    public void setCountryCodeList(List<CountryCode> countryCodeList) {
        this.countryCodeList = countryCodeList;
    }


    public List<CountryCode> getCountryCodeList() {
        return countryCodeList;
    }


    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((charset == null) ? 0 : charset.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        CharacterSetEncoding other = (CharacterSetEncoding) obj;
        if (charset == null) {
            if (other.charset != null) {
                return false;
            }
        } else if (!charset.equals(other.charset)) {
            return false;
        }
        return true;
    }


    //Methods
    public static List<CharacterSetEncoding> getAllCharacterSetEncoding() {
        CharacterSetEncodingQuery query = new CharacterSetEncodingQuery();
        return query.getList();
    }


    public static CharacterSetEncoding getUTF8CharacterSetEncoding() {
        CharacterSetEncodingQuery query = new CharacterSetEncodingQuery();
        query.charset().eq("UTF-8");
        return query.getUniqueResult();
    }


    public String getHl7Charset() {
        return hl7Charset;
    }


    public void setHl7Charset(String hl7Charset) {
        this.hl7Charset = hl7Charset;
    }


}
