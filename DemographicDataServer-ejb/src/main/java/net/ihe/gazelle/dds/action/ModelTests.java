package net.ihe.gazelle.dds.action;

import net.ihe.gazelle.common.interfacegenerator.GenerateInterface;
import net.ihe.gazelle.common.profiling.MeasureCalls;
import net.ihe.gazelle.dds.model.*;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.*;
import org.jboss.seam.log.Log;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import java.io.Serializable;


@AutoCreate
@MeasureCalls
@Stateful
@Name("modelTests")
@Scope(ScopeType.SESSION)
@GenerateInterface(value = "ModelTestsRemote", isLocal = false, isRemote = true)
public class ModelTests implements Serializable, ModelTestsRemote {


    /**
     *
     */
    private static final long serialVersionUID = -3000792016479972302L;

    @Logger
    private static Log log;


    @Destroy
    @Remove
    public void destroy() {
        log.info("destroy");
    }


    public FirstNameCountryCode generateFirstName(String countryCode) {
        // TODO Auto-generated method stub
        return Person.generateFirstName(countryCode, null, null, null);
    }


    public LastNameCountryCode generateLastName(String countryCode) {
        // TODO Auto-generated method stub
        return Person.generateLastName(countryCode, null, null, null);
    }


    public ReligionByCountryCode generateReligion(String countryCode) {
        // TODO Auto-generated method stub
        return Person.generateReligion(countryCode);
    }


    public RaceByCountryCode generateRace(String countryCode) {
        // TODO Auto-generated method stub
        return Person.generateRace(countryCode);
    }


    public Sex generateSex(String countryCode, FirstNameSex firstNameSex) {
        // TODO Auto-generated method stub
        return Person.generateSex(countryCode, firstNameSex);
    }


    public Person generateRandomPerson(String countryCode) {
        // TODO Auto-generated method stub
        return Person.generateRandomPerson(
                countryCode,
                true,
                true,
                true,
                true,
                true,
                true,
                null,
                null,
                null,
                null,
                null,
                null,
                false,
                false,
                false,
                false,
                false,
                null);
    }

}
