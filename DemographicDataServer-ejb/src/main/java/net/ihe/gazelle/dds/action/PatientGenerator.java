/*
 * Copyright 2009 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.dds.action;


import net.ihe.gazelle.dds.admin.WebServicesSecurity;
import net.ihe.gazelle.dds.geonames.GeoName;
import net.ihe.gazelle.dds.model.*;
import net.ihe.gazelle.dds.utils.Utils;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.jboss.seam.log.Log;
import org.jdom.JDOMException;

import javax.ejb.Remove;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


@Name("patientGenerator")
@Scope(ScopeType.PAGE)
public class PatientGenerator implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -8734734233751607120L;

    private static final String IS = "Is";
    private static final String ISLIKE = "Is Like";
    private static final String NEARBY = "Nearby";
    private static final String RANDOM = "Random";
    private static final String EMPTY = "Empty";
    private static final String UNKNOWN = "Unknown";
    private static final String MARRIED = "Married";
    private static final String DIVORCED = "Divorced";
    private static final String YES = "Yes";
    private static final String NO = "No";
    private static final String EM = "entityManager";
    private static final String EPW = "Error - Problem with : ";

    /**
     * Logger
     */
    @Logger
    private static Log log;

    /**
     * entityManager is the interface used to interact with the persistence
     * context.
     */

    private Patient patient;

    private Patient specificPatient;

    private CountryCode countryCode;

    private String lastNameOption = "";

    private String firstNameOption = "";

    private String motherMaidenNameOption = "";

    private String genderOption;

    private String religionOption = "";

    private String raceOption = "";

    private String birthDayOption = "";

    private String addressOption = "";

    private GeoName selectedTown;

    private Double selectedLattitude;

    private Double selectedLongitude;

    private String lastNameField;

    private String firstNameField;

    private List<String> selectedProfileList = new ArrayList<String>();

    private Patient specificPatientWithProfile;

    private String maritalSatusOption;

    private String deadPatientOption;

    private String maidenNameOption;

    private String aliasNameOption;

    private String displayNameOption;

    private String newBornOption;

    private boolean maritalStatusDisabledOption;

    private boolean maidenNameDisabledOption;

    private boolean newBornDisabledOption;

    private boolean alreadyInitialize = false;

    //  getters and setters ////////////////////////////////////////////////////////////

    public static String getUserIpAddress() {
        return (String) ((HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest()).getRemoteAddr();
    }

    public String getLastNameOption() {
        return lastNameOption;
    }

    public void setLastNameOption(String lastNameOption) {
        this.lastNameOption = lastNameOption;
    }

    public boolean isMaritalStatusDisabledOption() {
        return maritalStatusDisabledOption;
    }

    public void setMaritalStatusDisabledOption(boolean maritalStatusDisabledOption) {
        this.maritalStatusDisabledOption = maritalStatusDisabledOption;
    }

    public boolean isMaidenNameDisabledOption() {
        return maidenNameDisabledOption;
    }

    public void setMaidenNameDisabledOption(boolean maidenNameDisabledOption) {
        this.maidenNameDisabledOption = maidenNameDisabledOption;
    }

    public boolean isNewBornDisabledOption() {
        return newBornDisabledOption;
    }

    public void setNewBornDisabledOption(boolean newBornDisabledOption) {
        this.newBornDisabledOption = newBornDisabledOption;
    }

    public String getMotherMaidenNameOption() {
        return motherMaidenNameOption;
    }

    public void setMotherMaidenNameOption(String motherMaidenNameOption) {
        this.motherMaidenNameOption = motherMaidenNameOption;
    }

    public String getFirstNameOption() {
        return firstNameOption;
    }

    public void setFirstNameOption(String firstNameOption) {
        this.firstNameOption = firstNameOption;
    }

    public Patient getSpecificPatient() {
        return specificPatient;
    }

    public void setSpecificPatient(Patient specificPatient) {
        this.specificPatient = specificPatient;
    }

    public String getLastNameField() {
        return lastNameField;
    }

    public void setLastNameField(String lastNameField) {
        this.lastNameField = lastNameField;
    }

    public String getFirstNameField() {
        return firstNameField;
    }

    public void setFirstNameField(String firstNameField) {
        this.firstNameField = firstNameField;
    }

    public Double getSelectedLattitude() {
        return selectedLattitude;
    }

    public void setSelectedLattitude(Double selectedLattitude) {
        this.selectedLattitude = selectedLattitude;
    }

    public Double getSelectedLongitude() {
        return selectedLongitude;
    }

    public void setSelectedLongitude(Double selectedLongitude) {
        this.selectedLongitude = selectedLongitude;
    }

    public GeoName getSelectedTown() {
        return selectedTown;
    }

    public void setSelectedTown(GeoName selectedTown) {
        this.selectedTown = selectedTown;
    }

    public String getReligionOption() {
        return religionOption;
    }

    public void setReligionOption(String religionOption) {
        this.religionOption = religionOption;
    }

    public String getRaceOption() {
        return raceOption;
    }

    public void setRaceOption(String raceOption) {
        this.raceOption = raceOption;
    }

    public String getBirthDayOption() {
        return birthDayOption;
    }

    public void setBirthDayOption(String birthDayOption) {
        this.birthDayOption = birthDayOption;
    }

    public String getAddressOption() {
        return addressOption;
    }

    public void setAddressOption(String addressOption) {
        this.addressOption = addressOption;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public CountryCode getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(CountryCode countryCode) {
        this.countryCode = countryCode;
    }

    public String getGenderOption() {
        return genderOption;
    }

    public void setGenderOption(String genderOption) {
        this.genderOption = genderOption;
    }

    public Patient getSpecificPatientWithProfile() {
        return specificPatientWithProfile;
    }

    public void setSpecificPatientWithProfile(Patient specificPatientWithProfile) {
        this.specificPatientWithProfile = specificPatientWithProfile;
    }

    public String getMaritalSatusOption() {
        return maritalSatusOption;
    }

    public void setMaritalSatusOption(String maritalSatusOption) {
        this.maritalSatusOption = maritalSatusOption;
    }

    public String getDeadPatientOption() {
        return deadPatientOption;
    }

    public void setDeadPatientOption(String deadPatientOption) {
        this.deadPatientOption = deadPatientOption;
    }

    public String getMaidenNameOption() {
        return maidenNameOption;
    }

    public void setMaidenNameOption(String maidenNameOption) {
        this.maidenNameOption = maidenNameOption;
    }

    public String getAliasNameOption() {
        return aliasNameOption;
    }

    public void setAliasNameOption(String aliasNameOption) {
        this.aliasNameOption = aliasNameOption;
    }

    public String getDisplayNameOption() {
        return displayNameOption;
    }

    public void setDisplayNameOption(String displayNameOption) {
        this.displayNameOption = displayNameOption;
    }

    public List<String> getSelectedProfileList() {
        return selectedProfileList;
    }

    public void setSelectedProfileList(List<String> selectedProfileList) {
        this.selectedProfileList = selectedProfileList;
    }

    public String getNewBornOption() {
        return newBornOption;
    }

    public void setNewBornOption(String newBornOption) {
        this.newBornOption = newBornOption;
    }

    public boolean isAlreadyInitialize() {
        return alreadyInitialize;
    }

    //  methods ////////////////////////////////////////////////////////////

    public void setAlreadyInitialize(boolean alreadyInitialize) {
        this.alreadyInitialize = alreadyInitialize;
    }

    public void generatePatient() {
        if (this.countryCode == null) {
            FacesMessages.instance().addFromResourceBundle(StatusMessage.Severity.ERROR, "gazelle.dds.AnyCountryCodeSelectedMessage");
            this.specificPatient = null;
            return;
        }
        if ((firstNameOption.equals(IS) || firstNameOption.equals(ISLIKE)) && (firstNameField.isEmpty())) {
            FacesMessages.instance().addFromResourceBundle(StatusMessage.Severity.ERROR, "gazelle.dds.FirstNameIsEmpty");
            this.specificPatient = null;
            return;
        }
        if ((lastNameOption.equals(IS) || lastNameOption.equals(ISLIKE)) && (lastNameField.isEmpty())) {
            FacesMessages.instance().addFromResourceBundle(StatusMessage.Severity.ERROR, "gazelle.dds.LastNameIsEmpty");
            this.specificPatient = null;
            return;
        }
        if ((addressOption.equals(NEARBY)) && (selectedTown == null)) {
            FacesMessages.instance().addFromResourceBundle(StatusMessage.Severity.ERROR, "gazelle.dds.CityIsEmpty");
            this.specificPatient = null;
            return;
        }


        if (addressOption.equals(NEARBY)) {
            if (this.selectedTown != null) {
                generatePatient(selectedTown);
            } else {
                generatePatient(countryCode);
            }
        } else {
            generatePatient(countryCode);
        }
    }


    public void generatePatient(GeoName inGeoName) {
        this.patient = null;
        specificPatient = new Patient();
        if (countryCode != null) {
            Patient result;
            try {
                boolean firstNameBoolean = true;
                if (firstNameOption.equals(EMPTY)) {
                    firstNameBoolean = false;
                }

                boolean lastNameBoolean = true;
                if (lastNameOption.equals(EMPTY)) {
                    lastNameBoolean = false;
                }

                String firstNameLike = null;
                String lastNameLike = null;
                if (firstNameOption.equals(ISLIKE)) {
                    firstNameLike = this.firstNameField;
                }

                if (lastNameOption.equals(ISLIKE)) {
                    lastNameLike = this.lastNameField;
                }

                boolean motherMaidenNameBoolean = true;
                if (motherMaidenNameOption.equals(EMPTY)) {
                    motherMaidenNameBoolean = false;
                }

                boolean religionBoolean = false;
                if (countryCode.getGenerateReligion() && religionOption.equals(RANDOM)) {
                    religionBoolean = true;
                }

                boolean raceBoolean = false;
                if (countryCode.getGenerateRace() && raceOption.equals(RANDOM)) {
                    raceBoolean = true;
                }

                boolean dobBoolean = true;
                if (birthDayOption.equals(EMPTY)) {
                    dobBoolean = false;
                }

                boolean addBoolean = true;
                if (addressOption.equals(EMPTY)) {
                    addBoolean = false;
                }

                Gender gender;
                if (genderOption.equals(RANDOM)) {
                    gender = Gender.getRandomGenderWithoutMixed();
                } else {
                    gender = Gender.getGenderByDescription(genderOption);
                }

                String maritalStatus = "";
                if (!maritalSatusOption.equals(UNKNOWN)) {
                    if (maritalSatusOption.equals(MARRIED)) {
                        maritalStatus = "M";
                    } else if (maritalSatusOption.equals("Single")) {
                        maritalStatus = "S";
                    } else if (maritalSatusOption.equals(DIVORCED)) {
                        maritalStatus = "D";
                    } else if (maritalSatusOption.equals(RANDOM)) {
                        double a = Math.random();
                        if (a < 0.3) {
                            maritalStatus = "M";
                        } else if (a > 0.7) {
                            maritalStatus = "D";
                        } else {
                            maritalStatus = "S";
                        }
                    }
                }

                boolean deadPatientBoolean = false;
                if (deadPatientOption.equals(YES)) {
                    deadPatientBoolean = true;
                }

                boolean maidenNameBoolean = false;
                if (maidenNameOption.equals(RANDOM)) {
                    maidenNameBoolean = true;
                }

                boolean aliasNameBoolean = false;
                if (aliasNameOption.equals(RANDOM)) {
                    aliasNameBoolean = true;
                }

                boolean displayNameBoolean = false;
                if (displayNameOption.equals(RANDOM)) {
                    displayNameBoolean = true;
                }

                boolean newBornBoolean = false;
                if (newBornOption.equals(YES)) {
                    newBornBoolean = true;
                }

                result = Patient.generateRandomPatient(
                        countryCode.getIso(),
                        inGeoName,
                        lastNameBoolean,
                        firstNameBoolean,
                        motherMaidenNameBoolean,
                        religionBoolean,
                        raceBoolean,
                        dobBoolean,
                        addBoolean,
                        gender,
                        null,
                        null,
                        firstNameLike,
                        lastNameLike,
                        maritalStatus,
                        deadPatientBoolean,
                        maidenNameBoolean,
                        aliasNameBoolean,
                        displayNameBoolean,
                        newBornBoolean,
                        lastNameField,
                        (EntityManager) Component.getInstance(EM));

                if (isInvalidNames(result)) return;

                if (firstNameOption.equals(IS)) {
                    result.getPerson().setFirstAlternativeName(this.firstNameField);
                    result.getPerson().setFirstNameSex(null);
                    //FIXME : is should not be an alternative name ?
                    //result.getPerson().getFirstNameSex().setFirstName(this.firstNameField);
                }

                if (lastNameOption.equals(IS)) {
                    result.getPerson().setLastAlternativeName(this.lastNameField);
                    result.getPerson().setLastName(null);
                    //FIXME : is should not be an alternative name ?
                    //result.getPerson().getLastName().setValue(this.lastNameField);
                }

                if (!religionOption.equals(RANDOM) && !religionOption.equals(EMPTY)) {
                    Religion religion = Religion.getReligionByDescription(religionOption);
                    if (religion != null) {
                        result.getPerson().setReligion(religion);
                    }
                }

                if (!raceOption.equals(RANDOM) && !raceOption.equals(EMPTY)) {
                    Race race = Race.getRaceByDescription(raceOption);
                    if (race != null) {
                        result.getPerson().setRace(race);
                    }
                }

                specificPatient = mergePatient(result);
                FacesMessages.instance().add(StatusMessage.Severity.INFO, "Patient generated");
            } catch (Exception e) {
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, EPW + e.getMessage());
            }
        } else {
            log.warn("countryCode is null - cannot generate patient : " + countryCode);
        }
    }


    public void generateRandomPatient(CountryCode inCountryCode) {

        String currentIp = getUserIpAddress();

        if (WebServicesSecurity.requestWsPermission(currentIp)) {
            Patient inPatient = new Patient();
            if (inCountryCode != null) {
                try {
                    inPatient = Patient.generateRandomPatient(
                            inCountryCode.getIso(),
                            true,
                            true,
                            true,
                            inCountryCode.getGenerateReligion(),
                            inCountryCode.getGenerateRace(),
                            true,
                            true,
                            null,
                            null,
                            null,
                            null,
                            null,
                            null,
                            false,
                            false,
                            false,
                            false,
                            false,
                            null,
                            (EntityManager) Component.getInstance(EM));

                } catch (JDOMException e) {
                    FacesMessages.instance().add(StatusMessage.Severity.ERROR, EPW + e.getMessage());
                    log.error("" + e.getMessage());
                } catch (IOException e) {
                    FacesMessages.instance().add(StatusMessage.Severity.ERROR, EPW + e.getMessage());
                    log.error("" + e.getMessage());
                }
                this.patient = mergePatient(inPatient);
            } else {
                FacesMessages.instance().addFromResourceBundle(StatusMessage.Severity.ERROR, "gazelle.dds.AnyCountryCodeSelectedMessage");
                this.patient = null;
            }
        } else {
            FacesMessages.instance().addFromResourceBundle(StatusMessage.Severity.ERROR, "gazelle.dds.WSLimiteExceeded");
        }

    }


    public void generateRandomPatient() {

        if (this.countryCode != null) {
            this.generateRandomPatient(this.countryCode);
        } else {
            FacesMessages.instance().addFromResourceBundle(StatusMessage.Severity.ERROR, "gazelle.dds.AnyCountryCodeSelectedMessage");
            this.patient = null;
        }
    }


    public void generatePatient(CountryCode inCountryCode) {
        this.patient = null;

        String currentIp = getUserIpAddress();

        if (WebServicesSecurity.requestWsPermission(currentIp)) {
            countryCode = inCountryCode;
            specificPatient = new Patient();

            if (countryCode != null) {
                Patient result;

                try {

                    boolean firstNameBoolean = true;
                    if (firstNameOption.equals(EMPTY)) {
                        firstNameBoolean = false;
                    }

                    boolean lastNameBoolean = true;
                    if (lastNameOption.equals(EMPTY)) {
                        lastNameBoolean = false;
                    }

                    String firstNameLike = null;
                    if (firstNameOption.equals(ISLIKE)) {
                        firstNameLike = this.firstNameField;
                    }

                    String lastNameLike = null;
                    if (lastNameOption.equals(ISLIKE)) {
                        lastNameLike = this.lastNameField;
                    }

                    boolean motherMaidenNameBoolean = true;
                    if (motherMaidenNameOption.equals(EMPTY)) {
                        motherMaidenNameBoolean = false;
                    }

                    boolean religionBoolean = false;
                    if (inCountryCode.getGenerateReligion() && religionOption.equals(RANDOM)) {
                        religionBoolean = true;
                    }

                    boolean raceBoolean = false;
                    if (inCountryCode.getGenerateRace() && raceOption.equals(RANDOM)) {
                        raceBoolean = true;
                    }

                    boolean dobBoolean = true;
                    if (birthDayOption.equals(EMPTY)) {
                        dobBoolean = false;
                    }

                    boolean addBoolean = true;
                    if (addressOption.equals(EMPTY)) {
                        addBoolean = false;
                    }

                    Gender gender;
                    if (genderOption.equals(RANDOM)) {
                        gender = Gender.getRandomGenderWithoutMixed();
                    } else {
                        gender = Gender.getGenderByDescription(genderOption);
                    }

                    String maritalStatus = "";
                    if (!maritalSatusOption.equals(UNKNOWN)) {
                        if (maritalSatusOption.equals(MARRIED)) {
                            maritalStatus = "M";
                        } else if (maritalSatusOption.equals("Single")) {
                            maritalStatus = "S";
                        } else if (maritalSatusOption.equals(DIVORCED)) {
                            maritalStatus = "D";
                        } else if (maritalSatusOption.equals(RANDOM)) {
                            double a = Math.random();
                            if (a < 0.3) {
                                maritalStatus = "M";
                            } else if (a > 0.7) {
                                maritalStatus = "D";
                            } else {
                                maritalStatus = "S";
                            }
                        }
                    }

                    boolean deadPatientBoolean = false;
                    if (deadPatientOption.equals(YES)) {
                        deadPatientBoolean = true;
                    }

                    boolean maidenNameBoolean = false;
                    if (maidenNameOption.equals(RANDOM)) {
                        maidenNameBoolean = true;
                    }

                    boolean aliasNameBoolean = false;
                    if (aliasNameOption.equals(RANDOM)) {
                        aliasNameBoolean = true;
                    }

                    boolean displayNameBoolean = false;
                    if (displayNameOption.equals(RANDOM)) {
                        displayNameBoolean = true;
                    }

                    boolean newBornBoolean = false;
                    if (newBornOption.equals(YES)) {
                        newBornBoolean = true;
                    }


                    result = Patient.generateRandomPatient(
                            countryCode.getIso(),
                            lastNameBoolean,
                            firstNameBoolean,
                            motherMaidenNameBoolean,
                            religionBoolean,
                            raceBoolean,
                            dobBoolean,
                            addBoolean,
                            gender,
                            null,
                            null,
                            firstNameLike,
                            lastNameLike,
                            maritalStatus,
                            deadPatientBoolean,
                            maidenNameBoolean,
                            aliasNameBoolean,
                            displayNameBoolean,
                            newBornBoolean,
                            lastNameField,
                            (EntityManager) Component.getInstance(EM));

                    if (isInvalidNames(result)) return;

                    if (firstNameOption.equals(IS)) {
                        result.getPerson().setFirstAlternativeName(firstNameField);
                        result.getPerson().setFirstNameSex(null);
                        //FIXME : is should not be an alternative name ?
                        //result.getPerson().getFirstNameSex().setFirstName(firstNameField);
                    }

                    if (lastNameOption.equals(IS)) {
                        result.getPerson().setLastAlternativeName(lastNameField);
                        result.getPerson().setLastName(null);
                        //FIXME : is should not be an alternative name ?
                        //result.getPerson().getLastName().setValue(lastNameField);
                    }

                    if (!religionOption.equals(RANDOM) && !religionOption.equals(EMPTY)) {
                        Religion religion = Religion.getReligionByDescription(religionOption);
                        if (religion != null) {
                            result.getPerson().setReligion(religion);
                        }
                    }

                    if (!raceOption.equals(RANDOM) && !raceOption.equals(EMPTY)) {
                        Race race = Race.getRaceByDescription(raceOption);
                        if (race != null) {
                            result.getPerson().setRace(race);
                        }
                    }

                    specificPatient = mergePatient(result);
                    FacesMessages.instance().add(StatusMessage.Severity.INFO, "Patient generated");
                } catch (JDOMException e) {
                    FacesMessages.instance().add(StatusMessage.Severity.ERROR, EPW + e.getMessage());
                } catch (IOException e) {
                    FacesMessages.instance().add(StatusMessage.Severity.ERROR, EPW + e.getMessage());
                }
            } else {
                log.warn("countryCode is null - cannot generate patient : " + countryCode);
            }
        } else {
            FacesMessages.instance().addFromResourceBundle(StatusMessage.Severity.ERROR, "gazelle.dds.WSLimiteExceeded");
        }

    }

    private boolean isInvalidNames(Patient patient) {
        if (patient.getPerson().getFirstNameSex() == null || patient.getPerson().getFirstNameSex().getFirstName() == null || patient.getPerson().getFirstNameSex().getFirstName().isEmpty()) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Error - First name doesn't match your requirement");
            specificPatient = null;
            return true;
        } else if (patient.getPerson().getLastName() == null || patient.getPerson().getLastName().getValue() == null || patient.getPerson().getLastName().getValue().isEmpty()) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Error - Last name doesn't match your requirement");
            specificPatient = null;
            return true;
        }
        return false;
    }


    public Patient mergePatient(Patient patient) {
        return Patient.mergePatient(patient, (EntityManager) Component.getInstance(EM));
    }


    public void generatePatient(String iso) {
        EntityManager entityManager = (EntityManager) Component.getInstance(EM);
        CountryCode cc = entityManager.find(CountryCode.class, iso);
        generatePatient(cc);
    }


    public String getFirstNameAlternateSpellingValues(FirstNameSex firstNameSex) {
        StringBuilder sb = new StringBuilder();
        try {
            if (firstNameSex != null && firstNameSex.getId() != null) {
                List<FirstNameAlternateSpelling> FNASList = FirstNameAlternateSpelling.
                        getFirstNameAlternateSpellingFiltered(firstNameSex);

                if (FNASList != null && FNASList.size() > 0) {
                    sb.append(" ( ");
                    for (int i = 0; i < (FNASList.size() - 1); i++) {
                        if (FNASList.get(i) != null && FNASList.get(i).getValue() != null) {
                            sb.append(FNASList.get(i).getValue());
                            sb.append(" - ");
                        }
                    }
                    if (FNASList.get(FNASList.size() - 1) != null && FNASList.get(FNASList.size() - 1).getValue() != null) {
                        sb.append(FNASList.get(FNASList.size() - 1).getValue());
                    }
                    sb.append(" ) ");
                }
            }
        } catch (Exception e) {
        }
        return sb.toString();
    }


    public String getMotherMaidenNameAlternateSpellingValues() {
        return this.getMotherMaidenNameAlternateSpellingValues(this.patient);
    }


    public String getLastNameAlternateSpellingValues(LastName lastName) {
        StringBuilder sb = new StringBuilder();
        if (lastName != null) {
            List<LastNameAlternateSpelling> listLNAS = LastNameAlternateSpelling.
                    getLastNameAlternateSpellingFiltered(lastName);
            if (listLNAS.size() > 0) {
                sb.append(" ( ");
                for (int i = 0; i < (listLNAS.size() - 1); i++) {
                    if (listLNAS.get(i) != null) {
                        sb.append(listLNAS.get(i).getValue());
                        sb.append(" - ");
                    }
                }
                if (listLNAS.get(listLNAS.size() - 1) != null) {
                    sb.append(listLNAS.get(listLNAS.size() - 1).getValue());
                }
                sb.append(" ) ");
            }
        }
        return sb.toString();
    }


    public String getMotherMaidenNameAlternateSpellingValues(Patient inPatient) {
        StringBuilder sb = new StringBuilder();
        if (inPatient != null && inPatient.getPerson() != null && inPatient.getPerson().getMotherMaidenName() != null) {
            List<LastNameAlternateSpelling> listLnas = LastNameAlternateSpelling.getLastNameAlternateSpellingFiltered(inPatient.getPerson().getMotherMaidenName());
            if (listLnas.size() > 0) {
                sb.append(" ( ");
                for (int i = 0; i < (listLnas.size() - 1); i++) {
                    if (listLnas.get(i) != null) {
                        sb.append(listLnas.get(i).getValue());
                        sb.append(" - ");
                    }
                }
                if (listLnas.get(listLnas.size() - 1) != null) {
                    sb.append(listLnas.get(listLnas.size() - 1).getValue());
                }
                sb.append(" ) ");
            }
        }
        return sb.toString();
    }


    public List<GeoName> getListTownByCountrycode() {
        List<GeoName> result = GeoName.getGeoNameFiltered(this.countryCode.getIso(), null);
        Collections.sort(result);
        return result;
    }

    public void initializePatients() {
        this.patient = null;
        this.specificPatient = null;
    }

    /**
     * Destroy the Manager bean when the session is over.
     */

    @Destroy
    @Remove
    public void destroy() {
        log.debug("destroy");
    }


    public List<String> getGenerationNameOption() {
        List<String> list = new ArrayList<String>();
        list.add(RANDOM);
        list.add(IS);
        list.add(ISLIKE);
        list.add(EMPTY);
        return list;
    }

    public List<String> getGenerationOption() {
        List<String> list = new ArrayList<String>();
        list.add(RANDOM);
        list.add(EMPTY);
        return list;
    }

    public List<String> getGenerationReligionList() {
        List<String> list = new ArrayList<String>();
        list.add(RANDOM);
        list.add(EMPTY);
        List<Religion> religionList = Religion.getReligionList();
        if (religionList != null && !religionList.isEmpty()) {
            for (Religion religion : religionList) {
                list.add(religion.getDescription());
            }
        }
        return list;
    }


    public List<String> getGenerationReligionListForSpecificCountry(CountryCode country) {
        List<String> list = new ArrayList<String>();
        list.add(RANDOM);
        list.add(EMPTY);
        List<Religion> religionList = ReligionByCountryCode.getReligionListForSpecificCountry(country);
        if (religionList != null && !religionList.isEmpty()) {
            for (Religion religion : religionList) {
                list.add(religion.getDescription());
            }
        }
        return list;
    }

    public List<String> getGenerationRaceList() {
        List<String> list = new ArrayList<String>();
        list.add(RANDOM);
        list.add(EMPTY);
        List<Race> raceList = Race.getRaceList();
        if (raceList != null && !raceList.isEmpty()) {
            for (Race race : raceList) {
                list.add(race.getDescription());
            }
        }
        return list;
    }

    public List<String> getGenerationRaceListForSpecificCountry(CountryCode country) {
        List<String> list = new ArrayList<String>();
        list.add(RANDOM);
        list.add(EMPTY);
        List<Race> raceList = RaceByCountryCode.getRaceListForSpecificCountry(country);
        if (raceList != null && !raceList.isEmpty()) {
            for (Race race : raceList) {
                list.add(race.getDescription());
            }
        }
        return list;
    }

    public List<String> getGenerationAddressOption() {
        List<String> list = new ArrayList<String>();
        list.add(RANDOM);
        list.add(NEARBY);
        list.add(EMPTY);
        return list;
    }


    public List<String> getGender() {
        List<String> list = new ArrayList<String>();
        List<Gender> genderList = Gender.getAllGenderWithoutMixed();
        list.add(RANDOM);
        if (genderList != null && !genderList.isEmpty()) {
            for (Gender gender : genderList) {
                list.add(gender.getDescription());
            }
        }
        return list;
    }


    public void resetOption(boolean resetcountryCode) {
        if (resetcountryCode) {
            countryCode = null;
        }

        lastNameOption = RANDOM;
        lastNameField = "";

        firstNameOption = RANDOM;
        firstNameField = "";

        motherMaidenNameOption = RANDOM;
        birthDayOption = RANDOM;
        addressOption = RANDOM;
        genderOption = RANDOM;

        maritalSatusOption = UNKNOWN;
        deadPatientOption = NO;
        maidenNameOption = EMPTY;
        aliasNameOption = RANDOM;
        displayNameOption = RANDOM;
        newBornOption = NO;

        maritalStatusDisabledOption = false;
        maidenNameDisabledOption = false;
        newBornDisabledOption = false;

        getTheDisabledStatus();
        FacesMessages.instance().add(StatusMessage.Severity.INFO, "Options reseted");
    }


    public String getMaritalStatusDescription(String value) {
        return Utils.getMaritalStatusDescription(value);
    }


    public String getNameTypeDescription(String value) {
        return Utils.getNameTypeDescription(value);
    }


    public void presetOption() {
        if (selectedProfileList == null || selectedProfileList.isEmpty()) {
            resetOption(false);
            return;
        }

        if (selectedProfileList.contains("New Born") && selectedProfileList.contains("Married Woman")) {
            FacesMessages.instance().addFromResourceBundle(StatusMessage.Severity.ERROR, "gazelle.dds.TheNewBornCantBeAMarriedWoman");
            return;
        }

        maritalSatusOption = UNKNOWN;
        maidenNameOption = EMPTY;
        genderOption = RANDOM;
        aliasNameOption = EMPTY;
        deadPatientOption = NO;
        newBornOption = NO;
        displayNameOption = EMPTY;

        if (selectedProfileList.contains("Married Woman")) {
            maritalSatusOption = MARRIED;
            maidenNameOption = RANDOM;
            genderOption = "Female";
        }

        if (selectedProfileList.contains("Alias And Display Name")) {
            aliasNameOption = RANDOM;
            displayNameOption = RANDOM;
        }

        if (selectedProfileList.contains("Dead Patient")) {
            deadPatientOption = YES;
        }

        if (selectedProfileList.contains("New Born")) {
            newBornOption = YES;
        }

        getTheDisabledStatus();
        FacesMessages.instance().add(StatusMessage.Severity.INFO, "Options applied");
    }


    public List<String> getMaritalStatusList() {
        List<String> list = new ArrayList<String>();
        list.add(RANDOM);
        list.add(UNKNOWN);
        list.add("Single");
        list.add(MARRIED);
        list.add(DIVORCED);

        return list;
    }


    public List<String> getDeadPatientList() {
        List<String> list = new ArrayList<String>();
        list.add(YES);
        list.add(NO);

        return list;
    }


    public void getTheDisabledStatus() {
        //In order to initialize all options
        if (genderOption == null) {
            resetOption(false);
        }

        initializePatients();

        maritalStatusDisabledOption = false;
        maidenNameDisabledOption = false;
        newBornDisabledOption = false;


        if (!genderOption.equals("Female")) {
            maritalSatusOption = UNKNOWN;
            maritalStatusDisabledOption = true;

            maidenNameOption = EMPTY;
            maidenNameDisabledOption = true;
        }

        if (!maritalSatusOption.equals(UNKNOWN)) {
            newBornOption = NO;
            newBornDisabledOption = true;
        }

        if (!maritalSatusOption.equals(MARRIED) && !maritalSatusOption.equals(DIVORCED)) {
            maidenNameOption = EMPTY;
            maidenNameDisabledOption = true;
        }

        if (maritalSatusOption.equals(MARRIED) || maritalSatusOption.equals(DIVORCED)) {
            maidenNameOption = RANDOM;
        }

        if (countryCode != null) {
            if (!countryCode.getGenerateReligion()) {
                religionOption = EMPTY;
            }

            if (!countryCode.getGenerateRace()) {
                raceOption = EMPTY;
            }
        }

    }

    public void initializeOptions() {
        if (!alreadyInitialize) {
            lastNameOption = RANDOM;
            lastNameField = "";

            firstNameOption = RANDOM;
            firstNameField = "";

            motherMaidenNameOption = RANDOM;
            birthDayOption = RANDOM;
            addressOption = RANDOM;
            genderOption = RANDOM;

            maritalSatusOption = UNKNOWN;
            deadPatientOption = NO;
            maidenNameOption = EMPTY;
            aliasNameOption = RANDOM;
            displayNameOption = RANDOM;
            newBornOption = NO;

            maritalStatusDisabledOption = true;
            maidenNameDisabledOption = true;
            newBornDisabledOption = false;

            alreadyInitialize = true;
        }

    }

    public String sharePatient() {
        List<Patient> patientsToShare = new ArrayList<Patient>();
        if (this.patient != null) {
            patientsToShare.add(patient);
            Contexts.getSessionContext().set("patientsToShare", patientsToShare);
            return "/patient/sharePatient.xhtml";
        } else if (this.specificPatient != null) {
            patientsToShare.add(specificPatient);
            Contexts.getSessionContext().set("patientsToShare", patientsToShare);
            return "/patient/sharePatient.xhtml";
        } else {
            return null;
        }
    }

}
