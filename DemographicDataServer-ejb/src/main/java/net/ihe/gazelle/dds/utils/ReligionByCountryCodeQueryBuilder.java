package net.ihe.gazelle.dds.utils;

import net.ihe.gazelle.dds.model.ReligionByCountryCode;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.beans.HQLOrder;
import net.ihe.gazelle.hql.beans.HQLRestrictionValues;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.NoResultException;
import javax.persistence.Query;

public class ReligionByCountryCodeQueryBuilder extends HQLQueryBuilder<ReligionByCountryCode> {

    private static Logger log = LoggerFactory.getLogger(ReligionByCountryCodeQueryBuilder.class);

    public ReligionByCountryCodeQueryBuilder() {
        super(ReligionByCountryCode.class);
    }

    public Integer getMax(String pathToAttribute) {
        StringBuilder sb = new StringBuilder("select max(" + getShortProperty(pathToAttribute) + ") ");
        // First build where, to get all paths in from
        StringBuilder sbWhere = new StringBuilder();
        HQLRestrictionValues values = buildQueryWhere(sbWhere);
        // Build order, to get all paths in from
        buildQueryOrder(sbWhere);

        // add order columns if needed, cannot sort without it!
        // will be removed at the end (result list will be Object[] then)
        for (HQLOrder order : getOrders()) {
            sb.append(", ").append(order.getPath());
        }
        sb.append(buildQueryFrom());
        sb.append(sbWhere);

        Query query = createRealQuery(sb);

        buildQueryWhereParameters(query, values);
        try {
            return (Integer) query.getSingleResult();
        } catch (NoResultException e) {
            log.error("" + e.getMessage());
            return null;
        }
    }

    public Integer getMin(String pathToAttribute) {
        StringBuilder sb = new StringBuilder("select min(" + getShortProperty(pathToAttribute) + ") ");
        // First build where, to get all paths in from
        StringBuilder sbWhere = new StringBuilder();
        HQLRestrictionValues values = buildQueryWhere(sbWhere);
        // Build order, to get all paths in from
        buildQueryOrder(sbWhere);

        // add order columns if needed, cannot sort without it!
        // will be removed at the end (result list will be Object[] then)
        for (HQLOrder order : getOrders()) {
            sb.append(", ").append(order.getPath());
        }
        sb.append(buildQueryFrom());
        sb.append(sbWhere);

        Query query = createRealQuery(sb);

        buildQueryWhereParameters(query, values);
        try {
            return (Integer) query.getSingleResult();
        } catch (NoResultException e) {
            log.error("" + e.getMessage());
            return null;
        }
    }

}
