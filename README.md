# Demographic Data Server

<!-- TOC -->
* [Demographic Data Server](#demographic-data-server)
  * [Build and tests](#build-and-tests)
  * [Installation manual](#installation-manual)
    * [Configuration for sso-client-v7](#configuration-for-sso-client-v7)
  * [User manual](#user-manual)
  * [Release note](#release-note)
<!-- TOC -->

## Build and tests

The project must be build using JDK-8 and run on a JDK-7 environment.

```shell
mvn clean install
```

## Installation manual

The Demographic Data Server installation manual is available here:
https://gazelle.ihe.net/gazelle-documentation/Demographic-Data-Server/installation.html

### Configuration for sso-client-v7

As Demographic Data Server depends on sso-client-v7, it needs to be configured properly to work. You can follow the README of sso-client-v7
available here: [sso-client-v7](https://gitlab.inria.fr/gazelle/library/sso-client-v7)

## User manual

The Demographic Data Server user manual is available here:
https://gazelle.ihe.net/gazelle-documentation/Demographic-Data-Server/user.html

## Release note

The Demographic Data Server release note is available here:
https://gazelle.ihe.net/gazelle-documentation/Demographic-Data-Server/release-note.html