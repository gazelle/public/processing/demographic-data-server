ALTER TABLE dds_country_code ADD COLUMN cx_4_code character varying(80);
UPDATE dds_country_code SET cx_4_code = country_code_iso;

update dds_country_code SET cx_4_code = 'ASIP-SANTE-INS-C' where name = 'FRANCE';