ALTER TABLE dds_web_services_information ADD COLUMN ws_priority integer;
update dds_web_services_information set ws_priority = 1 where ws_name like 'Nominatim';
update dds_web_services_information set ws_priority = 2 where ws_name like 'Google';
update dds_web_services_information set ws_priority = 3 where ws_name like 'Geoname';
update dds_web_services_information set ws_priority = 4 where ws_name like 'DDS_Ws';
update dds_web_services_information set ws_url = 'https://nominatim.openstreetmap.org/reverse?format=xml&lat=&lon=&zoom=18&addressdetails=1&email=eric.poiseau@inria.fr' where ws_name like 'Nominatim';